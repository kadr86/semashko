<?php get_header(); ?>

<?php 
	
#	 Get page settings
#
	$page_data = ewf_get_page_settings( basename(__FILE__) );
	
	
	switch ($page_data['layout']) {
	
		case "layout-sidebar-single-left": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					dynamic_sidebar($page_data['sidebar']);
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['content'].'">';
				
					woocommerce_content();
					echo '&nbsp;';
					
				echo '</div>';
				echo '</div>';
			echo '</div>';
			break;
	
		case "layout-sidebar-single-right": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['content'].'">';

					woocommerce_content();
					echo '&nbsp;';

				echo '</div>';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					dynamic_sidebar($page_data['sidebar']);
				echo '</div>';
				
			echo '</div>';
			echo '</div>';
			break; 
	
		case "layout-full": 				
			echo '<div class="container">';
			echo '<div class="row ewf-no-composer">';
				echo '<div class="span12">';
				
					woocommerce_content();
					
				echo '</div>';
			echo '</div>';
			echo '</div>';
			break;
	}

?>
<?php get_footer(); ?>