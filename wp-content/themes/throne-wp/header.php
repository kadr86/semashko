<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1,requiresActiveX=true">

<?php

global $ewf_theme_options_defaults;

$favicon = ewf_get_option_UIImage(EWF_SETUP_THNAME."_favicon");
$favicon_retina = ewf_get_option_UIImage(EWF_SETUP_THNAME."_favicon_retina");

$ms_tile_image = ewf_get_option_UIImage(EWF_SETUP_THNAME."_ms_tile_image");
$ms_tile_color = get_option(EWF_SETUP_THNAME."_color_ms_tile", $ewf_theme_options_defaults['color_ms_tile']);
$android_theme_color = get_option(EWF_SETUP_THNAME."_color_android_theme", $ewf_theme_options_defaults['color_android_theme']);



echo '<meta name="msapplication-TileImage" content="' . $ms_tile_image . '" />';
echo '<meta name="msapplication-TileColor" content="' . $ms_tile_color . '" />';
echo '<meta name="theme-color" content="' . $android_theme_color . '" />';

echo '<link rel="shortcut icon" href="'.$favicon.'" />';
echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'.$favicon_retina.'" />';

?>

<?php wp_head(); ?>
	
</head>
<body <?php body_class(); ?>> 
	
    <noscript>
        <div class="javascript-required">
            <i class="fa fa-times-circle"></i> <?php  echo __('You seem to have Javascript disabled. This website needs javascript in order to function properly!', 'bitpub');  ?>
        </div>
    </noscript>
	
	<div id="wrap">

		<?php 
		#	Check if is a blank page template
		
			$page_blank = ewf_is_page_blank();
			
			if (!$page_blank){ 
		?>
	
		<div id="header-wrap">
		
			<div id="header"> 
			
			<!-- /// HEADER  //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

				<div class="container">
					<div class="row">
						<div class="span3">
							
							<!-- // Logo // -->
							<?php 
											
								$logo_url = ewf_get_option_UIImage(EWF_SETUP_THNAME."_logo_url");
								
								echo '<div id="logo">';
								echo '<a href="'.get_home_url().'">
										<img class="responsive-img" src="'.$logo_url.'" alt="">
									  </a>';
								echo '</div><!-- end #logo -->';
									
							?>
							
						</div><!-- end .span3 -->
						
						<div class="span9">
						
							<a id="mobile-menu-trigger" href="#">
								<i class="fa fa-bars"></i>
							</a>					
													
							<?php
								
								if (get_option(EWF_SETUP_THNAME."_header_action", $ewf_theme_options_defaults['header_action']) == 'true'){
									$header_action_text = get_option(EWF_SETUP_THNAME."_header_action_text", null);
									$header_action_link = get_option(EWF_SETUP_THNAME."_header_action_link", '#');
								
									if ($header_action_text){
										echo '<!-- // Buy button // -->';
										echo '<a class="btn" id="header-button" href="' . $header_action_link . '">' . $header_action_text . '</a>';
									}
								}
								
							?>
							
							<!-- // Menu // -->
							<nav>
								<?php  	do_action('ewf-menu-top'); ?>
							</nav>
							
											
						</div><!-- end .span9 -->
					</div><!-- end .row -->		
				</div><!-- end .container -->		

			<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

			</div><!-- end #header -->
		</div>
		
		<?php 
			
		#	Check if is a blank page template
		
			} 
		?>
		
		<div id="content">

		<!-- /// CONTENT  //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
		
		<?php	
			
			if (!$page_blank){ 
				
				#	Load page header
				#				
				get_template_part('framework/modules/ewf-header/templates/page-header');  
		
			}
		
		?>
	