<?php get_header(); ?>

<?php 
	
#	 Get page settings
#
	$page_data = ewf_get_page_settings( basename(__FILE__) );
	
	
	switch ($page_data['layout']) {
	
		case "layout-sidebar-single-left": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					dynamic_sidebar($page_data['sidebar']);
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['content'].'">';
				
					if ( have_posts() ) while ( have_posts() ) : the_post(); 										
						echo the_content();
						wp_link_pages();
					endwhile; 
					echo '&nbsp;';
					
				echo '</div>';
				echo '</div>';
			echo '</div>';
			break;
			
	
		case "layout-sidebar-single-right": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['content'].'">';

					if ( have_posts() ) while ( have_posts() ) : the_post(); 
						echo the_content();
						wp_link_pages();
					endwhile; 
					echo '&nbsp;';

				echo '</div>';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					dynamic_sidebar($page_data['sidebar']);
				echo '</div>';
				
			echo '</div>';
			echo '</div>';
			break; 
			
	
		case "layout-full": 
			if ( have_posts() ) while ( have_posts() ) : the_post(); 
			
				if ($page_data['page']['composer']){
				
					echo the_content();
					wp_link_pages();
					
				}else{
				
					echo '<div class="container">';
					echo '<div class="row ewf-no-composer">';
						echo '<div class="span12">';
						
							echo the_content();
							wp_link_pages();
							
						echo '</div>';
					echo '</div>';
					echo '</div>';
				}

			endwhile;  
	}

?>
<?php get_footer(); ?>