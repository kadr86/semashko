<?php  get_header(); ?>
	
<?php
		
	$ewf_extra_attr = null;
	

#	 Get page settings
#
	$page_data = ewf_get_page_settings( basename(__FILE__) );
	$page_meta = get_post($page_data['page']['id']);
	
	
	switch ($page_data['layout']) {
	
		case "layout-sidebar-single-left": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					dynamic_sidebar($page_data['sidebar']);
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['content'].'">';
				
					if ($page_data['page']['page-posts'] == $page_data['page']['id'] && !empty($page_meta)){ 
						echo apply_filters('the_content',$page_meta->post_content);
					}
					
					echo do_shortcode('[blog '.$ewf_extra_attr.'sidebar="true" ]');
					echo '&nbsp;';
						
				echo '</div>';
			echo '</div>';
			echo '</div>';
			break;
	
		case "layout-sidebar-single-right":
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['content'].'">';

					if ($page_data['page']['page-posts'] == $page_data['page']['id'] && !empty($page_meta)){
						echo apply_filters('the_content',$page_meta->post_content);
					}
					
					echo do_shortcode('[blog '.$ewf_extra_attr.'sidebar="true" ]');
					echo '&nbsp;';
					
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					dynamic_sidebar($page_data['sidebar']);
				echo '</div>';
			echo '</div>';
			echo '</div>';
			break; 
	
		case "layout-full": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span12">';
			
					if ($page_data['page']['page-posts'] == $page_data['page']['id'] && !empty($page_meta)){
						echo apply_filters('the_content',$page_meta->post_content);
					}
					
					echo do_shortcode('[blog '.$ewf_extra_attr.']');
					echo '&nbsp;';

				echo '</div>';
			echo '</div>';
			echo '</div>';
	}

?>
<?php get_footer();  ?>