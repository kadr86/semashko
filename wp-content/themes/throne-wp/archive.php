<?php get_header(); ?>

<?php 

	global $wp_query;
		
#	 Get page settings
#
	$page_data = ewf_get_page_settings( basename(__FILE__) );
	
	
	switch ($page_data['layout']) {
	
		case "layout-sidebar-single-left": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					
					if ( !function_exists('dynamic_sidebar')  || !dynamic_sidebar( $page_data['sidebar'] ));
					
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['content'].'">';
						
					if (!empty($page_data['info'])){ 
						echo wp_kses_post($page_data['info']); 
					}
					
					if ( have_posts() ) while ( have_posts() ) : the_post(); 
						get_template_part('templates/blog-item-default');
					endwhile;
					
					echo ewf_sc_blog_navigation_pages(4, $wp_query);
						
				echo '</div>';
			echo '</div>'; 
			echo '</div>'; 
			break;
	
		case "layout-sidebar-single-right": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['content'].'">';
				
					if (!empty($page_data['info'])){ 
						echo wp_kses_post($page_data['info']); 
					}
					
					if ( have_posts() ) while ( have_posts() ) : the_post(); 
						get_template_part('templates/blog-item-default');
					endwhile; 
					
					echo ewf_sc_blog_navigation_pages(4, $wp_query);
					
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
			
					if ( !function_exists('dynamic_sidebar')  || !dynamic_sidebar( $page_data['sidebar'] ));
					
				echo '</div>';
			echo '</div>';
			echo '</div>';
			break; 
	
		case "layout-full": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span12">';
				
					if (!empty($page_data['info'])){ 
						echo wp_kses_post($page_data['info']); 
					}
					
					if ( have_posts() ) while ( have_posts() ) : the_post(); 
						get_template_part('templates/blog-item-default');
					endwhile; 
					
					echo ewf_sc_blog_navigation_pages(4, $wp_query);
					
				echo '</div>';
			echo '</div>';
			echo '</div>';
			break; 
	}	

?>
<?php get_footer(); ?>