<?php



#	Attach functions to filters
#
	add_action( 'init'  										, 'ewf_loat_tinyMCE_style');
	add_filter( 'tiny_mce_before_init'							, 'ewf_load_mce_styles' );
	add_filter( 'mce_buttons_2'		 							, 'ewf_mce_buttons' ); 


	function  ewf_loat_tinyMCE_style() {
		add_editor_style ( get_template_directory_uri().'/layout/css/fontawesome/font-awesome.min.css');
		add_editor_style ( get_template_directory_uri().'/layout/css/iconfontcustom/icon-font-custom.css');
		add_editor_style ( get_template_directory_uri().'/layout/css/mce.css');
	}
	
	
#	Show the style dropdown by default
#
	function ewf_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	

#	Attach new styles to dropdown
#
	function ewf_load_mce_styles( $settings ) {

		$style_formats = array( 
			array(
				'title' => 'List - Unstyled',
				'selector' => 'ul',
				'attributes' => array( 'class' => 'unstyled')
			),
			array(
				'title' => 'List - Fill circle',
				'selector' => 'ul',
				'attributes' => array( 'class' => 'fill-circle')
			),
			array(
				'title' => 'List - Fill circle border',
				'selector' => 'ul',
				'attributes' => array( 'class' => 'fill-circle border')
			),
			array(
				'title' => 'Highlight',
				'selector' => '*',
				'inline' => 'span',
				'classes' => 'text-highlight',
			),
			array(
				'title' => 'Uppercase',
				'selector' => '*',
				'inline' => 'span',
				'classes' => 'text-uppercase',
			),
			array(
				'title' => 'Mute',
				'selector' => '*',
				'inline' => 'span',
				'classes' => 'mute',
			),

		);

		$settings['style_formats'] = json_encode( $style_formats );
		return $settings;
		
	}


?>