<?php


	#	posts_per_page	-	Show at most x many posts on blog pages.
	#	page_for_posts 	-	The ID of the page that displays posts. Useful when show_on_front's value is page.
	
	$ewf_page_for_posts = get_option('page_for_posts');

	add_shortcode("blog"					,"ewf_sc_blog_overview"	);

	add_action('wp_ajax_ewf_blog_timelinePosts'				, 'ewf_blog_timelinePosts' );
	
	function ewf_blog_timelinePosts(){
		$response = array();
		
		if (is_array($_POST) && array_key_exists('page', $_POST)){ 
			
			$posts = ewf_sc_blog_timelinePages(array('page'=>$_POST['page']));
			
			wp_send_json_success(array('page'=>$_POST['page'], 'state'=>1, 'posts'=>$posts ));
		}else{
			wp_send_json_error(array('state' => 0, 'message'=>'Inconsistent data'));
		}
		
		exit;
	} 
	
	function ewf_sc_blog_navigation_default(){
		
		return get_the_posts_pagination();
		
	}
	
	function ewf_sc_blog_navigation_pages( $range = 5, $query){
		$src_nav = null;
		$max_page = 0;
		
		$data_pages = array();
		
		$class_current = 'current';
		$current_page = $query->query_vars['paged'];
		
		
		
		if ($current_page == 0) { 
			$current_page = 1; 
			}
	
	
	# 	How much pages do we have?
	
		if ( !$max_page ) {
			$max_page = $query->max_num_pages;
			}
			
			
			

	  // We need the pagination only if there are more than 1 page
	  if($max_page > 1){
	  
		if ( !$current_page ) 		{ $current_page = 1; }
		
		if($max_page > $range){
		  // When closer to the beginning
		  if($current_page < $range){
			for($i = 1; $i <= ($range + 1); $i++){		  
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);
			}
		  } 
		  // When closer to the end
		  elseif($current_page >= ($max_page - ceil(($range/2)))){
			$extra = 0;
		  
			if (($max_page - ($max_page - $range)) < 2 ) $extra = 1;
		  
			for($i = $max_page - $range - $extra; $i <= $max_page; $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i); 
			}
		  }
		  // Somewhere in the middle
		  elseif($current_page >= $range && $current_page < ($max_page - ceil(($range/2)))){
			$extra = 0;
			if ($current_page - ceil($range/2) == 0 ) $extra = 1;
			
			for($i = ( $current_page - ceil($range/2) + $extra); $i <= ($current_page + ceil(($range/2))+$extra); $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);  
			}
		  }
		} 
		// Less pages than the range, no sliding effect needed
		else{
		  for($i = 1; $i <= $max_page; $i++){
			$data_pages['curent'] = $current_page;
			$data_pages['pages'][$i] =  get_pagenum_link($i);
		  }
		}

		// On the last page, don't put the Last page link
		if($current_page != $max_page){}
	  }
	   
		$src_nav = null;
		
		if (array_key_exists('pages', $data_pages)){
			$pos_curent = $data_pages['curent'];
			
			$src_nav.= '<ul class="pagination">';
				$count = 0;
				
				foreach($data_pages['pages'] as $key => $url){
					$count++;

					if($pos_curent == $key){
						$src_nav.= '<li class="current"><a href="#">'.$key.'</a></li>';
					}else{
						$src_nav.= '<li><a href="'.$url.'">'.$key.'</a></li>';
					}
				}
				
			$src_nav.= '</ul>';
		}
	  
		return $src_nav;
	
	}
	
	function ewf_sc_blog_navigation_steps( $range = 4, $query, $label_next = null, $label_prev = null ){
		$src_nav = null;
		$max_page = 0;
		
		$data_pages = array();
		
		$class_current = 'current';
		
		$current_page = $query->query_vars['paged'];
		if ($current_page == 0) { $current_page = 1; }
	
	  // How much pages do we have?
	  if ( !$max_page ) {
		$max_page = $query->max_num_pages;
	  }

	  // We need the pagination only if there are more than 1 page
	  if($max_page > 1){
	  
		if ( !$current_page ) 		{ $current_page = 1; }
		if ( $current_page != 1 )	{ }
		
		if($max_page > $range){
		  // When closer to the beginning
		  if($current_page < $range){
			for($i = 1; $i <= ($range + 1); $i++){		  
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);
			}
		  } 
		  // When closer to the end
		  elseif($current_page >= ($max_page - ceil(($range/2)))){
			$extra = 0;
		  
			if (($max_page - ($max_page - $range)) < 2 ) $extra = 1;
		  
			for($i = $max_page - $range - $extra; $i <= $max_page; $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i); 
			}
		  }
		  // Somewhere in the middle
		  elseif($current_page >= $range && $current_page < ($max_page - ceil(($range/2)))){
			$extra = 0;
			if ($current_page - ceil($range/2) == 0 ) $extra = 1;
			
			for($i = ( $current_page - ceil($range/2) + $extra); $i <= ($current_page + ceil(($range/2))+$extra); $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);  
			}
		  }
		} 
		// Less pages than the range, no sliding effect needed
		else{
		  for($i = 1; $i <= $max_page; $i++){
			$data_pages['curent'] = $current_page;
			$data_pages['pages'][$i] =  get_pagenum_link($i);
		  }
		}

		// On the last page, don't put the Last page link
		if($current_page != $max_page){}
	  }
	   
		$src_nav = null;
		$last_nav = false;
		
		$label_prev = get_option(EWF_SETUP_THNAME."_blog_older", __('Older Posts', 'bitpub')); 
		$label_next = get_option(EWF_SETUP_THNAME."_blog_newer", __('Newer Posts', 'bitpub'));
	  
		$pos_curent = $data_pages['curent'];

		$src_nav.= '<div class="blog-navigation fixed">';
			if ($pos_curent < (count($data_pages['pages']))){
				$src_nav.= '<a href="'.$data_pages['pages'][$pos_curent+1].'">&#8249; '.$label_prev.'</a>';
				$last_nav = true;
			}

			if ($last_nav){
				$src_nav.= ' | ';
			}
			
			if ($pos_curent != 1){
				$src_nav.= '<a href="'.$data_pages['pages'][$pos_curent-1].'">'.$label_next.' &#8250;</a>';
			}
		$src_nav.= '</div>';	
	  
	  return $src_nav; 
	}
	
	function ewf_sc_blog_timelinePages( $atts, $content = null ){
		global $post;
		
		extract(shortcode_atts(array(
			"posts" => get_option('posts_per_page'),
			"categ_include" => null, 
			"categ_exclude" => null,
			"posts_exclude" => null,
			"layout"		=> "single", 
			"height"		=> "auto",
			"width" 		=> "auto",
			"date"			=> "true",
			"info"			=> "true",
			"nav"			=> "true",
			"sidebar"		=> "false",
			"page"			=> 2,
			"style" 		=> "featured",
			"template"		=> "default"	#	default | columns | timeline
		), $atts));
		

		# Build the query
		#
		$wp_query_blog = new WP_Query(array( 'post_type' => 'post', 'posts_per_page' => $posts, 'orderby'=>'date', 'order'=>'DESC','paged' =>$page ));
		
		
		
		$ewf_blog_items = array();
		
		# Timeline template
		#
			// $ewf_vars 	= array( 'count' => 0, 'last' => 'left', 'pair' => null, 'src_left' => null, 'src_right' => null );
		
			// echo '<div class="ewf-row">';			
			while ($wp_query_blog->have_posts()) : $wp_query_blog->the_post();

				// if ($ewf_vars['row-items'] == 0){
					// echo '<div class="ewf-row">';			
				// }

				
			#	Prepare internal variables
			// #
				// $ewf_vars['count']++;
				// $ewf_vars['row-items']++;

				// if ($ewf_vars['last'] == 'left'){
					ob_start();
						get_template_part('templates/blog-columns-full');
					
					$ewf_blog_items[] = ob_get_clean();
					
					
					// $ewf_vars['last'] = 'right';
				// }else{
					// ob_start();
					// get_template_part('templates/blog-columns-full');
					
					// $ewf_blog_items[] = ob_get_clean();
					// $ewf_vars['last'] = 'left';
				// }

				
			endwhile;
			// echo '</div>';
			
			wp_reset_postdata();

			return $ewf_blog_items;
	}	

	function ewf_sc_blog_overview( $atts, $content = null ){
		global $post;
		
		extract(shortcode_atts(array(
			"posts" => get_option('posts_per_page'),
			"readmorelabel" => get_option(EWF_SETUP_THNAME."_blog_read_more", __('&#8212; Read More', 'bitpub')),
			"categ_include" => null, 
			"categ_exclude" => null,
			"posts_exclude" => null,
			"layout"		=> "single", 
			"height"		=> "auto",
			"width" 		=> "auto",
			"date"			=> "true",
			"info"			=> "true",
			"nav"			=> "true",
			"sidebar"		=> "false",
			"style" 		=> "featured",
			"template"		=> "default"	#	default | columns | timeline
		), $atts));
		
		wp_reset_postdata();
		
		
		$src = null;
		$ewf_paged 	= get_query_var('paged') ? get_query_var('paged') : 1;
		$ewf_page_template = get_post_meta($post->ID,'_wp_page_template',TRUE);
	
		# Build the query
		#
		$wp_query_blog = new WP_Query(array( 'post_type' => 'post', 'posts_per_page' => $posts, 'orderby'=>'date', 'order'=>'DESC','paged' =>$ewf_paged ));
		
		
		

		
		ob_start();

		# Default blog template
		#
		if ($template == 'default'){
			$ewf_vars 	= array( 'count' => 0, 'pair' => null );
			
			while ($wp_query_blog->have_posts()) : $wp_query_blog->the_post();
				
			#	Prepare internal variables
			#
				$ewf_vars['count']++;
				$ewf_vars['pair'] = $ewf_vars['count'] % 2;			
			
			
			#	Load template
			#
				get_template_part('templates/blog-item-default');
				
			endwhile;
			
		}
			
		
		
		
		
		# 2 Columns full width template
		#
		if ($template == 'columns'){
			$ewf_vars 	= array( 'count' => 0, 'per-row' => 2, 'row-items' => 0, 'close-item-separator' => false, 'last-row' => false, 'pair' => null );
		
			// echo '<div class="ewf-row">';			
			while ($wp_query_blog->have_posts()) : $wp_query_blog->the_post();

				if ($ewf_vars['row-items'] == 0){
					echo '<div class="ewf-row">';			
				}

				
			#	Prepare internal variables
			#
				$ewf_vars['count']++;
				$ewf_vars['row-items']++;

				
				
				if ( (($wp_query_blog->post_count - ($wp_query_blog->current_post)) == $ewf_vars['per-row']) && $ewf_vars['close-item-separator'] ) {
					$ewf_vars['last-row'] = true;
					$ewf_vars['close-item-separator'] = false;
				}else{
					$ewf_vars['close-item-separator'] = true;
				}
				
				// DEBUG
				//
				// echo '<pre>';
					// print_r($ewf_vars);
					// print_r($wp_query_blog);
					// echo '<br/>Post count:'.$wp_query_blog->post_count;
					// echo '<br/>Post curremt:'.$wp_query_blog->current_post;
				// echo '</pre>';
			

				
			#	Load template
			#
				echo '<div class="ewf-span6">';			
					get_template_part('templates/blog-columns-full');
					
					// if ($ewf_vars['last-row'] != true || $ewf_vars['close-item-separator'] == false ){
						// echo '<div class="divider single-line"></div>';
					// }
				echo '</div>';
				
				
				if ($ewf_vars['row-items'] == 2 || ($wp_query_blog->post_count == ($wp_query_blog->current_post+1)) ){
					echo '</div>';
					
					$ewf_vars['close-item-separator'] = true;
				}
				
				if ($ewf_vars['row-items'] == $ewf_vars['per-row']){
					$ewf_vars['row-items'] = 0;
				}
				
			endwhile;
			// echo '</div>';
		
			wp_reset_postdata();
		}
		
		$src .= ob_get_clean();
		
		
		
		# Timeline template
		#
		if ($template == 'timeline'){
			$ewf_vars 	= array( 'count' => 0, 'last' => 'left', 'pair' => null, 'src_left' => null, 'src_right' => null );
				
			while ($wp_query_blog->have_posts()) : $wp_query_blog->the_post();

			#	Prepare internal variables
			#
				$ewf_vars['count']++;
				$ewf_vars['row-items']++;

				if ($ewf_vars['last'] == 'left'){
					ob_start();
					get_template_part('templates/blog-columns-full');
					
					
					$ewf_vars['src_left'] .= ob_get_clean();
					$ewf_vars['last'] = 'right';
				}else{
					ob_start();
					get_template_part('templates/blog-columns-full');
					
					$ewf_vars['src_right'] .=  ob_get_clean();
					$ewf_vars['last'] = 'left';
				}
				
				
			endwhile;
			
			
			ob_start();
			
			echo '<div class="timeline fixed">';
				
				echo '<div class="left-side">';
					echo $ewf_vars['src_left'];
				echo '</div>';
				
				echo '<div class="separator"></div>';
				
				echo '<div class="right-side">';
					echo $ewf_vars['src_right'];
				echo '</div>';
				
			echo '</div>';
			
			
			
			echo '<div class="ewf-row timeline-nav">';
            	echo '<div class="ewf-span12">';
                    echo '<a href="#" class="read-more" data-page="1">More</a>';
                echo '</div><!-- end .span12 -->';
            echo '</div>';
			
			$src .= ob_get_clean();
			
		}

		
		#	Load navigation
		#		
		if ($nav == 'true' && $template != 'timeline'){
			if ($wp_query_blog->max_num_pages > 1){
				$src .= ewf_sc_blog_navigation_pages(4, $wp_query_blog); 
			}
		}
			
			
		return $src;
	}	

	function ewf_comments( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		switch ( $comment->comment_type ) :
			case '' :
		?>
		<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
			<div class="comment-body <?php  if (!get_option('show_avatars')){ echo 'no-avatar'; } ?>">
				
				<div class="comment-author vcard" >
					<?php
	
					if (get_option('show_avatars')){
						echo get_avatar( $comment, 80 ); 
					}
					
					?>
					
					<cite class="fn">
						<?php
							
							$comment_url = get_comment_link();
							echo '<a class="url" rel="external nofollow" href="'.$comment_url.'">'.get_comment_author().'</a>';
						?>
					</cite>
					
					<span class="says"><?php  echo __( 'says', 'bitpub' );  ?>:</span>
				</div>
					
				<div class="comment-meta commentmetadata">
					<a href="<?php comment_link(); ?>">
					<?php 
					
						comment_date();
						echo ' ' . __( 'at', 'bitpub' ) . ' ';
						comment_time();
					?>
					</a>
				</div>
				
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'bitpub' ); ?></em>
				<?php endif; ?>
				
				<?php comment_text(); ?>
				
				<!-- get_edit_comment_link();  -->
				
				
				<div class="reply">
					<?php  
						
						comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );  
						echo ' ';
						edit_comment_link();
						
					?>
				</div>
			</div> 
		<?php
				break;

			case 'pingback'  :
			case 'trackback' :
		?>
		
		<li class="post pingback">
			<p><?php _e( 'Pingback:', 'bitpub' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('Edit', 'bitpub'), ' ' ); ?></p>
		<?php
				break;
		endswitch;
	}
	
	
	
	
	#	Affect archive widget structure
	add_filter('get_archives_link', 'ewf_archive_filter'); 
	
	function ewf_archive_filter($links) {
		$links = str_replace('</a>&nbsp;(', ' (', $links);
		$links = str_replace(')', ')</a>', $links);
		
		return $links;
	}
	
	

	add_filter('wp_list_categories','ewf_categories_filter');
		
	function ewf_categories_filter ($variable) {
		$variable = str_replace('</a> (', ' (', $variable);
		$variable = str_replace(')', ')</a>', $variable);
	   return $variable;
	}


?>