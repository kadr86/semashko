<?php

	if ( ! isset( $content_width ) ) $content_width = 940;


#
#	Add and ID column to pages & taxonomy
#	
#	add_theme_support('ewf-editor-columnID');
#

	if (current_theme_supports('ewf-editor-columnID')){
				
			add_action('admin_init'		, 'ewf_add_id_column');

			function ewf_add_id_column() {
				add_action('admin_head', 'ewf_add_id_column_css');

				add_filter('manage_posts_columns', 'ewf_add_id_column_source');
				add_action('manage_posts_custom_column', 'ewf_add_id_column_value', 10, 2);

				add_filter('manage_pages_columns', 'ewf_add_id_column_source');
				add_action('manage_pages_custom_column', 'ewf_add_id_column_value', 10, 2);

				foreach ( get_taxonomies() as $taxonomy ) {
					add_action("manage_edit-${taxonomy}_columns", 'ewf_add_id_column_source');			
					add_filter("manage_${taxonomy}_custom_column", 'ewf_add_id_column_value_return', 10, 3);
				}
			}
			
			function ewf_add_id_column_source($cols) {
				$cols['item-id'] = '<span>ID</span>'; 
				return $cols;
			} 

			function ewf_add_id_column_value($column_name, $id) { 
				if ($column_name == 'item-id') echo intval($id);
			}

			function ewf_add_id_column_value_return($value, $column_name, $id) {
				if ($column_name == 'item-id') $value = $id;
				return $value;
			}

			function ewf_add_id_column_css() {
				echo ' <style type="text/css"> #item-id { width: 50px; }</style>';
			}

	}

	

	
	#	Detect if this is a blank page template
	#	
	function ewf_is_page_blank(){
		global $post;
		
		$page_blank = false;
		
		if (!empty($post) && is_singular()){
			$page_template = get_post_meta( $post->ID, '_wp_page_template', true );
			
			if ($page_template == 'page-blank.php') {
				return true;
			}
		}

		return $page_blank;
	}
	


	#	Get Attachment details
	#
	function ewf_get_attachment( $attachment_id ) {

		$attachment = get_post( $attachment_id );
		return array(
			'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
			'caption' => $attachment->post_excerpt,
			'description' => $attachment->post_content,
			'href' => get_permalink( $attachment->ID ),
			'src' => $attachment->guid,
			'title' => $attachment->post_title
		);
	}
	

	
		
	function ewf_excerpt_max($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo wp_kses_post(mb_substr( $subex, 0, $excut ));
			} else {
				echo wp_kses_post($subex);
			}
			echo '...';
		} else {
			echo wp_kses_post($excerpt);
		}
	}
	

	
#	Add debug on/off to admin toolbar
#
	add_action( 'wp_before_admin_bar_render', 'ewf_admin_bar_debug');

	function ewf_admin_bar_debug() {
		global $wp_admin_bar;
		
		if(is_admin() && get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true'){
		
			$parent = 'EWF_DEBUG';
			$debug_url = null;
			$debug_state = null;
			
			if (get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true'){
				$debug_url = '?ewf-debug-off';
				// $debug_state = 'OFF';
			}else{
				$debug_url = '?ewf-debug';
				// $debug_state = 'ON';
			}
			
			$wp_admin_bar->add_menu( array(
				'parent' => false,
				'id' => $parent,
				'title' =>  'Debug'.$debug_state,
				'href'  => admin_url($debug_url),
				'meta'  => array(
					'title' => 'debug',
					)
			));

			
			$wp_admin_bar->add_menu( array(
				'parent' => $parent,
				'id' => 'ewf-debug-header',
				'title' =>  'Page Header',
				'href'  =>  '#',
				'meta'  => array(
					'title' => 'debug',
					)
			));
			
		}

	}
	
	
	
	add_action( 'wp_footer', 'ewf_debug_show',1);
	
	function ewf_debug_show(){
		global $ewf_theme_debug;
		
		if (get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true' && is_user_logged_in()){
			echo '<pre class="ewf-debug-message">';
				print_r($ewf_theme_debug);
			echo '</pre>';
		}
		
	}
	
	
	function ewf_debug_active(){
		if (get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true' && is_user_logged_in()){
			return true;
		}else{
			return false;
		}
	}
	
	
	function ewf_debug($array_data, $array_source = null, $group = false){
		global $ewf_theme_debug;
		
		if ($group){
			$ewf_theme_debug[$array_source][] = $array_data;
		}else{
			$ewf_theme_debug[$array_source] = $array_data;
		}
	}
	
	
	function ewf_debug_log($debug, $message){ 
		#
		# ewf_message($message);
		#
		apply_filters($debug, $message);
	}
	
	
	
	
	function ewf_debugging_mode_notices() {
		//
		// For resolving customer support on site without actually making changes to the theme enable ewf-debug and show some basic information about the setup itself
		//
		
		if (array_key_exists('ewf-debug', $_REQUEST)){
			update_option(EWF_SETUP_THNAME."_debug_mode", 'true');
		}
		
		if (array_key_exists('ewf-debug-off', $_REQUEST)){
			update_option(EWF_SETUP_THNAME."_debug_mode", 'false');
		}
		
	}
	
	add_action( 'admin_notices', 'ewf_debugging_mode_notices' );
	
	
	
#	Remove Revolution slider update notice
#

	if(function_exists( 'set_revslider_as_theme' )){
		add_action( 'init', 'ewf_revslider_remove_notices' );

		function ewf_revslider_remove_notices() {
			set_revslider_as_theme(); 
		}
	}
	


#	Check if composer is active on a page content
#
	function ewf_get_rgb($hex) {
	   $hex = str_replace("#", "", $hex);

	   if(strlen($hex) == 3) {
		  $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		  $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		  $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
		  $r = hexdec(substr($hex,0,2));
		  $g = hexdec(substr($hex,2,2));
		  $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	
	   return $rgb; // returns an array with the rgb values
	}
	
	
	
	/*
	//	Identify the blog page template
	//
	
	$page_custom = get_post_meta($page_data['page']);
	if (array_key_exists('_wp_page_template', $page_custom)){
			$page_custom_template = $page_custom['_wp_page_template'][0];
		switch($page_custom_template){
			case 'page-blog-default.php':
				$ewf_extra_attr .= ' template="default" ';
				break;
				
			case 'page-blog-columns.php':
				$ewf_extra_attr .= ' template="columns" ';
				break;			
		}
	}else{
		$ewf_extra_attr .= ' template="default" ';
	}
	
	*/
	
	
	
	function ewf_get_page_settings($file){
		global $wp_query, $ewf_theme_layout;
		
		$page_data = array();
	

		$page_data['page']				= ewf_get_page_related();

		$page_layout 					= ewf_get_page_layout( $page_data['page']['type-layout'], $page_data['page']['id']);
		$page_sidebar 					= ewf_get_sidebar_id($page_data['page']['type-sidebar'], $page_data['page']['id'], true);
		
		$page_data['sidebar'] 			= $page_sidebar['sidebar'];
		$page_data['layout'] 			= $page_layout['layout'];
		
		$page_data['spans'] 			= ewf_get_page_spans( $page_data['page']['type'] );

		$page_data['file'] 				= $file;
		
		
		#	Debug loading process
		#
		$page_data['page']['debug'][] = '[' . $page_layout['debug']['post'] . '] ' . $page_layout['debug']['layout'];
		$page_data['page']['debug'][] = '[' . $page_sidebar['debug']['post'] . '] ' . $page_sidebar['debug']['sidebar'];

		
		#	Load search results templates 
		#
		if ($page_data['page']['type-sub'] == 'search'){
			switch($page_data['page']['type']){
				case 'page':
					$page_data['layout'] = 'layout-full-site';
					$page_data['template'] = 'templates/search-item-default';
					break;
				
				case 'blog':
					$page_data['template'] 	= 'templates/blog-item-default';
					break;				
			}
		}
		
		
		
		#	Load page message on archive & search page
		#
		ewf_get_page_message($page_data);

		
		
		#	Debug page data
		#
		ewf_debug($page_data, 'page-settings');
		
		
		return $page_data;
	}
	
	
	#	Get page spans based on page type: blog / shop / page
	#
	function ewf_get_page_spans($page_type = null, $custom_sidebar_size = null){
		global $ewf_theme_options_defaults;
		
		$sidebar_string = get_option(EWF_SETUP_THNAME.'_' . $page_type . '_sidebar_size', $ewf_theme_options_defaults[ $page_type.'_sidebar_size' ]);
		$sidebar_spans = explode(',', $sidebar_string);
		
		return array( 'content' => $sidebar_spans[0], 'sidebar' => $sidebar_spans[1]);
	}
	
	
	#	Get page info message based on page template eg: Archive Categories, Archive Tags, Search results
	#
	function ewf_get_page_message(&$page_data){
		global $wp_query;
		
		$message = null;
		$term = null;
		
		switch($page_data['page']['type-sub']){
			
			case 'archive-category':
				$categ_id = get_query_var('cat');
				$categ = get_category($categ_id, false);
				
				$message = __('Viewing posts categorised under: %s', 'bitpub');
				$term = ucfirst($categ->name);
				break;
			
			
			case 'archive-tag':
				$message = __('Showing posts tagged with: %s', 'bitpub');
				$term = $wp_query->queried_object->name;
				break;
				
				
			case 'archive-date':
				$message = __('Viewing posts from: %s', 'bitpub');
				
				if (get_query_var('monthnum').get_query_var('year') != null ){
					$tmp_date = '1'.'-'.get_query_var('monthnum').'-'.get_query_var('year');
					$term = date('F Y' ,strtotime($tmp_date));					
				}
				
				if (get_query_var('m') != null ){
					$tmp_year = substr(get_query_var('m'), 0, 4);
					$tmp_month = substr(get_query_var('m'), 5, 7);
					
					$tmp_date = '1'.'-'.$tmp_month.'-'.$tmp_year;
					$term = date('F Y' ,strtotime($tmp_date));					
				}
				break;

			
			case 'search':
				$term = $wp_query->query_vars['s'];
				
				if ($wp_query->found_posts == 0){
					$message = __('The search for %s returned no results.', 'bitpub');
				}else{
					$message = __('Search results for: %s', 'bitpub');
				}
				break;
		}
		
		if ($message){
			$page_data['info'] = '<div class="alert info">' . sprintf($message, '<strong>'.$term.'</strong>') . '</div>';
		}
	}
	

	# 	It get's the related ID, returns the following
	# 	- Page ID if this is a single page
	# 	- Blog page ID from admin reading settings if this is a blog post or an archive page
	# 	- Parent page ID if this is a child page
	#
	function ewf_get_page_related( $headerCheck = false, $page_for_posts = 0 ){
		global $post, $wp_query, $ewf_theme_layout;
		
		$page_data = array(
			'debug' => array(), 
			'id' => 0, 
			'type' => null,
			'type-sub' => null,
			'type-layout' => null,
			'type-sidebar' => null,
			'page-posts' => get_option('page_for_posts', 0),
			'page-front' => get_option('page_on_front', 0),
			
		);		
		
		$ewf_page_id = 0;
		 
		##	Check if there's a blog page ID provided
		##
		if (!$page_for_posts){
			$page_for_posts = get_option('page_for_posts');
		}
		
		
		if (is_object($post)){
			$page_data['debug'][] = "Post Object";
			$page_data['id'] = $post->ID;
		}
		
		if (is_home() && !$page_data['page-posts']  && !$page_data['page-posts']){
			$page_data['debug'][] = "Automatic home detected";
			
			$page_data['type'] = 'blog';
			$page_data['type-sub'] = 'default-index';
		}
		
		
		if (function_exists('is_shop') && is_product()){
			$page_data['id'] = woocommerce_get_page_id('shop');
		}elseif (is_single()){
			$page_data['debug'][] = "Detected: Single";
			
			$ewf_page_data = null;			
			switch($post->post_type){

				case "post":
					#$ewf_page_data = get_page_by_title( get_option(EWF_SETUP_THNAME."_page_blog", null ) );
					$ewf_page_data = get_post($page_for_posts);
					
					$page_data['debug'][] = "Detected: Post Single";
					$page_data['type'] = 'blog';
					$page_data['type-sub'] = 'single';
					break;
			
				case EWF_PROJECTS_SLUG:				
					$ewf_page_data = get_post($post->ID);
					
					$page_data['debug'][] = "Detected: Portfolio Single";
					$page_data['type'] = 'page';
					$page_data['type-sub'] = 'parent';
					
					$page_data['composer'] = (count(explode('[/vc_row]', $post->post_content))-1);
					
					break;		
			}
			
			if (is_object($ewf_page_data)){
				$page_data['id'] = $ewf_page_data->ID;
			}	

			
		}elseif(function_exists('is_shop') && (is_shop() || is_product_category() || is_product_tag() || is_woocommerce() || is_checkout() || is_account_page() || is_cart() || is_product() )){
			
			$page_data['debug'][] = "Detected: Shop";
			$page_data['type'] = 'shop';
			$page_data['id'] = woocommerce_get_page_id('shop');
			
			$page_content = get_post($page_data['id']);
			
			if (is_shop()) { $page_data['type-sub'] = 'shop'; }
			if (is_cart()) { $page_data['type-sub'] = 'cart'; }
			if (is_checkout()) { $page_data['type-sub'] = 'checkout'; }
			if (is_product()) { $page_data['type-sub'] = 'product'; }
			if (is_account_page()) { $page_data['type-sub'] = 'account'; }
			if (is_product_category()) { $page_data['type-sub'] = 'category'; }
			if (is_product_tag()) { $page_data['type-sub'] = 'tag'; }					
			
			$page_data['composer'] = (count(explode('[/vc_row]', $page_content->post_content))-1);
			
		}elseif(is_page()){
		 
			$page_data['type'] = 'page';
			$page_data['debug'][] = "Detected: Page";
			
			if (count($post->ancestors)){
				$page_data['debug'][] = "Detected: Child Page";
				$page_data['id-parent'] = $post->ancestors[0];
				$page_data['type-sub'] = 'child';
			}else{
				$page_data['debug'][] = "Detected: Page";
				$page_data['type-sub'] = 'parent';
			}

			$page_data['composer'] = (count(explode('[/vc_row]', $post->post_content))-1);
			
			## Check if the child's parent has a header image
			##
			if ($headerCheck){
				$ewf_child_page_parent_imgID = ewf_getHeaderImageID($ewf_page_id);
				$ewf_child_page_imgID = ewf_getHeaderImageID($post->ID);
				
				$page_data['debug'][] = "Parent does not have header image, returning child ID [".$ewf_child_page_parent_imgID.']['.$ewf_child_page_imgID.']';
				
				 
				## If the child have an image but the parent also has one, return the child ID
				##
				if ($ewf_child_page_imgID > 0){
					$ewf_page_id = $post->ID;
					
					$page_data['debug'][] = "Parent does have header image, but child also has one, returning child ID [".$ewf_child_page_parent_imgID.']['.$ewf_child_page_imgID.']';
				}
				
				
				## If the parent does not have an image niether the child return null
				if ( $ewf_child_page_parent_imgID == 0 &&  $ewf_child_page_imgID == 0){
					$ewf_page_id = 0;
				}
			}
			
		}elseif(is_404()){
			
			$page_data['debug'][] = "Detected: 404 Page";
			$page_data['id'] = get_option(EWF_SETUP_THNAME."_page_404", 0);
			$page_data['type'] = 'page';
			$page_data['type-sub'] = '404';
			

			$page_content = get_post($page_data['page']['id']);
			$page_data['composer'] = (count(explode('[/vc_row]', $page_content->post_content))-1);
			
			if (!$page_data['id']){
					$page_data['debug'][] = "Page not selected on theme options";
			}
				
		}elseif(is_archive()){
			$page_data['debug'][] = "Detected: Archive";
			
			if (is_tax(EWF_PROJECTS_TAX_SERVICES)){
				// $ewf_page_data = get_page_by_title( get_option(EWF_SETUP_THNAME."_page_portfolio", null ) );
				$page_data['debug'][] = "Archive Taxonomy: EWF_PROJECTS_SLUG";
			}else{
			
				$ewf_page_data = get_post($page_for_posts);
				
				$page_data['type'] = 'blog';
				$page_data['type-sub'] = 'archive';

				
				if ($wp_query->is_category == 1 && $wp_query->is_archive == 1){
					$page_data['type-sub'] = 'archive-category';
				}

				if ($wp_query->is_tag == 1 && $wp_query->is_archive == 1) {
					$page_data['type-sub'] = 'archive-tag';
				}
				
				if ($wp_query->is_category == null && $wp_query->is_archive == 1 && $wp_query->is_month = 1 && $wp_query->is_tag == 0 ) {
					$page_data['type-sub'] = 'archive-date';
				}


				$page_data['debug'][] = "Detected: Blog Archive - Return Blog Page ID";
				
			}
			
			if (is_object($ewf_page_data)){
				$page_data['id'] = $ewf_page_data->ID;
			}else{
				$page_data['id'] = 0;
			}

		}elseif(is_search()){
			$page_data['debug'][] = "Detected: Search";
			
			if ($page_for_posts){
				$ewf_page_data = get_post($page_for_posts);
			
				if (is_object($ewf_page_data)){

					$page_data['type'] = 'blog';
					$page_data['type-sub'] = 'search';
				
					if (!empty($_GET['post_type'])){
						$page_data['type'] = 'blog';
					}else{
						$page_data['type'] = 'page';
					}
				
					$page_data['id'] = $ewf_page_data->ID;
					$page_data['debug'][] = "Search Page - Return Blog Page ID: ".$ewf_page_data->ID;
				}
				
			}else{
					$page_data['type'] = 'blog';
					$page_data['type-sub'] = 'search';

					$page_data['debug'][] = "Search Page - No blog page ID";
			}
			
		}elseif(is_home() && is_front_page() == false && $page_data['page-posts']){
			$ewf_page_data = get_post($page_for_posts);
			
			if (is_object($ewf_page_data)){
				
				$page_data['id'] = $ewf_page_data->ID; 
				$page_data['type'] = 'blog';
				$page_data['type-sub'] = 'home';
				$page_data['debug'][] = "Static Posts Page ID: ".$ewf_page_data->ID;
				
			}
		}
		
		$page_data['type-layout'] = $ewf_theme_layout['layout']['types'][ $page_data['type'] ]['layout'];
		$page_data['type-sidebar'] = $ewf_theme_layout['layout']['types'][ $page_data['type'] ]['sidebar'];
		
		// ewf_debug($page_data, 'ewf_get_page_related');
		
		return $page_data;
	}
	
		
		
	if (!function_exists('array_replace_recursive'))
	{
		
		function ewf_array_recurse($array, $array1)
		{
		  foreach ($array1 as $key => $value)
		  {
			// create new key in $array, if it is empty or not an array
			if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key])))
			{
			  $array[$key] = array();
			}
	  
			// overwrite the value in the base array
			if (is_array($value))
			{
			  $value = ewf_array_recurse($array[$key], $value);
			}
			$array[$key] = $value;
		  }
		  return $array;
		}
		
	  function array_replace_recursive($array, $array1){
	  
		// handle the arguments, merge one by one
		$args = func_get_args();
		$array = $args[0];
		if (!is_array($array))
		{
		  return $array;
		}
		for ($i = 1; $i < count($args); $i++)
		{
		  if (is_array($args[$i]))
		  {
			$array = ewf_array_recurse($array, $args[$i]);
		  }
		}
		return $array;
	  }
	}
	

	/*
	if (!function_exists('is_post_type')){
	
		function is_post_type($type = null){
			global $post;
			
			if (get_post_type($post) == strtolower($type)){
				return true;
			}else{
				return false;
			}
		}
	}
	*/
?>