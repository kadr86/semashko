<?php

#	Default image sizes

	add_image_size( 'ewf-header-logo'	,260 	,9999	,false );	
	set_post_thumbnail_size( 1140, 99999, true ); 
	

#	Default theme constants
	define( 'EWF_SETUP_PAGE'			, 'functions.php');										# page containing set up
	define( 'EWF_SETUP_THNAME'			, 'bitpub');											# theme options short name
	define( 'EWF_SETUP_TITLE'			, 'Throne WordPress');									# wordpress menu title
	define( 'EWF_SETUP_THEME_NAME'		, 'Throne WordPress');									# wordpress menu title
	define( 'EWF_SETUP_VC_GROUP'		, __('Throne Elements', 'bitpub'));		# visual composer group elements
	define( 'EWF_SETUP_THEME_VERSION'	, '1.0.1');												# theme version
	
	
	#	Check for the plugin init
	if (!defined('EWF_PROJECTS_SLUG')){
		
		#	Change default slugs
		define ('EWF_PROJECTS_SLUG'					, 'project'	);
		define ('EWF_PROJECTS_TAX_SERVICES'			, 'service'	);
		
	}

	
#	Theme debug array 
#
	$ewf_theme_debug = array();
	

#	Theme options default values
#	
	$ewf_theme_options_defaults = array(
		
		// General typography
		'body_font_size' 				=> '14px',
		'body_font_lineheight' 			=> '24px',
		'body_font_margin' 				=> '0px',
	
	
		// Custom typography
        'h1_font_size' 					=> '48px',
		'h1_font_lineheight' 			=> '64px',
		'h1_font_margin' 				=> '14px',
		
		'h2_font_size' 					=> '36px',
		'h2_font_lineheight' 			=> '48px',
		'h2_font_margin' 				=> '12px',
		
		'h3_font_size' 					=> '30px',
		'h3_font_lineheight' 			=> '40px',
		'h3_font_margin' 				=> '10px',
		
		'h4_font_size' 					=> '24px',
		'h4_font_lineheight' 			=> '32px',
		'h4_font_margin' 				=> '8px',
		
		'h5_font_size' 					=> '18px',
		'h5_font_lineheight' 			=> '24px',
		'h5_font_margin' 				=> '6px',
		
		'h6_font_size' 					=> '14px',
		'h6_font_lineheight' 			=> '18px',
		'h6_font_margin' 				=> '4px',
		
		
		// Colors
		'color_accent_01'				=> '#98d4ec',
		// 'color_accent_02'			=> '#5c5a69',
		'color_content_font'			=> '#7b868c',
		'color_header_font'				=> '#28353e',
		
		'color_darkgrey'				=> '#d7d7d7',
		'color_lightgrey'				=> '#f3f3f3',
		
		'color_menu_font'				=> '#7b868c',
		// 'color_menu_background' 		=> '#313947',
		// 'color_menu_font_hover'		=> '#5c5a69',
		// 'color_menu_font_selected'	=> '#5c5a69',
				
		'color_ms_tile'					=> '#98d4ec',	
		'color_android_theme'			=> '#98d4ec',	
		
		// 'color_menu_drop_background'	=> '#7a7885',
		// 'color_menu_drop_font'		=> '#ffffff',
		// 'color_menu_drop_font_hover'	=> '#d7d7d7',
		// 'color_menu_drop_divider'	=> '#d7d7d7',	
		
		// 'color_footer_background'	=> '#7A7885',	
		// 'color_footer_font'			=> '#ffffff',	
		// 'color_footer_divider'		=> '#ffffff',	
		
		// 'color_footer_top'			=> '#4FB7FF',
		// 'color_footer_top_font'		=> '#ffffff',
		
		// 'color_footer_bottom'		=> '#0D131B',
		// 'color_footer_bottom_font'	=> '#ffffff',
				

		'content_width'					=> '1500px',
		
		
		// Sidebar sizes
		'page_sidebar_size'				=> '9,3',
		'blog_sidebar_size'				=> '9,3',
		
		
		// Footer Columns
		'footer_columns'				=> '3,5,4',
		'footer_bottom_columns'			=> '6,6',
		// 'footer_top_columns'			=> '12',
		
		
		// Header settings
		'header_height'					=> '86px',
		'header_height_sticky'			=> '79px',
		
		
		// Shop settings
		'shop_items'					=> '12',
		'shop_sidebar_size'				=> '8,4',
	
		// Header settings
		'header_action'					=> 'false',
		
	);

	

#
#	Page header settings
#
#	Features: image, background-color, font-color, border-color, master, description, icon, templates, parallax
#	Defaults: 
#		active 				=> *			(* - auto / 1 - active / 0 - disalbed)
#		image_id 			=> 0			(integer - image id default 0);
#		image_url 			=> null			(string - default blank)
#		icon 				=> null			(string - icon class ex: ifc-home)
#		title 				=> null			(string - default blank)
#		description 		=> null			(string - default blank)
#		background_color 	=> #000000		(hexazecimal value)
#		border_color 		=> #ffffff		(hexazecimal value)
#		font_color 			=> #ffffff		(hexazecimal value)
#		parallax 			=> 0			(0 - disabled / 1 - enabled)
#		master_id 			=> 0			(0 - disabled / 1 - enabled)
#		master_use 			=> 0			(0 - disabled / 1 - enabled)
#

	$ewf_theme_header = array(
		'support' 	=> array( 'page', EWF_PROJECTS_SLUG),
		'features' 	=> array( 'image', 'background-color', 'font-color', 'border-color' , 'master', 'title'),
		'defaults'	=> array(
			'background_color' 	=> '#f3f3f3',
			'font_color' 		=> '#7b868c',
			'border_color' 		=> '#d7d7d7',
		)
	);
	
	
	

			
	$ewf_theme_footer = array();
	
	if (get_option(EWF_SETUP_THNAME."_footer_section", "true") === "true"){
		$ewf_theme_footer['footer'] = explode(',', get_option(EWF_SETUP_THNAME."_footer_columns", $ewf_theme_options_defaults['footer_columns']));
	}
	
	if (get_option(EWF_SETUP_THNAME."_footer_bottom", "true") === "true"){
		$ewf_theme_footer['footer-bottom'] = explode(',', get_option(EWF_SETUP_THNAME."_footer_bottom_columns", $ewf_theme_options_defaults['footer_bottom_columns']));
	}
	
	

	
#
#	Page layout & sidebars settings
#	
	$ewf_theme_layout = array(
		'support' 	=> array( 'page', EWF_PROJECTS_SLUG ),
		'layout' 	=> array(
			'default' => 'layout-full',		
			'types'  => array(
			
				'blog' => array(
					'layout' 	=> 'layout-sidebar-single-right',
					'sidebar' 	=> 'sidebar-page'
				),
				'page' => array(
					'layout' 	=> 'layout-full',
					'sidebar' 	=> 'sidebar-page'
				),		
				'shop' => array(
					'layout' 	=> 'layout-full',
					'sidebar' 	=> 'sidebar-page'
				)
			)
		),
		'sidebars' 	=> array(
			
			#	Fallback sidebar - when you delete the current sidebar this is the default one
			'default' => 'sidebar-page',
		
		
			#	The list with uneditable sidebars
			'install' => array(
				'sidebar-blog'	=> array( 'title' => __('Blog Sidebar'			, 'bitpub'), 'description' => null ),
				// 'sidebar-shop'			=> array( 'title' => __('WooCommerce Sidebar'	, 'bitpub'), 'description' => null ),
			),
			
			
			#	Theme's default sidebars
			'theme' => array(	
				// 'header-left' 			=> array('title' => __('Header widget area left','bitpub'), 	'description' 	=> __('In the header left column','bitpub')),
				// 'header-right' 			=> array('title' => __('Header widget area right','bitpub'), 	'description' 	=> __('In the header right column','bitpub')),
				
				// 'footer-widgets-top-1' 	=> array('title' => __('Footer top widget area 1','bitpub'), 'description' 	=> __('The section located above footer','bitpub')),
				// 'footer-widgets-top-2' 	=> array('title' => __('Footer top widget area 2','bitpub'), 'description' 	=> __('The section located above footer','bitpub')),
				// 'footer-widgets-top-3' 	=> array('title' => __('Footer top widget area 3','bitpub'), 'description' 	=> __('The section located above footer','bitpub')),
				// 'footer-widgets-top-4' 	=> array('title' => __('Footer top widget area 4','bitpub'), 'description' 	=> __('The section located above footer','bitpub')),
				
				'footer-widgets-mid-1' => array('title' => __('Footer widget area 1', 'bitpub'), 'description' 		=> __('In the footer the left column','bitpub')),
				'footer-widgets-mid-2' => array('title' => __('Footer widget area 2', 'bitpub'), 'description' 		=> __('In the footer the left center column','bitpub')),
				'footer-widgets-mid-3' => array('title' => __('Footer widget area 3', 'bitpub'), 'description' 		=> __('In the footer the right center column','bitpub')),
				'footer-widgets-mid-4' => array('title' => __('Footer widget area 4', 'bitpub'), 'description' 		=> __('In the footer the right column','bitpub')),
				
				'footer-widgets-bot-1' => array('title' => __('Footer bottom widget area 1','bitpub'), 'description'   => __('The section located below footer','bitpub')),
				'footer-widgets-bot-2' => array('title' => __('Footer bottom widget area 2','bitpub'), 'description'   => __('The section located below footer','bitpub')),
				'footer-widgets-bot-3' => array('title' => __('Footer bottom widget area 3','bitpub'), 'description'   => __('The section located below footer','bitpub')),
				'footer-widgets-bot-4' => array('title' => __('Footer bottom widget area 4','bitpub'), 'description'   => __('The section located below footer','bitpub')),
			)
		),
		
		'widget' => array(
			'before_title'  => '<h6 class="widget-title">',
			'after_title'   => '</h6>',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>'
		)
	);
	
	
	
		