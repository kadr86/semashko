<?php

	add_shortcode( 'ewf_slider_testimonials', 'ewf_vc_slider_testimonials' );

	
	function ewf_vc_slider_testimonials( $atts, $content ) {
		extract( shortcode_atts( array(
			'background' => '#F6F1ED'
		), $atts ) ); 
		
		ob_start();
		
		echo '<div class="testimonial-slider"><div class="slides">';
			echo do_shortcode($content);
		echo '</div></div>';
		
		echo '<div class="slider-control text-center">';
			echo '<span id="prev-testimonial"></span>';
			echo '<span id="next-testimonial"></span>';
		echo '</div><!-- end .slider-control -->';
		
		return ob_get_clean();
	}
	
	
	vc_map( array(
		"name" => __("Testimonials slider", 'bitpub'),
		"base" => "ewf_slider_testimonials",
		"as_parent" => array('only' => 'ewf-testimonial, ewf-testimonial-adv'),
		"content_element" => true,
		"category" => EWF_SETUP_VC_GROUP,
		"icon" => "icon-wpb-ewf-slider-testimonials",
		"description" => __("Creates a carousel displaying a list of testimonials", 'bitpub'),  
		"show_settings_on_create" => false,
		"params" => array(
			
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'bitpub'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'bitpub')
			),
		),
		"js_view" => 'VcColumnView'
	) );
	
	
	
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_ewf_slider_testimonials extends WPBakeryShortCodesContainer {
		}
	}


?>