<?php


	add_shortcode( 'ewf-pricing-item', 'ewf_vc_pricing_item' );
	
	
	function ewf_vc_pricing_item( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'plan' => __('Starter edition','bitpub'),
		  'cost' => '30',
		  'currency' => '$',
		  'description' => '&nbsp;',
		  'division' => '99',
		  'icon' => '',
		  'options' => '',
		  'period' => 'month',
		  'link' => '#',
		  'css' => ''
	   ), $atts ) );
	 
		$link = vc_build_link($link); 
		$class_extra = ' '.$css;
	 
		ob_start();
		
		
		
		echo '<div class="pricing-table'.$class_extra.'">';
			
			echo '<div class="pricing-table-header">';
			
				echo '<h3>'.$plan.'</h3>';
				
				// if ($description){
					// echo '<h6>'.$description.'</h6>';
				// }
			
				// if ($icon){
					// echo '<i class="'.$icon.'"></i>';
				// }
				
		    echo '<h1>'; 
					if ($currency){
						echo '<sup>'.$currency.'</sup> ';
					}
					
					echo $cost;
					
					if ($period){ 
						echo ' <sub>/ '.$period.'</sub>';					
					}
			echo '</h1>';
				
			echo '</div><!-- end .pricing-table-header -->';
				
			
			echo '<div class="pricing-table-offer">';
			echo '<ul>';
				$all_options = explode (',', $options);
				
				foreach($all_options as $index => $option_item){
					echo '<li>'.$option_item.'</li>';
				}
				
			echo '</ul>';
			echo '</div>';
			
			if ($link['title'] != ''){
				echo '<a class="btn" href="'.$link['url'].'">'.$link['title'].' >>'.'</a>';
			}
			
		echo '</div>';

		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Pricing Table", 'bitpub'),
	   "base" => "ewf-pricing-item",
	   "class" => "",
	   "icon" => "icon-wpb-ewf-pricing-item",
	   // "description" => __("Shows a pricing table row", 'bitpub'),  
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Plan name", 'bitpub'),
			 "param_name" => "plan",
			 "value" => __('Starter edition','bitpub'),
			 // "description" => __("The final value will animate to, from 0 to the number provided by you", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Plan description", 'bitpub'),
			 "param_name" => "description",
			 "value" => '',
			 // "description" => __("The final value will animate to, from 0 to the number provided by you", 'bitpub')
		  ),
		  // array(
			 // "type" => "ewf-icon",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Select Icon", 'bitpub'),
			 // "param_name" => "icon",
			 // "value" => '',
			 // "description" => __("Select the icon you want to have asside plan name", 'bitpub')
		  // ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Cost", 'bitpub'),
			 "param_name" => "cost",
			 "value" => '30',
			 // "description" => __("The final value will animate to, from 0 to the number provided by you", 'bitpub')
		  ),
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Division", 'bitpub'),
			 // "param_name" => "division",
			 // "value" => '99',
			 // "description" => __("The final value will animate to, from 0 to the number provided by you", 'bitpub')
		  // ),	
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Currency", 'bitpub'),
			 "param_name" => "currency",
			 "value" => '$',
			 // "description" => __("The final value will animate to, from 0 to the number provided by you", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Period", 'bitpub'),
			 "param_name" => "period",
			 "value" => __('month','bitpub')
			 // "description" => __("The final value will animate to, from 0 to the number provided by you", 'bitpub')
		  ),		  
		  array(
			 "type" => "exploded_textarea",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Plan features (add one per line)", 'bitpub'),
			 "param_name" => "options",
			 "value" => ''
			 // "description" => __("The final value will animate to, from 0 to the number provided by you", 'bitpub')
		  ),
		  array(
			 "type" => "vc_link",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Read more button", 'bitpub'),
			 "param_name" => "link",
			 "value" => '#',
			 "description" => __("Specify an optional link pointing to a details page", 'bitpub') 
		  ),
		  array( 
				"type" => "textfield", 
				"holder" => "div", 
				"class" => "", 
				"heading" => __("Extra CSS Class", 'bitpub'), 
				"param_name" => "css", 
				"value" => '', 
				"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)
	   )
	));



?>