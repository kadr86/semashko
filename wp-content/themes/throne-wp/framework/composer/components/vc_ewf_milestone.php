<?php

	add_shortcode( 'ewf-milestone', 'ewf_vc_milestone' );
	
	
	function ewf_vc_milestone( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'title' 		=> null,
		  'style' 		=> 'ewf-milestone-style-1',
		  'number' 		=> '1',
		  'icon' 		=> null,
		  'description' => null,
		  'symbol' 		=> null,
		  'speed' 		=> '2000',
		  'css' 		=> null
	   ), $atts ));
	 
		$number = intval($number);
		$class_extra = ' '.$css;
	 
		ob_start();
		
		echo '<div class="milestone'.$class_extra.'">';
			if ($icon){
				echo '<i class="'.$icon.'"></i>';
			}
		
			echo '<div class="milestone-content">';
				
				
				echo '<div class="milestone-description">';
					if ($title){
						echo $title;
					}
				echo '</div><!-- end .milestone-description -->';
					

				echo '<div class="milestone-value" data-speed="'.$speed.'" data-stop="'.$number.'">';
 
					// if ($symbol){
						// echo '<span> '.$symbol.'</span>';				
					// }
				
				echo '</div>';
			echo '</div>';
		echo '</div>';
		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Milestone", 'bitpub'),
	   "base" => "ewf-milestone",
	   "class" => "",
	   "icon" => "icon-wpb-ewf-milestone",
	   "description" => __("Shows milestones and numeric statistic with animated numbers", 'bitpub'),  
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
		  // array(
			 // "type" => "ewf-image-select",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Milestone Style", 'bitpub'),
			 // "param_name" => "style",
			 // "options" => array(
				// 'Style 1' => 'ewf-milestone-style-1', 
				// 'Style 2' => 'ewf-milestone-style-2', 
				// 'Style 3' => 'ewf-milestone-style-3', 
			 // ),
			 // "value" => 'ewf-milestone-style-1'
		  // ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", 'bitpub'),
			 "param_name" => "title",
			 "value" => __("Title", 'bitpub'),
			 "description" => __("The milestone title", 'bitpub')
		  ),
		  // array(
			 // "type" => "textarea",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Details", 'bitpub'),
			 // "param_name" => "description",
			 // "value" => null,
			 // "description" => __("The description of the milestone", 'bitpub')
		  // ),
		  array(
			 "type" => "ewf-icon",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Select Icon", 'bitpub'),
			 "param_name" => "icon",
			 "value" => null,
			 "description" => __("Select the icon you want to have at the top of the section", 'bitpub')
		  ),
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Symbol (optional)", 'bitpub'),
			 // "param_name" => "symbol",
			 // "value" => null,
			 // "description" => __("Add an extra letter aside the milestone number, ex: %, #", 'bitpub')
		  // ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Numeric value", 'bitpub'),
			 "param_name" => "number",
			 "value" => 1,
			 "description" => __("Numeric value representing the milestone", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Speed", 'bitpub'),
			 "param_name" => "speed",
			 "value" => 2000,
			 "description" => __("Specify the animation speed", 'bitpub')
		  ),
		  array( 
				"type" => "textfield", 
				"holder" => "div", 
				"class" => "", 
				"heading" => __("Extra CSS Class", 'bitpub'), 
				"param_name" => "css", 
				"value" => '', 
				"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)
	   )
	));


?>