<?php
	
	
	#	Register image size for project
	#
	add_image_size( 'ewf-portfolio-strip', 720, 720, true);
	
	
	#	Register shortcode for Visual Composer component
	#
	add_shortcode( 'ewf-portfolio-strip', 'ewf_vc_portfolio_strip' );
	
	add_action('init', 'ewf_register_vc_portfolio_strip');
	
	
	function ewf_vc_portfolio_strip( $atts, $content ) {
		global $post;
		
		$options = shortcode_atts( array(
			"items" 				=> 5,
			"id" 					=> null,
			"exclude" 				=> null,
			"order" 				=> "DESC",
			"details" 				=> 'excerpt',
			"list" 					=> "latest",
			"service" 				=> null,
			"css" 					=> null
		), $atts);
		extract($options);

		$class_extra = ' '.$css;
		
		
		if (get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true'){
			echo '<pre><strong>Portfolio Strip</strong><br/>';
				// print_r($atts);
				print_r($options);
			echo '</pre>';
		}
		
		
		ob_start();
		
		
		$wp_portfolio_query = ewf_helper_query_builder($options);
		$wp_portfolio = new WP_Query($wp_portfolio_query);
		
		while ($wp_portfolio->have_posts()) : $wp_portfolio->the_post();
			global $post;
			
			
			#	Get post extra info
			$post_extra = ewf_helper_get_post_extra($post, 'ewf-portfolio-strip', EWF_PROJECTS_TAX_SERVICES);
		
			
						
				echo '<div class="item">';	
					echo '<div class="portfolio-item">';	
						
						echo '<div class="portfolio-item-preview">';	
					
							if ($post_extra['image']) {
								echo '<img src="'.$post_extra['image-thumb'].'" alt="" >';
							}
						
							echo '<div class="portfolio-item-overlay">';
								echo '<div class="portfolio-item-description">';
							  
										echo '<h5><a href="'.get_permalink().'">'.get_the_title().'</a></h5>';
										echo '<p>' . get_the_excerpt() .'</p>';
										echo '<a class="btn" href="'.get_permalink().'">' . __('View', 'bitpub') . '</a>';
										
								echo '</div><!-- end .portfolio-item-description -->';
							echo '</div><!-- end .portfolio-item-overlay -->';
														
						echo '</div><!-- end .portfolio-item-preview -->';
				
				echo '</div><!-- end .portfolio-item -->';
				echo '</div>';
			
		endwhile;
		
		wp_reset_postdata();
							
		// echo '<div class="portfolio-item-overlay-actions">
				// <a class="portfolio-item-zoom magnificPopup-gallery" href="'.$image_preview_large[0].'"><span>+</span></a>
				// <a class="portfolio-item-link" href="'.get_permalink().'"><span>&gt;</span></a>
			// </div><!-- end .portfolio-item-overlay-actions -->';		
		
		return '<div class="portfolio-strip fixed'.$class_extra.'">' . ob_get_clean() . '</div> <!-- end .portfolio-strip -->';
	 
	}
		
	
	function ewf_vc_portfolio_strip_services(){
		global $ewf_portfolioStrip_services;
		
		$services = array();	// array('All Services');
		$terms = get_terms (EWF_PROJECTS_TAX_SERVICES); 
	
		if (is_array($terms)){
			foreach($terms as $key => $service){

				$services[] = $service->slug;
			}
		}
  		
		return $services;
	}
		
	
	
	function ewf_register_vc_portfolio_strip(){
		global $ewf_composer_class, $ewf_composer_class_field;
		
		vc_map( array(
			"name" => __("Portfolio Strip", 'bitpub'),
			"base" => "ewf-portfolio-strip",
			"class" => "",
			"icon" => "icon-wpb-ewf-portfolio-strip",
			"description" => __("Add a full width row with porftolio items", 'bitpub'), 
			"category" => EWF_SETUP_VC_GROUP,
			"params" => array(
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Load 5 portfolio items", 'bitpub'),
					"param_name" => "list",
					"value" => array(
					__('Latest projects', 'bitpub') => 'latest',
					__('From a service', 'bitpub') => 'service', 
					__('Random', 'bitpub') => 'random', 
					),
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Service", 'bitpub'),
					"param_name" => "service",
					"value" => ewf_vc_portfolio_strip_services(),
					"description" => __("Specify projects from a defined category", 'bitpub'),
					"dependency" => Array("element" => "list","value" => array("service"))
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Projects order", 'bitpub'),
					"param_name" => "order",
					"value" => array( 
					__("Descendent", 'bitpub')  => 'DESC', 
					__("Ascendent", 'bitpub') => 'ASC',
					__("Random", 'bitpub') => 'rand',
					),
					"dependency" => Array("element" => "list","value" => array('service')),
					"description" => __("Load projects in a Ascendent(1,2,3), Descendent (3,2,1) order or random.", 'bitpub')
				),
				array( 
					"type" => "textfield", 
					"holder" => "div", 
					"class" => "", 
					"heading" => __("Extra CSS Class", 'bitpub'), 
					"param_name" => "css", 
					"value" => '', 
					"description" => __("Add and extra CSS class to the component", 'bitpub') 
				)
			  // array(
				// "type" => "dropdown",
				// "holder" => "div",
				// "class" => "",
				// "heading" => __("Secondary info", 'bitpub'),
				// "param_name" => "details",
				// "value" => array(
					// __('Project Excerpt', 'bitpub') => 'excerpt',
					// __('Project Services', 'bitpub') => 'services', 
					// __("Don't show", 'bitpub') => 'none', 
				// ),
				// "description" => __("Specify what to show below the project title", 'bitpub'),
			  // ),
		   )
		));
			
	}


?>