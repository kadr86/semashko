<?php

	add_shortcode( 'ewf-service', 'ewf_vc_service' );
	
	
	function ewf_vc_service( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'title' 		=> __('Sample title', 'bitpub'),
		  'image_id' 		=> 0,
		  'image_url' 		=> '#',
		  'link' 			=> '#',
		  'css' 			=> null
	   ), $atts ) );
	   
	   $link = vc_build_link($link); 
	   $class_extra = ' '.$css;
	   
		if ($image_id){
			$image_url = wp_get_attachment_image_src($image_id, 'large'); 
			$image_url = $image_url[0]; 
		}
	   
		ob_start();
		
		echo '<div class="service-box'.$class_extra.'">';				
				
				echo '<div class="service-box-thumb">';

					if ($image_id){
						echo '<img src="'.$image_url.'" alt="'.$image_id.'" />';
					}
				
				echo '</div><!-- end .service-box-thumb -->';
				
				
				if ($title){
					echo '<h2><strong><a href="#">'.$title.'</a></strong></h2>';
				}
							
				if ($content){
					echo '<p>'.$content.'</p>';
				}
				
				if ($link['title'] != ''){
					echo '<a class="btn btn-white" href="'.$link['url'].'">'.$link['title'].'</a>';
				}
            
        echo '</div><!-- end .service-box -->';
		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Service", 'bitpub'),
	   "base" => "ewf-service",
	   "class" => "",
	   "category" => EWF_SETUP_VC_GROUP,
	   "icon" => "icon-wpb-ewf-service",
	   "description" => __("Name, image and description", 'bitpub'),  
	   "params" => array(
		  array(
			 "type" => "attach_image",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Image", 'bitpub'),
			 "param_name" => "image_id",
			 "description" => __("Add the image of the service", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", 'bitpub'),
			 "param_name" => "title",
			 "value" => __('Sample Title', 'bitpub'),
			 "description" => __("Specify the name of the service", 'bitpub')
		  ),
		  array(
			 "type" => "textarea",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Details", 'bitpub'),
			 "param_name" => "content",
			 "value" => null,
			 "description" => __("The content of the service", 'bitpub')
		  ),
		  array(
			 "type" => "vc_link",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Read more link", 'bitpub'),
			 "param_name" => "link",
			 "value" => '#',
			 "description" => __("Specify an optional link to another page", 'bitpub') 
		  ),
	   )
	));


?>