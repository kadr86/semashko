<?php

	//	Register actions
	//
	add_action ( "wp_ajax_ewf_portfolio_isogrid_loadmore"							, "ewf_portfolio_isogrid_loadMore" );
	add_action ( "wp_ajax_nopriv_ewf_portfolio_isogrid_loadmore"					, "ewf_portfolio_isogrid_loadMore" );
	
	
	//	Register shortcode
	//
	add_shortcode ( "portfolio-isogrid" 											, "ewf_projects_isogrid" );
	
	
	//	Register image size
	//
	add_image_size( 'ewf-portfolio-filtrable'											, 720, 720, true);
	
	
	
	add_action('wp_enqueue_scripts'													, 'ewf_load_isogrid_includes');
	function ewf_load_isogrid_includes(){
		
		//  Plugin - Isotope
		wp_enqueue_script('ewf-vc-isogrid'											, get_template_directory_uri().'/framework/composer/components/vc_ewf_portfolio_filtrable/vc_ewf_portfolio_filtrable.js', array('plugins-js'), null , true );
		
	}
	
	//	.portfolio-strip.columns-3 li { width: 33.3333333333%; }
	//	.portfolio-strip.columns-4 li { width: 25%; }
	
	
	
	function ewf_portfolio_isogrid_loadMore(){
		$filter = stripslashes($_POST['filter']);
		 
		$options = array( 
			'title' 	=> __("Portfolio", 'bitpub'),
			'items' 	=> -1,
			'service' 	=> null,
			'style' 	=> 'ewf-portfolio-style-1',
			'start' 	=> 0,
			'unlimited' => true,
			'loaditems' => intval($_POST['items']),
			'exclude' 	=> $_POST['exclude']
			
		);
		
		if ($filter != 'all'){
			$options['service'] = array($filter);
		}

		$result = ewf_projects_isogrid( $options, null, false, false, true );
		
		wp_send_json_success(array('filter' => $filter, 'source' => $result['source'], 'builder'=>$options));
		exit;
	}
	
	
	function ewf_projects_isogrid($atts, $content, $filters = true, $wrapper = true, $return_array = false){
		global $post;
		
		$options = shortcode_atts(array( 
			"unlimited" 		=> true, 
			"title" 			=> null,
			"button_title" 		=> __('Load more', 'bitpub'),
			"button_alignment"	=> 'text-center', 
			"items" 			=> 6, 
			"start" 			=> 0, 
			"display" 			=> 'all', 
			"all_label" 		=> __('All', 'bitpub'),
			"load_more" 		=> 'true',
			"style" 			=> 'ewf-portfolio-style-1', 
			"service" 			=> null, 
			"exclude" 			=> null, 
			"loaditems" 		=> 3,
			"columns"			=> 3,
			'css' 				=> null
		), $atts ); 
		extract($options); 
		
		$class_extra = ' '.$css;
		
		$src = null;
	
	
	//	Build WP Query
	//		
		$query_projects = ewf_hlp_queryBuilder($options);
		$wp_query_project = new WP_Query($query_projects);
		$_ewf_span = 3;

		
		switch($columns){
			case '3': $_ewf_span = '4';break;
			case '4': $_ewf_span = '3';break;
		}
		
		if ((get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true') && !$return_array){
			echo '<pre>';
				print_r($atts);
				print_r($options);
				print_r($query_projects); 
				echo '<br/>Items:'.$items;
			echo '</pre>';
		}

		
	//	Load items using WP Query
	//			
		$count_items = 0; 
		
		while ($wp_query_project->have_posts()) : $wp_query_project->the_post();
		
			$image_large_preview = null;
			$image_extra_large_preview = null;

			$count_items++;
			
			
			//	Get project terms
			//				
			$project_terms = wp_get_post_terms ($post->ID, EWF_PROJECTS_TAX_SERVICES);
			$project_terms_src = null;
			$project_services = null;
			
			foreach($project_terms as $key => $service){
				$project_terms_src .= $service->slug.' ';
					
				if ($project_services == null){
					$project_services .= $service->name;
					
					// break; // Stop at the first category
				}else{
					$project_services .= ', '.$service->name;
				}
			}

			
			//	Get the featured image
			//
			$project_image_id = get_post_thumbnail_id();  
			$project_image_src = null;
			if ($project_image_id) {
				$image_large_preview = wp_get_attachment_image_src($project_image_id,'ewf-portfolio-filtrable');  
				$image_extra_large_preview = wp_get_attachment_image_src($project_image_id,'large');   
				$project_image_src = '<img src="'.$image_large_preview[0].'" alt="" />';  
			}
			
			
			
			
			
			ob_start();
			
			
			switch ($style){
			
				
				//	Style Isotope Grid
				//
					case 'ewf-portfolio-style-1';
					echo 
					'<div data-id="'.$post->ID.'" class="item '.$project_terms_src.'" data-terms="'.$project_services.'">
						
						<div class="portfolio-item">
							
							<div class="portfolio-item-preview">
								 '.$project_image_src.'
								
								<div class="portfolio-item-overlay">
											
									<div class="portfolio-item-description">
										
										<h5><a href="' . get_permalink() . '">'.get_the_title().'</a></h5>
										
										<p>' . get_the_excerpt() . '</p>
										
										<a class="btn" href="'.get_permalink().'">' . __('View project', 'bitpub') . '</a>
										
									</div><!-- end .portfolio-item-description -->									
								
								</div>
								
							</div><!-- end .portfolio-item-preview -->   

						</div> <!-- end .portfolio-item -->
												
					</div>';
					break;
				
				// <a class="portfolio-item-zoom magnificPopup-gallery" href="'.$image_extra_large_preview[0].'"> </a>
				// <h5>'.$project_services.'</h5>
				// <div class="portfolio-item-overlay-actions">
					// <a class="portfolio-item-zoom magnificPopup-gallery" href="'.$image_extra_large_preview[0].'"><span>+</span></a>
					// <a class="portfolio-item-link" href="'.get_permalink().'"><span>&gt;</span></a>
				// </div><!-- end .portfolio-item-overlay-actions -->
			
			
			}
			
			$src .= ob_get_clean();
			
			
			if ($return_array){
				if ($count_items == $loaditems){
					break;
				}
			}else{
				if ($count_items == $items){ 
					break;
				}	
			}

			
		endwhile;
		
		wp_reset_postdata();
	
		
		
		
		
	//	Display portfolio
	//
		if ($wrapper){
			$src = '<div class="wpb_row vc_row"><div class="span12"><div class="portfolio-items portfolio-columns tree-cols portfolio-isotope gutter columns-'.$columns.' fixed'.$class_extra.'" data-display="'.$display.'" data-style="'.$style.'">'.$src.'</div></div></div>';
			
			
			
			
			if ($load_more == 'true'){
				$src .= '<div class="vc_row wpb_row">
							<div class="span12 '.$button_alignment.'">
								<a href="#" class="btn btn-large portfolio-load-more" data-display="'.$display.'" data-style="'.$style.'"  data-load-items="'.$loaditems.'"><span>'.$button_title.'</span></a>
							</div><!-- end .span12 -->
						</div>';
			}
		 
		
	
		//	Attach filters & title
		//
		
			$src_header = null;
			$filter_src = null;
			
			// if ($display )
			
			if ($filters == true){
				$filter_terms 	= get_terms (EWF_PROJECTS_TAX_SERVICES);
				$filter_items 	= array('all'=>0);
				$filter_css_class = null;
				
				if (is_array($filter_terms)){
					$filter_src.= '<ul>';			
						$filter_src.= '<li><a class="active" href="#" data-items="'.$wp_query_project->found_posts.'" data-filter="*" data-term="all" >'.$all_label.'</a></li>';	
						
						foreach($filter_terms as $key => $service){
							$filter_src.= '<li><a data-term="'.$service->slug.'" data-items="'.intval($service->count).'" data-filter=".'.$service->slug.'" href="#">'.$service->name.'</a></li>';
						}
					$filter_src.= '</ul>';		
				}
			}
				
			
			
			$display_title = null;
			$display_filter = null;
			
			$show_title = false;
			$show_filter = false;
				
			if ($display != 'none'){
				switch($display) {
					
					case 'all':
						$display_title = '<h2>'.$title.'</h2>';
						$display_filter = '<div class="portfolio-filter">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = true;$show_filter = true;
						break;
					
					case 'title':
						$display_title = '<h2>'.$title.'</h2>';
						$display_filter = '<div class="portfolio-filter portfolio-filter-hidden">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = true;$show_filter = false;
						break;
					
					case 'title-center':
						$display_title = '<h2 class="text-center">'.$title.'</h2>';
						$display_filter = '<div class="portfolio-filter portfolio-filter-hidden">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = true;$show_filter = false;
						break;

					case 'no-filters':
						$display_title = null;
						$display_filter = '<div class="portfolio-filter portfolio-filter-hidden">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = false;$show_filter = false;
						break;
					
					case 'filters-left':
						$display_title = null;
						$display_filter = '<div class="portfolio-filter text-left">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = false;$show_filter = true;
						break;
						
					case 'filters-right':
						$display_title = null;
						$display_filter = '<div class="portfolio-filter text-right">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = false;$show_filter = true;
						break;
					
					case 'filters':
						$display_title = null;
						$display_filter = '<div class="portfolio-filter">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = false;$show_filter = true;
						break;

					case 'filters-center':
						$display_title = null;
						$display_filter = '<div class="portfolio-filter text-center">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = false;$show_filter = true;
						break;			
					
					case 'none':
						$display_title = null;
						$display_filter = '<div class="portfolio-filter portfolio-filter-hidden">'.$filter_src.'</div><!-- end .portfolio-filter -->';
						$show_title = false;$show_filter = false;
						break;
				}
			}
				
			
			if ($show_title || $show_filter){
				$src_header = '<div class="vc_row wpb_row title-filter" data-display="'.$display.'">';
				
					if ($show_title && !$show_filter || !$show_title && $show_filter){
						$src_header .= '<div class="span12">';
							$src_header .= $display_title;
							$src_header .= $display_filter;
						$src_header .= '</div>';
					}else{
						$src_header .=  '<div class="span3">';
							$src_header .= $display_title;
						$src_header .= '</div>';
						$src_header .= '<div class="span9 text-right">';
						
							$src_header .= $display_filter;
						 $src_header .= '</div>';
					}
					
				$src_header .= '</div>';
			}elseif(!$show_title || !$show_filter){
				$src_header = '<div class="vc_row wpb_row title-filter" data-display="'.$display.'" style="background:Red;">';
					$src_header .= $display_filter;
				 $src_header .= '</div>';
			}else{
				$src_header = '<div class="vc_row wpb_row title-filter no-all" data-display="'.$display.'">'.$display_filter.'</div>';
			}
		
		}
		
		if ($return_array){
			return array(
				'source' => $src, 
				// 'query' => $query_projects
			);
		}
	
		return '<div class="ewf-portfolio-isogrid-wrapper">'.$src_header.$src.'</div>';
	}

	
	vc_map( array(
	   "name" => __("Portfolio filtrable", 'bitpub'),
	   "base" => "portfolio-isogrid",
	   "class" => "",
	   "icon" => "icon-wpb-ewf-portfolio-isogrid",
	   "description" => __("Dynamically loads items in grid.Allows filtering by category", 'bitpub'), 
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
		// array(
			// "type" => "textfield", 
			// "holder" => "div",
			// "class" => "",
			// "heading" => __("Title", 'bitpub'),
			// "param_name" => "title",
			// "value" => __("Portfolio", 'bitpub'),
			// "description" => __("The title of the section.", 'bitpub')
		// ),
		array(
			"type" => "textfield", 
			"holder" => "div",
			"class" => "",
			"heading" => __("Initial items to show", 'bitpub'),
			"param_name" => "items",
			"value" => 6,
			"description" => __("The number of items to show initially", 'bitpub')
		),
		// array(
			// "type" => "dropdown",
			// "holder" => "div",
			// "class" => "",
			// "heading" => __("Prortfolio grid layout", 'bitpub'),
			// "param_name" => "columns",
			// "value" => array(
			// __('3 Columns', 'bitpub') => 3, 
			// __('4 Columns', 'bitpub') => 4, 
			// ),
		// ),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Display options", 'bitpub'),
			"param_name" => "display",
			"value" => array(
				// __("Title & Filters"					, 'bitpub') 	=> 'all', 
				// __("Title only, no filters"			, 'bitpub') 	=> 'title', 
				// __("Title only center, no filters"	, 'bitpub') 	=> 'title-center', 
				__("Filters on left"					, 'bitpub') 	=> 'filters-left', 
				__("Filters on center"					, 'bitpub')	=> 'filters-center', 
				__("Filters on right"					, 'bitpub')	=> 'filters-right', 
				__("No Filters"							, 'bitpub')	=> 'no-filters', 
				// __("Don't show title and filters"	, 'bitpub')	=> 'none' 
			),
		),
		array(
			"type" => "textfield", 
			"holder" => "div",
			"class" => "",
			"heading" => __("'All' label caption", 'bitpub'),
			"param_name" => "all_label",
			"value" => __('All', 'bitpub'),
			"description" => __("The portfolio filter has All option that shows all portfolio items. use this to specify how it should be labled", 'bitpub')
		),
		// array(
			// "type" => "ewf-image-select",
			// "holder" => "div",
			// "class" => "",
			// "heading" => __("Portfolio Style", 'bitpub'),
			// "param_name" => "style",
			// "options" => array(
			// 'Grid' 		=> 'ewf-portfolio-style-1', 
			// ),
			// "value" => 'ewf-portfolio-style-1',
		// ),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Show 'Load More' button below the grid", 'bitpub'),
			"param_name" => "load_more",
			"value" => array(
				__("Show it"				, 'bitpub') => 'true', 
				__("Don't show"				, 'bitpub') => 'false', 

			),
		),
		array(
			"type" => "textfield", 
			"holder" => "div",
			"class" => "",
			"heading" => __("'Load more' button caption", 'bitpub'),
			"param_name" => "button_title",
			"value" => __('Load more', 'bitpub'),
			"description" => __("Portfolio items are added dinamically to the grid when the 'Load more' button is pressed. Use this to specify how it should be labled", 'bitpub')
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Load More Items", 'bitpub'),
			"param_name" => "loaditems",
			"value" => 3,
			"description" => __("Specify the number of items to dinamically add to the grid when you press the 'Load more' button.", 'bitpub')
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("'Load more' button alignment", 'bitpub'),
			"param_name" => "button_alignment",
			"value" => array(
				__("Center"				, 'bitpub') => 'text-center', 
				__("Left"				, 'bitpub') => 'text-left', 
				__("Right"				, 'bitpub') => 'text-right', 	
			),
			"description" => __("How should the Load More buton be positioned", 'bitpub') 
		),
		array(  
			"type" => "textfield", 
			"holder" => "div", 
			"class" => "", 
			"heading" => __("Extra CSS Class", 'bitpub'), 
			"param_name" => "css", 
			"value" => '', 
			"description" => __("Add and extra CSS class to the component", 'bitpub') 
		)
	   )
	   
	));


?>