<?php

	add_shortcode( 'ewf-map', 'ewf_vc_map' );
	
	
	function ewf_vc_map( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'caption' => null,
		  'zoom' 	=> 16,
		  'phone' 	=> null,
		  'fax' 	=> null,
		  'email' 	=> null,
		  'height' 	=> 600,
		  'style' 	=> 'ewf-map-style-1',
		  'address' => 'Empire State Building, 5th Avenue, New York, Statele Unite ale Americii',
		  'css' 	=> null
	   ), $atts ) );
	 
		ob_start();
		
		$class_extra = ' '.$css;
		
		switch($style){
			
			case 'ewf-map-style-1':
				echo '<div class="map"><div 	  class="google-map'.$class_extra.'"';
						echo 'data-address="'.$address.'" ';
						echo 'data-zoom="'.$zoom.'" ';
						echo 'data-height="'.$height.'" ';
						
						if ($caption){
							echo 'data-caption="'.$caption.'" ';
						}
						
					echo '></div></div><!-- end .map -->'; 
				break;
			
			case 'ewf-map-style-2':
				echo '<div class="map-wrapper">';
					echo '<div class="google-map map'.$class_extra.'" data-address="'.$address.'" data-zoom="'.$zoom.'" data-height="'.$height.'" data-caption="'.$caption.'"></div><!-- end .google-map -->'; 
					
					echo '<div class="row"> 
							<div class="span12">
							
								<div class="map-overlay">
									<div class="widget ewf_widget_contact_info">
										
										<ul>';
											
											if ($address) { 
												echo '<li><strong>Address:</strong> '.$address.'</li>'; 
												}
												
												
											if ($phone) { 
												echo '<li><strong>Phone:</strong> '.$phone.'</li>'; 
												}
												
												
											if ($fax) { 
												echo '<li><strong>Fax:</strong> '.$fax.'</li>'; 
												}
												
												
											if ($email) { 
												echo '<li><strong>Email:</strong> <a href="mailto:'.$email.'">'.$email.'</a></li>'; 
												}
										
									echo '</ul>
										
									</div><!-- end .ewf_widget_contact_info -->
								</div><!-- end .map-overlay -->
								
							</div><!-- end .span12 -->
						</div><!-- end .row -->                                            
						
					</div><!-- end .map-wrapper -->';
				break;
				
		}

		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Google Map", 'bitpub'),
	   "base" => "ewf-map",
	   "icon" => "icon-wpb-ewf-map",
	   "description" => null, 
	   "class" => "",
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
		  // array(
			 // "type" => "ewf-image-select",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Map Style", 'bitpub'),
			 // "param_name" => "style",
			 // "options" => array(
				// 'Style 1' => 'ewf-map-style-1', 
				// 'Style 2' => 'ewf-map-style-2', 
			 // ),
			 // "value" => 'ewf-map-style-1',
		  // ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Address", 'bitpub'),
			 "param_name" => "address",
			 "value" => '',
			 "description" => __("The address where the map is centered", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Pinpoint caption", 'bitpub'),
			 "param_name" => "caption",
			 "value" => null,
			 "description" => __("Specify text that will appear when you click on the pin", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Zoom", 'bitpub'),
			 "param_name" => "zoom",
			 "value" => 16,
			 "description" => __("Specify a zoom value between 5 and 20", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Map height", 'bitpub'),
			 "param_name" => "height",
			 "value" => 400,
			 "description" => __("Specify the height of the map", 'bitpub')
		  ),
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Phone", 'bitpub'),
			 // "param_name" => "phone",
			 // "value" => '',
			 // "description" => __("Specify a phone number", 'bitpub')
		  // ),
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Fax", 'bitpub'),
			 // "param_name" => "fax",
			 // "value" => '',
			 // "description" => __("Specify the fax number", 'bitpub')
		  // ),
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Email", 'bitpub'),
			 // "param_name" => "email",
			 // "value" => '',
			 // "description" => __("Specify an email address", 'bitpub')
		  // ),
		  array( 
			"type" => "textfield", 
			"holder" => "div", 
			"class" => "", 
			"heading" => __("Extra CSS Class", 'bitpub'), 
			"param_name" => "css", 
			"value" => '', 
			"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)
	   )
	));


?>