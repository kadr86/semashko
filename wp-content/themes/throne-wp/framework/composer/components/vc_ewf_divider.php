<?php

	add_shortcode( 'ewf-divider', 'ewf_vc_divider' );
	
	function ewf_vc_divider( $atts, $content ) {
		extract( shortcode_atts( array(
			'height' => 10,
			'separator' => null,
			'css' => null,
		), $atts ) );
	 		
		$height = intval($height);
		$style_class = $separator;
		
		if ($css){ $style_class .= ' '.$css; }

		// if ($height){
			// return '<div class="divider '.$style_class.'" style="clear:both;padding:'.$height.'px;">&nbsp;</div>';
		// }else{
			// return null;
		// }

		return '<div class="divider '.$style_class.'">&nbsp;</div>';
	}

	
	vc_map( array(
	   "name" => __("Divider", 'bitpub'),
	   "base" => "ewf-divider",
	   "class" => "",
	   "icon" => "icon-wpb-ewf-divider",
	   "description" => __("Use it to divide content", 'bitpub'),  
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
	   
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Dividing Height", 'bitpub'),
			 // "param_name" => "height",
			 // "value" => 10,
			 // "description" => __("Specify the height this block should have", 'bitpub')
		  // ),
		  
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Divider Style", 'bitpub'),
			 "param_name" => "separator",
			 "value" => array(
				__('Single line', 'bitpub') => 'single-line', 
				__('Double line', 'bitpub') => 'double-line', 
				// __('Dotted line', 'bitpub') => 'single-dotted', 
				// __('Double dotted line', 'bitpub') => 'double-dotted'
			),
			 "description" => __("Specify the type of the line", 'bitpub')
		  ),
		array( 
			"type" => "textfield", 
			"holder" => "div", 
			"class" => "", 
			"heading" => __("Extra CSS Class", 'bitpub'), 
			"param_name" => "css", 
			"value" => '', 
			"description" => __("Add and extra CSS class to the component", 'bitpub') 
		)
		  
	   )
	));

?>