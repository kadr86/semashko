<?php
	
	
	#	Register image size for project
	#
	add_image_size( 'ewf-portfolio-list', 720, 405, true);
	
	
	#	Register shortcode for Visual Composer component
	#
	add_shortcode( 'ewf-portfolio-list', 'ewf_vc_portfolio_list' );
	
	add_action('init', 'ewf_register_vc_portfolio_list');
	
	
	function ewf_vc_portfolio_list( $atts, $content ) {
		global $post;
		
		$options = shortcode_atts( array(
			"items" 				=> 4,
			"exclude" 				=> null,
			"columns" 				=> 2,
			"order" 				=> "DESC",
			"list" 					=> null,
			"service" 				=> null,
			"nav"					=> 0,
			"css" 					=> null
		), $atts);
		
	   extract($options);
				
		$columns = intval($columns);
		$class_extra = ' '.$css;
		
		$items = 0;
		$row_items = 0;
		$col_spans = 12 / $columns;		
		
		
		if (get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true'){
			echo '<pre><strong>Portfolio List</strong><br/>';
				print_r($options);
			echo '</pre>';
		}

		
		ob_start();
		
		$wp_portfolio_query = ewf_helper_query_builder($options);
		$wp_portfolio = new WP_Query($wp_portfolio_query);
		
		while ($wp_portfolio->have_posts()) : $wp_portfolio->the_post();
			global $post;
		
			
			#	Get post extra info
			$post_extra = ewf_helper_get_post_extra($post, 'ewf-portfolio-list', EWF_PROJECTS_TAX_SERVICES, array('term_before' => '<span>', 'term_after' => '</span>'));

			
			#	Split in columns
			// if ($row_items == 0){
				// echo '<div class="row">'; 
			// }
			// echo '<div class="span'.$col_spans.'">';
			
			$items++;
			$row_items++;
			
			
			echo '<div class="portfolio-item">';
				
				echo '
				<div class="row">
					<div class="span6">
						
						<div class="portfolio-item-preview">
							<img src="'. $post_extra['image-thumb'] . '" alt="" />
						</div><!-- end .portfolio-item-preview -->
						
					</div><!-- end .span6 -->
					<div class="span6">
						
						<div class="portfolio-item-description">	
							<h5><a href="'.get_permalink().'">'.get_the_title().'</a></h5>

							<p>' . get_the_excerpt() . '</p>
							<a class="btn" href="' . get_permalink() . '">'.__('View', 'bitpub').'</a>
						</div><!-- end .portfolio-item-description -->
						
					</div><!-- end .span6 -->
				</div><!-- end .row -->';
				
			echo '</div>';
			
			
			#	Split in columns
			// if ($row_items == $columns || $wp_portfolio->post_count == $items){
				// $row_items = 0;
				// echo '</div>';
			// }
			
		
		endwhile;
		
		wp_reset_postdata();
	
		if ($nav){
			echo '<div class="row">';
				echo '<div class="span12 fixed">';
					echo ewf_sc_portfolio_list_nav(5, $wp_portfolio);
				echo '</div>';
			echo '</div>';
		}
		
		return '<div class="ewf-portfolio-list '.$class_extra.'">'.ob_get_clean().'</div>';
	 
	}	
	
	function ewf_vc_portfolio_list_services(){
		global $ewf_portfolioStrip_services;
		
		$services = array();	// array('All Services');
		$terms = get_terms (EWF_PROJECTS_TAX_SERVICES); 
	
		if (is_array($terms)){
			foreach($terms as $key => $service){

				$services[] = $service->slug;
			}
		}
  		
		return $services;
	}
		
	function ewf_sc_portfolio_list_nav( $range = 5, $query){
		$src_nav = null;
		$max_page = 0;
		
		$data_pages = array();
		
		$class_current = 'current';
		$current_page = $query->query_vars['paged'];
		
		
		
		if ($current_page == 0) { 
			$current_page = 1; 
			}
	
	
	# 	How much pages do we have?
	
		if ( !$max_page ) {
			$max_page = $query->max_num_pages;
			}
			
			
			

	  // We need the pagination only if there are more than 1 page
	  if($max_page > 1){
	  
		if ( !$current_page ) 		{ $current_page = 1; }
		
		if($max_page > $range){
		  // When closer to the beginning
		  if($current_page < $range){
			for($i = 1; $i <= ($range + 1); $i++){		  
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);
			}
		  } 
		  // When closer to the end
		  elseif($current_page >= ($max_page - ceil(($range/2)))){
			$extra = 0;
		  
			if (($max_page - ($max_page - $range)) < 2 ) $extra = 1;
		  
			for($i = $max_page - $range - $extra; $i <= $max_page; $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i); 
			}
		  }
		  // Somewhere in the middle
		  elseif($current_page >= $range && $current_page < ($max_page - ceil(($range/2)))){
			$extra = 0;
			if ($current_page - ceil($range/2) == 0 ) $extra = 1;
			
			for($i = ( $current_page - ceil($range/2) + $extra); $i <= ($current_page + ceil(($range/2))+$extra); $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);  
			}
		  }
		} 
		// Less pages than the range, no sliding effect needed
		else{
		  for($i = 1; $i <= $max_page; $i++){
			$data_pages['curent'] = $current_page;
			$data_pages['pages'][$i] =  get_pagenum_link($i);
		  }
		}

		if($current_page != $max_page){}
	  }
	   
		$src_nav = null;
		
		if (array_key_exists('pages', $data_pages)){
			$pos_curent = $data_pages['curent'];
			
			$src_nav.= '<ul class="pagination">';
				$count = 0;
				
				foreach($data_pages['pages'] as $key => $url){
					$count++;

					if($pos_curent == $key){
						$src_nav.= '<li class="current"><a href="#">'.$key.'</a></li>';
					}else{
						$src_nav.= '<li><a href="'.$url.'">'.$key.'</a></li>';
					}
				}
				
			$src_nav.= '</ul>';
		}
	  
		return $src_nav;
	
	}
	
	function ewf_vc_portfolio_list_items(){
		$result = array();
	
		$query = array( 'post_type' => EWF_PROJECTS_SLUG,
						'order'=> 'DESC', 
						'orderby' => 'date',  
						'posts_per_page' => -1); 
						
		$wp_query_portfolio_strip = new WP_Query($query);
		while ($wp_query_portfolio_strip->have_posts()) : $wp_query_portfolio_strip->the_post();
			global $post;
			
			$result[get_the_title()] = $post->ID;
		endwhile;
		
		return $result;
	}
	
	function ewf_register_vc_portfolio_list(){
	
		vc_map( array(
		   "name" => __("Portfolio List", 'bitpub'),
		   "base" => "ewf-portfolio-list",
		   "class" => "",
		   "icon" => "icon-wpb-ewf-portfolio-list",
		   "description" => __("Show a list of portfolio items, pagination optional", 'bitpub'), 
		   "category" => EWF_SETUP_VC_GROUP,
		   "params" => array(
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Source of the portfolio items from the list", 'bitpub'),
					"param_name" => "list",
					"value" => array(
						__('Latest projects','bitpub')=>'latest',
						// __('Load a single project','bitpub')=>'single',
						__('From a service', 'bitpub')=>'service',
						__('Random order', 'bitpub') => 'random'
					)
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __("Number of portfolio items", 'bitpub'),
					"param_name" => "items",
					"value" => 3,
					"dependency" => Array("element" => "list","value" => array("service", "random", "latest")),
					"description" => __("Select the number of items the list should display. If Pagination is enabled this is the number of portfolio items per page. if pagination is disabled this is the total number of items", 'bitpub')
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Service", 'bitpub'),
					"param_name" => "service",
					"value" => ewf_vc_portfolio_list_services(),
					"description" => __("Specify projects from a defined category", 'bitpub'),
					"dependency" => Array("element" => "list","value" => array("service"))
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Projects order", 'bitpub'),
					"param_name" => "order",
					"value" => array('DESC', 'ASC'),
					"dependency" => Array("element" => "list","value" => array("service")),
					"description" => __("Load projects in a Ascendent(1,2,3) or Descendent (3,2,1) order", 'bitpub')
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Portfolio list layout", 'bitpub'),
					"param_name" => "columns",
					"value" => array(
					// __('2 Columns', 'bitpub') => 2, 
					__('1 Column', 'bitpub') => 1
					),
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Enable pagination?", 'bitpub'),
					"param_name" => "nav",
					"value" => array(
					__('Yes', 'bitpub') => 1, 
					__('No', 'bitpub') => 0, 
					),
				),
				array( 
					"type" => "textfield", 
					"holder" => "div", 
					"class" => "", 
					"heading" => __("Extra CSS Class", 'bitpub'), 
					"param_name" => "css", 
					"value" => '', 
					"description" => __("Add and extra CSS class to the component", 'bitpub') 
				)
			)
		));
	
	}


?>