<?php

	add_shortcode( 'ewf_processv_group', 'ewf_vc_process_vertical_group' );
	add_shortcode( 'ewf_processv_item', 'ewf_vc_process_vertical_item' );


	function ewf_vc_process_vertical_group( $atts, $content ) {
		return '<ul class="vertical-process-builder fixed">'.do_shortcode($content).'</ul>';
	}

	function ewf_vc_process_vertical_item( $atts, $content ) {
		$shortcode_options = shortcode_atts( array(
			'title' => __('Sample title', 'bitpub'),
			'style' => null,
			'number' => null,
			'icon' => null,
		), $atts );
		
		extract($shortcode_options);
	 
		ob_start();
		
		echo '<li>';
			
			if ($style == 'icon' && $icon){
				echo '<i class="'.$icon.'"></i>';
			}elseif($style == 'number' && $number){
				echo '<h2>'.$number.'</h2>';
			}
			
			echo '<div class="process-description">';
				echo '<p>'.$title.'</p>';
			echo '</div>';
		echo '</li>';
		
		return ob_get_clean();
	}
	
	
	vc_map( array(
		"name" => __("Process Vertical", 'bitpub'),
		"base" => "ewf_processv_group",
		"as_parent" => array('only' => 'ewf_processv_item'),
		"content_element" => true,
		"icon" => "icon-wpb-ewf-process-vertical",
		"description" => __("Create a vertical list with items", 'bitpub'),  
		"show_settings_on_create" => false,
		"params" => array(
			
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'bitpub'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'bitpub')
			)
		),
		"js_view" => 'VcColumnView'
	) );
	
	
	
	vc_map( array(
		"name" => __("Process Vertical Item", 'bitpub'),
		"base" => "ewf_processv_item",
		"icon" => "icon-wpb-ewf-processv-item",
		"content_element" => true,
		"as_child" => array('only' => 'ewf_processh_group'),
		"show_settings_on_create" => true, 
		"params" => array(
		  array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Circle content", 'bitpub'),
			"param_name" => "style",
			"value" => array(
				__('Icon', 'bitpub') => 'icon', 
				__('Number', 'bitpub') => 'number'
			),
			"description" => __("Select if you want a number or an icon in the circle", 'bitpub')
		  ),
		  array(
			 "type" => "ewf-icon",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Select Icon", 'bitpub'),
			 "param_name" => "icon",
			 "value" => null,
			 "dependency" => array( "element" => "style","value" => array("icon")),
			 "description" => __("Select the icon you want to have on the left side of the section", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Number", 'bitpub'),
			 "param_name" => "number",
			 "value" => '1',
			 "dependency" => array( "element" => "style","value" => array("number")),
			 // "description" => __("The title of the progress bar", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", 'bitpub'),
			 "param_name" => "title",
			 "value" => __("Title", 'bitpub'),
			 "description" => __("The title of the progress bar", 'bitpub')
		  ),
		  // array(
			 // "type" => "textarea",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Details", 'bitpub'),
			 // "param_name" => "content",
			 // "value" => null,
			 // "description" => __("The content of progress", 'bitpub')
		  // ),
		)
	) );
	
	// vc_map( array(
		// "name" => __("Process Item V", 'bitpub'),
		// "base" => "ewf_processv_item",
		// "icon" => "icon-wpb-ewf-process-item",
		// "content_element" => true,
		// "as_child" => array('only' => 'ewf_processv_group'),
		// "show_settings_on_create" => true, 
		// "params" => array(
			
		  // array(
			 // "type" => "ewf-icon",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Select Icon", 'bitpub'),
			 // "param_name" => "icon",
			 // "value" => null,
			 // "description" => __("Select the icon you want to have on the left side of the section", 'bitpub')
		  // ),
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Title", 'bitpub'),
			 // "param_name" => "title",
			 // "value" => __("Title", 'bitpub'),
			 // "description" => __("The title of the progress bar", 'bitpub')
		  // ),
		  // array(
			 // "type" => "textarea_html",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Details", 'bitpub'),
			 // "param_name" => "content",
			 // "value" => null,
			 // "description" => __("The content of progress", 'bitpub')
		  // ),
		// )
	// ) );
	
	
	
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_ewf_processv_group extends WPBakeryShortCodesContainer {
		}
	}
	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCodeewf_processv_item extends WPBakeryShortCode {
		}
	}

?>