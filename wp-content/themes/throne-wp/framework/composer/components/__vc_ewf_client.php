<?php

	add_shortcode( 'ewf_client_item'	, 'ewf_vc_client_item' 		);
	add_shortcode( 'ewf_client_slider'	, 'ewf_vc_client_slider' 	);
	
	
	function ewf_vc_client_slider( $atts, $content ) {
		extract( shortcode_atts( array(
			'el_class' => null
		), $atts ) );
		
		return '<div class="client-logos-slider">'.do_shortcode($content).'</div><!-- end .client-logos-slider -->';
	}
	
	function ewf_vc_client_item( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'image_id' 		=> 0,
		  'image_url' 		=> '',
		  'link' 			=> '#',
		  'details' 		=> '',
		  'css' 			=> ''
	   ), $atts ) );
	   
	   $link = vc_build_link($link); 
	   $class_extra = ' '.$css;
	   
		if ($image_id){
			$image_url = wp_get_attachment_image_src($image_id, 'large'); 
			$image_url = $image_url[0]; 
		}
	   
		ob_start();
		
		// if ($link['url']){
			// echo '<a href="'.$link['url'].'" class="client-item'.$class_extra.'" title="'.$link['title'].'" ';
				// if (trim($link['target'])){
					// echo ' target="'.trim($link['target']).'"';
				// }
			// echo ' >';
		// }
		
		
		echo '<div class="client-item">';
		
			if ($image_id){
				echo '<img src="'.$image_url.'" alt="'.$image_id.'" />';
			}
			
			if ($details){
				echo '<div class="client-details">';
					echo '<p>'.$details.'</p>';
				echo '</div>';
			}
		
		echo '</div>';
		
		// if ($link['url']){
			// echo '</a>';
		// }
		
		return ob_get_clean();
	}

	
	vc_map( array(
		"name" => __("Client List", 'bitpub'),
		"base" => "ewf_client_slider",
		"as_parent" => array('only' => 'ewf_client_item'),
		"content_element" => true,
		"category" => EWF_SETUP_VC_GROUP,
		"icon" => "icon-wpb-ewf-client-slider",
		"description" => __("Creates a carousel of clients", 'bitpub'),  
		"show_settings_on_create" => true,
		"params" => array(
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'bitpub'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'bitpub')
			)
		),
		"js_view" => 'VcColumnView'
	) );
	
	
	vc_map( array(
	   "name" => __("Client", 'bitpub'),
	   "base" => "ewf_client_item",
	   "class" => "",
	   "category" => EWF_SETUP_VC_GROUP,
	   "icon" => "icon-wpb-ewf-client-item",
	   "as_child" => array('only' => 'ewf_client_slider'),
	   // "description" => __("Image and link", 'bitpub'),  
	   "params" => array(
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => __("Client logo", 'bitpub'),
				"param_name" => "image_id",
				"description" => __("Add the logo image", 'bitpub')
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Client details (optional)", 'bitpub'),
				"param_name" => "details",
				"value" => '',
				"description" => __("Specify some details about the client", 'bitpub')
			),
			// array(
				// "type" => "vc_link",
				// "holder" => "div",
				// "class" => "",
				// "heading" => __("Link", 'bitpub'),
				// "param_name" => "link",
				// "value" => '',
				// "description" => __("Specify an optional link pointing to an URL", 'bitpub') 
			// ),
			array( 
				"type" => "textfield", 
				"holder" => "div", 
				"class" => "", 
				"heading" => __("Extra CSS Class", 'bitpub'), 
				"param_name" => "css", 
				"value" => '', 
				"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)
	   )
	));

	
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_ewf_client_slider extends WPBakeryShortCodesContainer { }
	}
	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCodeewf_client_item extends WPBakeryShortCode { }
	}

?>