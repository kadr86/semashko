<?php

	add_shortcode( 'ewf_animation', 'ewf_vc_animation' );


	function ewf_vc_animation( $atts, $content ) {
		extract( shortcode_atts( array(
			'an_style' => null,
			'an_speed' => null,
			'an_delay' => null,
			'an_offset'=> null,
			'el_class' => null,
		), $atts ) );
		
		$attr_extra = null;
		$class_extra = null;
		
		if ($el_class){
			$class_extra = ' '.$el_class;
		}
		
		if ($an_style){
			$attr_extra .= ' data-animation="'.$an_style.'"';
		}
		
		if ($an_speed){
			$attr_extra .= ' data-animation-speed="'.$an_speed.'"';
		}
		
		if ($an_offset){
			$attr_extra .= ' data-animation-offset="'.$an_offset.'"';
		}
		
		if ($an_delay){
			$attr_extra .= ' data-animation-delay="'.$an_delay.'"';
		}
		
		return '<div class="animate fixed'.$class_extra.'"'.$attr_extra.'>'.do_shortcode($content).'</div>';
	}

	
	
	vc_map( array(
		"name" => __("Animation", 'bitpub'),
		"base" => "ewf_animation",
		"as_parent" => array('except' => 'ewf_timeline_item'),
		"content_element" => true,
		"icon" => "icon-wpb-ewf-animation",
		"description" => __("Add animations to elements", 'bitpub'),  
		"show_settings_on_create" => true,
		"category" => EWF_SETUP_VC_GROUP,
		"params" => array(
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				// "group" => __("Design options", 'bitpub'),
				"heading" => __("CSS Animation", 'bitpub'),
				"param_name" => "an_style",
				"value" => array(
				__('Bounce'					, 'bitpub') => 'bounce', 
				__('Flash'					, 'bitpub') => 'flash', 
				__('Pulse'					, 'bitpub') => 'pulse', 
				__('Rubber Band'			, 'bitpub') => 'rubberBand', 
				__('Snake'					, 'bitpub') => 'shake', 
				__('Swing'					, 'bitpub') => 'swing', 
				__('Tada'					, 'bitpub') => 'tada', 
				__('Wobble'					, 'bitpub') => 'wobble', 
				__('Bounce In'				, 'bitpub') => 'bounceIn', 
				__('Bounce In Down'			, 'bitpub') => 'bounceInDown', 
				__('Bounce In Left'			, 'bitpub') => 'bounceInLeft', 
				__('Bounce In Right'		, 'bitpub') => 'bounceInRight', 
				__('Bounce In Up'			, 'bitpub') => 'bounceInUp', 
				__('Bounce Out'				, 'bitpub') => 'bounceOut', 
				__('Bounce Out Down'		, 'bitpub') => 'bounceOutDown', 
				__('Bounce Out Left'		, 'bitpub') => 'bounceOutLeft', 
				__('Bounce Out Right'		, 'bitpub') => 'bounceOutRight', 
				__('Bounce Out Up'			, 'bitpub') => 'bounceOutUp', 
				__('Fade In'				, 'bitpub') => 'fadeIn', 
				__('Fade In Down'			, 'bitpub') => 'fadeInDown', 
				__('Fade In Down Big'		, 'bitpub') => 'fadeInDownBig', 
				__('Fade In Left'			, 'bitpub') => 'fadeInLeft', 
				__('Fade In Left Big'		, 'bitpub') => 'fadeInLeftBig', 
				__('Fade In Right'			, 'bitpub') => 'fadeInRight', 
				__('Fade In Right Big'		, 'bitpub') => 'fadeInRightBig', 
				__('Fade In Up'				, 'bitpub') => 'fadeInUp', 
				__('Fade In Up Big'			, 'bitpub') => 'fadeInUpBig', 
				__('Fade Out'				, 'bitpub') => 'fadeOut', 
				__('Fade Out Down'			, 'bitpub') => 'fadeOutDown', 
				__('Fade Out Down Big'		, 'bitpub') => 'fadeOutDownBig', 
				__('Fade Out Left'			, 'bitpub') => 'fadeOutLeft', 
				__('Fade Out Left Big'		, 'bitpub') => 'fadeOutLeftBig', 
				__('Fade Out Right'			, 'bitpub') => 'fadeOutRight', 
				__('Fade Out Right Big'		, 'bitpub') => 'fadeOutRightBig', 
				__('Fade Out Up'			, 'bitpub') => 'fadeOutUp', 
				__('Fade Out Up Big'		, 'bitpub') => 'fadeOutUpBig', 
				__('Flip InX'				, 'bitpub') => 'flipInX', 
				__('Flip InY'				, 'bitpub') => 'flipInY', 
				__('Flip OutX'				, 'bitpub') => 'flipOutX', 
				__('Flip OutY'				, 'bitpub') => 'flipOutY', 
				__('Light Speed In'			, 'bitpub') => 'lightSpeedIn', 
				__('Light Speed Out'		, 'bitpub') => 'lightSpeedOut', 
				__('Rotate In'				, 'bitpub') => 'rotateIn', 
				__('Rotate In Down Left'	, 'bitpub') => 'rotateInDownLeft', 
				__('Rotate In Down Right'	, 'bitpub') => 'rotateInDownRight', 
				__('Rotate In Up Left'		, 'bitpub') => 'rotateInUpLeft', 
				__('Rotate In Up Right'		, 'bitpub') => 'rotateInUpRight', 
				__('Rotate Out'				, 'bitpub') => 'rotateOut', 
				__('Rotate Out Down Left'	, 'bitpub') => 'rotateOutDownLeft', 
				__('Rotate Out Down Righ'	, 'bitpub') => 'rotateOutDownRight', 
				__('Rotate Out Up Left'		, 'bitpub') => 'rotateOutUpLeft', 
				__('Rotate Out Up Right'	, 'bitpub') => 'rotateOutUpRight', 
				__('Hinge'					, 'bitpub') => 'hinge', 
				__('Roll In'				, 'bitpub') => 'rollIn', 
				__('Roll Out'				, 'bitpub') => 'rollOut', 
				__('Zoom In'				, 'bitpub') => 'zoomIn', 
				__('Zoom In Down'			, 'bitpub') => 'zoomInDown', 
				__('Zoom In Left'			, 'bitpub') => 'zoomInLeft', 
				__('Zoom In Right'			, 'bitpub') => 'zoomInRight', 
				__('Zoom In Up'				, 'bitpub') => 'zoomInUp', 
				__('Zoom Out'				, 'bitpub') => 'zoomOut', 
				__('Zoom Out Down'			, 'bitpub') => 'zoomOutDown', 
				__('Zoom Out Left'			, 'bitpub') => 'zoomOutLeft', 
				__('Zoom Out Right'			, 'bitpub') => 'zoomOutRight', 
				__('Zoom Out Up'			, 'bitpub') => 'zoomOutUp', 
				__('Slide In Down'			, 'bitpub') => 'slideInDown', 
				__('Slide In Left'			, 'bitpub') => 'slideInLeft', 
				__('Slide In Right'			, 'bitpub') => 'slideInRight', 
				__('Slide In Up'			, 'bitpub') => 'slideInUp', 
				__('Slide Out Down'			, 'bitpub') => 'slideOutDown', 
				__('Slide Out Left'			, 'bitpub') => 'slideOutLeft', 
				__('Slide Out Right'		, 'bitpub') => 'slideOutRight', 
				__('Slide Out Up'			, 'bitpub') => 'slideOutUp',
			),
			"description" => __("Select the type of animation you want this element to be animated with when it enteres in the browsers viewport", 'bitpub')
			),
			array(
				"type" => "textfield",
				"heading" => __("Animation delay", 'bitpub'),
				"param_name" => "an_delay",
				"description" => __("Use this to delay the animation for a few (mili)seconds. Value in milliseconds, default 0", 'bitpub')
			),
			array(
				"type" => "textfield",
				"heading" => __("Animation Duration", 'bitpub'),
				"param_name" => "an_speed",
				"description" => __("Use this to specify the amount of time the animation should play. Value in milliseconds, default 1000", 'bitpub')
			),
			array(
				"type" => "textfield",
				"heading" => __("Offset", 'bitpub'),
				"param_name" => "an_offset",
				"description" => __("Add an animation offset. Value in percentages, default 90%", 'bitpub')
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'bitpub'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'bitpub')
				)
			),
		"js_view" => 'VcColumnView'
	) );
	
	
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_ewf_animation extends WPBakeryShortCodesContainer {
		}
	}
	


?>