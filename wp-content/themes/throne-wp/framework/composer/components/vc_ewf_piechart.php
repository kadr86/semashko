<?php

	add_shortcode( 'ewf-piechart', 'ewf_vc_piechart' );
	
	
	function ewf_vc_piechart( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'title' 			=> __('Sample progress bar', 'bitpub'),
		  'value' 			=> '90',
		  'width' 			=> '170',
		  'icon' 			=> null,
		  'linewidth' 		=> '5',
		  'color_bar' 		=> '#98D4EC',
		  'color_track' 	=> '#D7D7D7',
		  'css' 			=> null
	   ), $atts ));
	 
		$value = intval($value);
		$class_extra = ' '.$css;
		ob_start();

		echo '<div class="pie-chart'.$class_extra.'" data-percent="'.$value.'" data-barColor="'.$color_bar.'" data-trackColor="'.$color_track.'" data-lineWidth="'.$linewidth.'" data-barSize="'.$width.'">';
		
			echo '<div class="pie-chart-percent">';
				echo '<span></span>%';
			echo '</div><!-- end .pie-chart-percent -->';

		echo '</div><!-- end .pie-chart -->';
		
		if ($title || $content){
			echo '<div class="pie-chart-description">';
			
			if ($title){
				echo '<h4>' . $title .'</h4>';
			}
			
			if ($content){
				echo '<p>'.$content.'</p>';
			}
			
			echo '</div>';
		}
	   
		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Pie Chart", 'bitpub'),
	   "base" => "ewf-piechart",
	   "icon" => "icon-wpb-ewf-piechart",
	   "description" => null, 
	   "class" => "",
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Value", 'bitpub'),
			 "param_name" => "value",
			 "value" => 90,
			 "description" => __("Specify a value between 1 and 100, it represents the loaded percentage", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Pie chart title", 'bitpub'),
			 "param_name" => "title",
			 "value" => __("Title", 'bitpub'),
			 "description" => __("Describes what the value represents", 'bitpub')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Description", 'bitpub'),
			 "param_name" => "content",
			 "value" => null,
			 "description" => __("Extra details about the piechart", 'bitpub')
		  ),
		  // array(
			 // "type" => "ewf-icon",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Select Icon", 'bitpub'),
			 // "param_name" => "icon",
			 // "value" => null,
			 // "description" => __("Select the icon you want to have at the bottom of the section", 'bitpub')
		  // ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Width", 'bitpub'),
			 "param_name" => "width",
			 "value" => 170,
			 "description" => __("Represents the width of the piechart measured in pixels", 'bitpub')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Line width", 'bitpub'),
			 "param_name" => "linewidth",
			 "value" => array(5, 10, 15),
			 "description" => __("Represents the thickness of the piechart", 'bitpub')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Bar Color", 'bitpub'),
			 "param_name" => "color_bar",
			 "value" => '#98D4EC',
			 "description" => __("Select the color of the bar", 'bitpub')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Track Color", 'bitpub'),
			 "param_name" => "color_track",
			 "value" => '#D7D7D7',
			 "description" => __("Select the color of the track", 'bitpub')
		  ),	
		  array( 
				"type" => "textfield", 
				"holder" => "div", 
				"class" => "", 
				"heading" => __("Extra CSS Class", 'bitpub'), 
				"param_name" => "css", 
				"value" => '', 
				"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)		  
	   )
	));

?>