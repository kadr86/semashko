<?php

	add_shortcode( 'ewf_processh_group', 'ewf_vc_process_horizontal_group' );
	add_shortcode( 'ewf_processh_item', 'ewf_vc_process_horizontal_item' );


	// function ewf_vc_process_horizontal_group( $atts, $content ) {
		// extract( shortcode_atts( array(
			// 'el_class' => null
		// ), $atts ) );
		
		// $class_extra = ' '.$el_class;
		
		// return '<ul class="horizontal-process-builder four-items fixed'.$class_extra.'">'.do_shortcode($content).'</ul>';
	// }

	
	function ewf_vc_process_horizontal_group( $atts, $content ) {
		extract( shortcode_atts( array(
			'el_class' => null
		), $atts ) );
		
		$class_extra = ' '.$el_class;
		$class_asocc = array('3' => 'three-items', '4'=> 'four-items', '5'=> 'five-items');
		
		$process_items = do_shortcode($content);
		$items_number = count(explode('[ewf_processh_item', $content)) - 1;
		
		return '<ul class="horizontal-process-builder '.$class_asocc[$items_number].' fixed'.$class_extra.'">'.$process_items.'</ul>';
	}
	
	
	function ewf_vc_process_horizontal_item( $atts, $content ) {
		$shortcode_options = shortcode_atts( array(
			'title' => '',
			'style' => null,
			'image_id' => 0,
			'image_url' => '',
			'number' => null,
			'icon' => null,
		), $atts );
		
		extract($shortcode_options);
	 
		ob_start();
		
		if ($image_id){
			$image_url = wp_get_attachment_image_src($image_id, 'thumbnail'); 
			$image_url = $image_url[0]; 
		}
		
		echo '<li>';
			
			// if ($style == 'icon' && $icon){
				// echo '<i class="'.$icon.'"></i>';
			// }elseif($style == 'number' && $number){
				// echo '<h1>'.$number.'</h1>';
			// }elseif($style == 'image' && $image_id){
				// echo '<img src="'.$image_url.'" />';
			// }
			
			echo '<div class="process-description">';
				echo '<h6>'.$title.'</h6>';
				echo '<p>'.$content.'</p>';
			echo '</div>';
		echo '</li>';
		
		return ob_get_clean();
	}

	
	
	vc_map( array(
		"name" => __("Our Process", 'bitpub'),
		"base" => "ewf_processh_group",
		"as_parent" => array('only' => 'ewf_processh_item'),
		"content_element" => true,
		"category" => EWF_SETUP_VC_GROUP,
		"icon" => "icon-wpb-ewf-process-horizontal",
		"description" => __("Display a step by step horizontal process list", 'bitpub'),  
		"show_settings_on_create" => false,
		"params" => array(
			
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", 'bitpub'),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'bitpub')
			)
		),
		"js_view" => 'VcColumnView'
	) );
	
	vc_map( array(
		"name" => __("Process Step", 'bitpub'),
		"base" => "ewf_processh_item",
		"icon" => "icon-wpb-ewf-processh-item",
		"content_element" => true,
		"as_child" => array('only' => 'ewf_processh_group'),
		"show_settings_on_create" => true, 
		"params" => array(
		  // array(
			// "type" => "dropdown",
			// "holder" => "div",
			// "class" => "",
			// "heading" => __("Circle content", 'bitpub'),
			// "param_name" => "style",
			// "value" => array(
				// __('Icon', 'bitpub') => 'icon', 
				// __('Number', 'bitpub') => 'number',
				// __('Image', 'bitpub') => 'image'
			// ),
			// "description" => __("Select if you want a number or an icon in the circle", 'bitpub')
		  // ),
		  // array(
			 // "type" => "ewf-icon",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Select Icon", 'bitpub'),
			 // "param_name" => "icon",
			 // "value" => null,
			 // "dependency" => array( "element" => "style","value" => array("icon")),
			 // "description" => __("Select the icon you want to have on the left side of the section", 'bitpub')
		  // ),
		  // array(
			 // "type" => "textfield",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Number", 'bitpub'),
			 // "param_name" => "number",
			 // "value" => '1',
			 // "dependency" => array( "element" => "style","value" => array("number")),
			 // "description" => __("The title of the progress bar", 'bitpub')
		  // ),
		  // array(
			 // "type" => "attach_image",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Image", 'bitpub'),
			 // "param_name" => "image_id",
			 // "description" => __("Add the image", 'bitpub'),
			 // "dependency" => array( "element" => "style","value" => array("image")),
		  // ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", 'bitpub'),
			 "param_name" => "title",
			 "value" => __("Title", 'bitpub'),
			 "description" => __("The title of the progress bar", 'bitpub')
		  ),  
		  array(
			 "type" => "textarea",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Step details", 'bitpub'),
			 "param_name" => "content",
			 "value" => null,
			 "description" => __("The content of progress", 'bitpub')
		  ),
		)
	) );
	
	
	
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_ewf_processh_group extends WPBakeryShortCodesContainer {
		}
	}
	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCodeewf_processh_item extends WPBakeryShortCode {
		}
	}

?>