<?php
	
	global $multipage;

	#	Register shortcode for Visual Composer component
	#
	add_shortcode( 'ewf-blog-posts', 'ewf_vc_blog_posts' );
	add_action( 'init', 'ewf_register_vc_blog_posts');
	
	
	function ewf_vc_blog_posts( $atts, $content ) {
		global $post;
		
	   extract( shortcode_atts( array(
			"columns" 				=> 4,
			"items" 				=> 4,
			"order" 				=> "DESC",
			"categ" 				=> null,
			"title_more"			=> __('read more', 'bitpub'),
			"title_latest"			=> __('Read our latest posts', 'bitpub'),
			"url"					=> '#'
		), $atts));
		
		$query = array(
			'post_type' 			=> 'post'
			,'order'				=> $order 
			,'orderby' 				=> 'date'
			,'ignore_sticky_posts' 	=> 1
			,'post__not_in' 		=> get_option('sticky_posts')
			,'posts_per_page'		=> $columns
		); 
		
		if ($categ != null){
			$query['tax_query'] = array(
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => array( $categ )
				));
		}
		
		

		ob_start();
	
		echo '<div class="row">';
		
		
		$col_span = 12 / intval($columns);
		
		$wp_query_blog_posts = new WP_Query($query);
		while ($wp_query_blog_posts->have_posts()) : $wp_query_blog_posts->the_post();
			global $post;
			
				
				
			# 	Get post categories
			#
			$ewf_post_categories = null;
			$ewf_post_categories_first = null;
			
			
			
			foreach((get_the_category( $post->ID )) as $category) { 				
				if ($ewf_post_categories == null){
					$ewf_post_categories.= '<a href="'.get_category_link( $category->term_id ).'" >'.$category->cat_name.'</a>';
					$ewf_post_categories_first = '<a href="'.get_category_link( $category->term_id ).'" >'.$category->cat_name.'</a>';
				}else{
					$ewf_post_categories.= ', <a href="'.get_category_link( $category->term_id ).'" >'.$category->cat_name.'</a>';
				}
			}
				
				
			
	
			# Get post featured image
			#
			$ewf_image_id = get_post_thumbnail_id($post->ID);  
			
			$ewf_image_url = wp_get_attachment_image_src($ewf_image_id,'blog-featured-image'); 
			$ewf_image_url = $ewf_image_url[0];
			
			echo '<div class="span'.$col_span.'">';
				echo '<div class="blog-post ">';
				
					if ($ewf_image_id){
						echo  '<div class="blog-post-thumb">';
							if ($ewf_image_id){
								echo  '<img class="blog-post-thumb" src="'.$ewf_image_url.'" alt="" />';
							}
							
							// echo  '<div class="blog-post-info">'.get_the_time('d').'<small>'.get_the_time('M').'</small></div>';
						echo  '</div><!-- end .blog-post-thumb -->';
					}
				
					echo  '<div class="blog-post-title">';
						echo  '<h4><a href="' . get_permalink() . '">'.get_the_title($post->ID).'</a></h4>' ;
						
						echo '<p>';
							echo $ewf_post_categories_first;
							echo ' / '.get_the_time('d').'.'.get_the_time('m').'.'.get_the_time('Y');
							
							if (comments_open() || get_comments_number()){
								echo ' <i class="fa fa-comments"></i><a href="'.get_permalink().'#comments"><small>'.get_comments_number() . ' ' . __('comments', 'bitpub') . '</small></a>';
							}
						echo '</p>';
					echo  '</div><!-- end .blog-post-title -->';
					
					echo  '<p>'.get_the_excerpt().'</p>';
					// echo '<a class="btn alt" href="'.get_permalink().'">'.$title_more.' &gt;&gt;</i></a>';
			

				echo '</div> <!-- .blog-post -->'; 
			echo '</div>';
			
 
		endwhile;

		wp_reset_postdata();
		
		echo '</div>';
		
		// echo'
		// <div class="row">
			// <div class="span12 text-center">

				// <br><br>
				// <a class="btn btn-white btn-large text-uppercase" href="'.$url.'"><strong>'.$title_latest.'</strong></a>
				// <br>
				
			// </div><!-- end .span12 -->
		// </div><!-- end .row -->';
		
		
		return ob_get_clean();
	 
	}
		
	
	function ewf_vc_blog_posts_categories(){
		global $ewf_portfolioStrip_services;
		
		$categs = array();
		$terms = get_terms ('category'); 
	
		if (is_array($terms)){
			foreach($terms as $key => $categ_item){

				$categs[] = $categ_item->slug;
			}
		}
  		
		return $categs;
	}
		
	
	
	function ewf_register_vc_blog_posts(){
	
		vc_map( array(
		   "name" => __("Blog Posts", 'bitpub'),
		   "base" => "ewf-blog-posts",
		   "class" => "",
		   "icon" => "icon-wpb-ewf-blog-posts",
		   "description" => __("Show latest blog posts", 'bitpub'), 
		   "category" => EWF_SETUP_VC_GROUP,
		   "params" => array(
			  // array(
				 // "type" => "textfield",
				 // "holder" => "div",
				 // "class" => "",
				 // "heading" => __("Number of posts", 'bitpub'),
				 // "param_name" => "items",
				 // "value" => 2,
				 // "description" => __("Specify the number of posts to load", 'bitpub')
			  // ),
			  // array(
				 // "type" => "dropdown",
				 // "holder" => "div",
				 // "class" => "",
				 // "heading" => __("Category", 'bitpub'),
				 // "param_name" => "categ",
				 // "value" => ewf_vc_blog_posts_categories(),
				 // "description" => __("Load posts from a defined category", 'bitpub'),
			  // ),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("How many recent blog posts to show", 'bitpub'),
					"param_name" => "columns",
					"value" => array(
					__('4 Columns', 'bitpub') => 4, 
					__('3 Columns', 'bitpub') => 3, 
					),
				),
			  // array(
				 // "type" => "textfield",
				 // "holder" => "div",
				 // "class" => "",
				 // "heading" => __("Read more title", 'bitpub'),
				 // "param_name" => "title_more",
				 // "value" => __('read more', 'bitpub'),
				 // "description" => __("Read more title from blog article", 'bitpub')
			  // ),
			  // array(
				 // "type" => "textfield",
				 // "holder" => "div",
				 // "class" => "",
				 // "heading" => __("Button title", 'bitpub'),
				 // "param_name" => "title_latest",
				 // "value" => __('read our latest posts', 'bitpub'),
				 // "description" => __("Read more title from the button", 'bitpub')
			  // ),
			  // array(
				 // "type" => "textfield",
				 // "holder" => "div",
				 // "class" => "",
				 // "heading" => __("Button link", 'bitpub'),
				 // "param_name" => "url",
				 // "value" => "#",
				 // "description" => __("Read more title from the button", 'bitpub')
			  // )
		   )
		));
	
	}


?>