<?php

	add_shortcode( 'ewf-testimonial', 'ewf_vc_testimonial' );
	
	
	function ewf_vc_testimonial( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'image_id' 	=> 0,
		  'image_url' 	=> null,
		  'name' 		=> __("Sample Name", 'bitpub'),
		  'description' => __("Client", 'bitpub'),
		  'style' 		=> 'ewf-testimonial-style-1',
		  'icon' 		=> null,
		  'css' 		=> ''
		  
	   ), $atts ) );
	   
	   $class_extra = null;
	   
		if ($image_id){
			$image_url = wp_get_attachment_image_src($image_id, 'thumbnail'); 
			$image_url = $image_url[0]; 
		}else{
			$class_extra = 'class="no-image"';
		}
	   
	   
		ob_start();
		

		
		switch($style) {
			
			case "ewf-testimonial-style-1":
				echo '<div class="testimonial fixed '.$css.'">';
					
					// if ($icon){ echo '<i class="'.$icon.'"></i>'; }
					echo '<i class="ifc-quote"></i>';

					echo '<blockquote '.$class_extra.'>'; 		
						echo '<p>' . $content . '</p>';
						
						echo '<p>'.$name;
							if ($description){
								echo '<span>'.$description.'</span>';
							}
						echo '</p>';
						
					echo '</blockquote>';				
					
					
				echo '</div><!-- end .testimonial -->';
				break;
			
			
			case "ewf-testimonial-style-2":
				echo '<div class="testimonial alt fixed '.$css.'">';
				
					echo '<blockquote '.$class_extra.'>'; 		
						echo '<p>' . $content . '</p>';
					echo '</blockquote>';				
				
					if ($image_id){
						echo '<img src="'.$image_url.'" height="80" width="80" />';
						}
				
					echo '<h4>'.$name;
						if ($description){
							echo '<small>'.$description.'</small>';
						}
					echo '</h4>';
						  
				echo '</div><!-- end .testimonial -->';
				break;
				
			
			case "ewf-testimonial-style-3":
				echo '<div class="testimonial-2 '.$css.'">';
				
					echo '<blockquote '.$class_extra.'>'; 		
						echo '<p>' . $content . '</p>';
					echo '</blockquote>';	
					
					if ($image_id){
						echo '<span>';
							echo '<img width="70" src="'.$image_url.'" alt="'.$image_id.'" />';
						echo '</span>';
					}
				
					echo '<h5>'.$name.'</h5>';
					if ($description){
						echo '<h6>'.$description.'</h6>';
					}
						  
			
				echo '</div><!-- end .testimonial -->';
				break;
				
				
			case "ewf-testimonial-style-4":
				echo '<div class="testimonial-3 '.$css.'">';
					
					if ($image_id){
						echo '<span>';
							echo '<img width="70" src="'.$image_url.'" alt="'.$image_id.'" />';
						echo '</span>';
					}
					
					echo '<h4>'.$name.'</h4>';
					if ($description){
						echo '<h6>'.$description.'</h6>';
					}
					
					echo '<blockquote '.$class_extra.'>'; 		
						echo '<p>' . $content . '</p>';
					echo '</blockquote>';	
							
				echo '</div><!-- end .testimonial -->';
				break;
			
		}
		
		
		
		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Testimonial", 'bitpub'),
	   "base" => "ewf-testimonial",
	   "class" => "",
	   "icon" => "icon-wpb-ewf-testimonial",
	   "description" => __("Add a client testimonial", 'bitpub'),  
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
			// array(
				// "type" => "ewf-image-select",
				// "holder" => "div",
				// "class" => "",
				// "heading" => __("Testimonial Style", 'bitpub'),
				// "param_name" => "style",
				// "options" => array(
					// 'Style 1' => 'ewf-testimonial-style-1', 
					// 'Style 2' => 'ewf-testimonial-style-2', 
					// 'Style 3' => 'ewf-testimonial-style-3', 
					// 'Style 4' => 'ewf-testimonial-style-4', 
				// ),
				// "value" => 'ewf-testimonial-style-1',
			// ),
			// array(
				// "type" => "attach_image",
				// "holder" => "div",
				// "class" => "",
				// "heading" => __("Image", 'bitpub'),
				// "param_name" => "image_id",
				// "description" => __("Add an image to the right side of the testimonial", 'bitpub')
			// ),
		  // array(
			 // "type" => "ewf-icon",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Select Icon", 'bitpub'),
			 // "param_name" => "icon",
			 // "value" => null,
			 // "description" => __("Select the icon you want to have at the top of the section", 'bitpub')
		  // ),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Company name / client name", 'bitpub'),
				"param_name" => "name",
				"value" => __("Sample Name", 'bitpub'),
				// "description" => __("Specify the the name of the testimonial author", 'bitpub')
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Company description / Client description", 'bitpub'),
				"param_name" => "description",
				"value" => __("Description", 'bitpub'),
				"description" => __("SAdd a short description like e.g the companys field or the clients position in the company", 'bitpub')
			),
			array(
				"type" => "textarea",
				"holder" => "div",
				"class" => "",
				"heading" => __("Testimonial", 'bitpub'),
				"param_name" => "content",
				"value" => null,
				"description" => __("Specify the text of the testimonial", 'bitpub')
			),
			array( 
				"type" => "textfield", 
				"holder" => "div", 
				"class" => "", 
				"heading" => __("Extra CSS Class", 'bitpub'), 
				"param_name" => "css", 
				"value" => '', 
				"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)
	   )
	));


?>