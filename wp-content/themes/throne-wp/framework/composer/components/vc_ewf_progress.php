<?php

	add_shortcode( 'ewf-progress', 'ewf_vc_progress' );
	
	
	function ewf_vc_progress( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'title' 				=> __('Sample progress bar', 'bitpub'),
		  'value' 				=> '90',
		  'direction' 			=> 'right',
		  'tracker_color' 		=> '#98D4EC',
		  'background_color'	=> '#D7D7D7',
		  'css' 				=> null
	   ), $atts ) );
	 
		$class_extra = ' '.$css;
		$class_direction = null; 
		$value = intval($value);
	 
		// if ($direction == 'left'){
			// $class_direction = ' alt';
		// }
	 
		ob_start();
		
		echo '<div class="fixed'.$class_extra.'" data-direction="'.$direction.'">';
			echo '<div class="progress-bar-description'.$class_direction.'">' . $title . '<span style="left:' . $value . '%">' . $value . '%</span>' . '</div>';

			echo '<div class="progress-bar'.$class_direction.'" style="background-color:'.$background_color.'">'; 
				echo '<span class="progress-bar-outer" data-width="'.$value.'" style="background-color:'.$tracker_color.'">';
					echo '<span class="progress-bar-inner"></span>';
				echo '</span>';
			echo '</div><!-- end .progress-bar -->';
		echo '</div>';
		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Progress Bar", 'bitpub'),
	   "base" => "ewf-progress",
	   "icon" => "icon-wpb-ewf-progress",
	   // "description" => __("Add atitle and a percentage loaded", 'bitpub'), 
	   "class" => "",
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Value", 'bitpub'),
			 "param_name" => "value",
			 "value" => 90,
			 "description" => __("Specify a value between 1 and 100, it represents the loaded percentage", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Progress bar title", 'bitpub'),
			 "param_name" => "title",
			 "value" => __("Title", 'bitpub'),
			 "description" => __("Describes what the value represents", 'bitpub')
		  ),
		  // array(
			 // "type" => "dropdown",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Loading direction", 'bitpub'),
			 // "param_name" => "direction",
			 // "value" => array( 
				// __('Left to right', 'bitpub') => 'left',
				// __('Right to left', 'bitpub') => 'right'
			// ),
			 // "description" => __("Specify the direction of the progress", 'bitpub')
		  // ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Tracker Color", 'bitpub'),
			 "param_name" => "tracker_color",
			 "value" => '#98D4EC',
			 "description" => __("Select the color of the tracker", 'bitpub')
		  ),
		  array(
			 "type" => "colorpicker",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Bar Color", 'bitpub'),
			 "param_name" => "background_color",
			 "value" => '#D7D7D7',
			 "description" => __("Select the color of the bar", 'bitpub')
		  ),
		  array( 
				"type" => "textfield", 
				"holder" => "div", 
				"class" => "", 
				"heading" => __("Extra CSS Class", 'bitpub'), 
				"param_name" => "css", 
				"value" => '', 
				"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)
	   )
	));


?>