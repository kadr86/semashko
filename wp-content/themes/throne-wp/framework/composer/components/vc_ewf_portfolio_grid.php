<?php
	
	
	#	Register image size for project
	#
	add_image_size( 'ewf-portfolio-grid', 720, 720, true);
	
	
	#	Register shortcode for Visual Composer component
	#
	add_shortcode( 'ewf-portfolio-grid', 'ewf_vc_portfolio_grid' );
	
	add_action('init', 'ewf_register_vc_portfolio_grid');
	

	
	function ewf_vc_portfolio_grid( $atts, $content ) {
		global $post;
		
		$options = shortcode_atts( array(
			"items" 				=> 8,
			"id" 					=> null,
			"exclude" 				=> null,
			"order" 				=> "DESC",
			"list" 					=> null,
			"service" 				=> null,
			"columns"				=> 3,
			"css"					=> null,
			"nav"					=> 1
		), $atts);
		extract($options);
				
		$class_extra = ' '.$css;
		$_ewf_rowItems = 0;
		$_ewf_span = 3;
		$_ewf_items = 0;


		switch($columns){
			case '3': $_ewf_span = '4';break;
			case '4': $_ewf_span = '3';break;
		}
		
		
		if (get_option(EWF_SETUP_THNAME."_debug_mode", 'false') == 'true'){
			echo '<pre><strong>Portfolio Grid</strong><br/>';
				// print_r($atts);
				print_r($options);
			echo '</pre>';
		}
		
		
		ob_start();
		
		$wp_portfolio_query = ewf_helper_query_builder($options);
		$wp_portfolio = new WP_Query($wp_portfolio_query);
		
		while ($wp_portfolio->have_posts()) : $wp_portfolio->the_post();
			global $post;
			
			
			#	Get post extra info
			$post_extra = ewf_helper_get_post_extra($post, 'ewf-portfolio-grid', EWF_PROJECTS_TAX_SERVICES);

			
			#	Split in columns
			$_ewf_items++;
			if ($_ewf_rowItems == 0){	echo '<div class="row">';	}
			$_ewf_rowItems++;
			
		
			  echo '<div class="span'.$_ewf_span.'">
						<div class="portfolio-item">
							<div class="portfolio-item-preview">
							
								<img src="'.$post_extra['image-thumb'].'" alt="" >
								
								<div class="portfolio-item-overlay">
									
									<div class="portfolio-item-description">
										<h5>'.get_the_title().'</h5>
										<p>'.get_the_excerpt().'</p>
										<a class="btn" href="' . get_permalink() . '">' . __("View", 'bitpub') . '</a>
									</div><!-- end .portfolio-item-description -->
									
								</div><!-- end .portfolio-item-overlay -->
								
							</div><!-- end .portfolio-item-preview -->   

						</div><!-- end .portfolio-item -->
					</div>';
			

			
			#	Split in columns
			if ($_ewf_rowItems == $columns || $wp_portfolio->post_count == $_ewf_items){
				$_ewf_rowItems = 0;
				echo '</div>';
			}
			
		endwhile;
		
		wp_reset_postdata();
		
		#	Navigation
		#
		if ($nav){
			echo ewf_sc_grid_navigation_pages(5, $wp_portfolio);
		}
	
		
		return '<div class="portfolio-columns gutter ' . $class_extra . '">' . ob_get_clean() . '</div>';
	 
	}
		
	
	function ewf_vc_portfolio_grid_services(){
		global $ewf_portfolioStrip_services;
		
		$services = array();	// array('All Services');
		$terms = get_terms (EWF_PROJECTS_TAX_SERVICES); 
	
		if (is_array($terms)){
			foreach($terms as $key => $service){
				$services[] = $service->slug;
			}
		}
  		
		return $services;
	}
		
		
	function ewf_sc_grid_navigation_pages( $range = 5, $query){
		$src_nav = null;
		$max_page = 0;
		
		$data_pages = array();
		
		$class_current = 'current';
		$current_page = $query->query_vars['paged'];
		
		
		
		if ($current_page == 0) { 
			$current_page = 1; 
			}
	
	
	# 	How much pages do we have?
	
		if ( !$max_page ) {
			$max_page = $query->max_num_pages;
			}
			
			
			

	  // We need the pagination only if there are more than 1 page
	  if($max_page > 1){
	  
		if ( !$current_page ) 		{ $current_page = 1; }
		
		if($max_page > $range){
		  // When closer to the beginning
		  if($current_page < $range){
			for($i = 1; $i <= ($range + 1); $i++){		  
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);
			}
		  } 
		  // When closer to the end
		  elseif($current_page >= ($max_page - ceil(($range/2)))){
			$extra = 0;
		  
			if (($max_page - ($max_page - $range)) < 2 ) $extra = 1;
		  
			for($i = $max_page - $range - $extra; $i <= $max_page; $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i); 
			}
		  }
		  // Somewhere in the middle
		  elseif($current_page >= $range && $current_page < ($max_page - ceil(($range/2)))){
			$extra = 0;
			if ($current_page - ceil($range/2) == 0 ) $extra = 1;
			
			for($i = ( $current_page - ceil($range/2) + $extra); $i <= ($current_page + ceil(($range/2))+$extra); $i++){
			  $data_pages['curent'] = $current_page;
			  $data_pages['pages'][$i] =  get_pagenum_link($i);  
			}
		  }
		} 
		// Less pages than the range, no sliding effect needed
		else{
		  for($i = 1; $i <= $max_page; $i++){
			$data_pages['curent'] = $current_page;
			$data_pages['pages'][$i] =  get_pagenum_link($i);
		  }
		}

		if($current_page != $max_page){}
	  }
	   
		$src_nav = null;
		
		if (array_key_exists('pages', $data_pages)){
			$pos_curent = $data_pages['curent'];
			
			$src_nav.= '<br><br><ul class="pagination fixed">';
				$count = 0;
				
				foreach($data_pages['pages'] as $key => $url){
					$count++;

					if($pos_curent == $key){
						$src_nav.= '<li class="current"><a href="#">'.$key.'</a></li>';
					}else{
						$src_nav.= '<li><a href="'.$url.'">'.$key.'</a></li>';
					}
				}
				
			$src_nav.= '</ul>';
		}
	  
		return $src_nav;
	
	}
	
	
	function ewf_register_vc_portfolio_grid(){

		vc_map( array(
		   "name" => __("Portfolio Grid", 'bitpub'),
		   "base" => "ewf-portfolio-grid",
		   "class" => "",
		   "icon" => "icon-wpb-ewf-portfolio-grid",
		   "description" => __("Show portfolio items, pagination optional", 'bitpub'), 
		   "category" => EWF_SETUP_VC_GROUP,
		   "params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __("Number of portfolio items", 'bitpub'),
					"param_name" => "items",
					"value" => 8,
					"description" => __("Select the number of items the grid should display. If Pagination is enabled this is the number of portfolio items per page. if pagination is disabled this is the total number of items", 'bitpub')
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Prortfolio grid layout", 'bitpub'),
					"param_name" => "columns",
					"value" => array(
					__('3 Columns', 'bitpub') => 3, 
					// __('4 Columns', 'bitpub') => 4, 
					// __('1 Column', 'bitpub')
					),
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Source of the portfolio items from the grid", 'bitpub'),
					"param_name" => "list",
					"value" => array( __('Random', 'bitpub') => 'random', __('From a service', 'bitpub')=>'service', __('Latest projects','bitpub')=>'latest'),
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Service", 'bitpub'),
					"param_name" => "service",
					"value" => ewf_vc_portfolio_grid_services(),
					"description" => __("Specify projects from a defined category", 'bitpub'),
					"dependency" => Array("element" => "list","value" => array("service"))
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Projects order", 'bitpub'),
					"param_name" => "order",
					"value" => array('DESC', 'ASC'),
					"dependency" => Array("element" => "list","value" => array("service")),
					"description" => __("Load projects in a Ascendent(1,2,3) or Descendent (3,2,1) order", 'bitpub')
				),
				array(
					"type" => "dropdown",
					"holder" => "div",
					"class" => "",
					"heading" => __("Enable pagination?", 'bitpub'),
					"param_name" => "nav",
					"value" => array(
					__('Yes', 'bitpub') => 1, 
					__('No', 'bitpub') => 0, 
					),
				),
				array( 
					"type" => "textfield", 
					"holder" => "div", 
					"class" => "", 
					"heading" => __("Extra CSS Class", 'bitpub'), 
					"param_name" => "css", 
					"value" => '', 
					"description" => __("Add and extra CSS class to the component", 'bitpub') 
				)
		   )
		));

	}


?>