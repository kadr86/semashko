<?php

	add_shortcode( 'ewf-odometer', 'ewf_vc_odometer' );
	
	
	function ewf_vc_odometer( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'number' 		=> 1,
		  'description' => null,
		  'css' 		=> null
	   ), $atts ));
	 
		$number = intval($number);
		$class_extra = ' '.$css;
	 
		ob_start();
		
		echo '<div class="ewf-odometer'.$class_extra.'">';
			echo '<div class="odometer" data-value="'.$number.'"></div>';
			echo '<div class="odometer-description">'.$description.'</div>';
		echo '</div>';
		
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Odometer", 'bitpub'),
	   "base" => "ewf-odometer",
	   "class" => "",
	   "icon" => "icon-wpb-ewf-odometer",
	   "description" => __("Animated count from 0 to specified number", 'bitpub'),  
	   "category" => EWF_SETUP_VC_GROUP,
	   "params" => array(

		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Number", 'bitpub'),
			 "param_name" => "number",
			 "value" => __("Number", 'bitpub'),
			 "description" => __("The odometer number", 'bitpub')
		  ),
		  array(
			 "type" => "textarea",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Details", 'bitpub'),
			 "param_name" => "description",
			 "value" => null,
		  ),
			array( 
				"type" => "textfield", 
				"holder" => "div", 
				"class" => "", 
				"heading" => __("Extra CSS Class", 'bitpub'), 
				"param_name" => "css", 
				"value" => '', 
				"description" => __("Add and extra CSS class to the component", 'bitpub') 
			)
	   )
	));


?>