<?php

	add_shortcode( 'ewf-iconbox', 'ewf_vc_iconbox' );
	
	
	function ewf_vc_iconbox( $atts, $content ) {
		extract( shortcode_atts( array(
			'title' 		=> __('Title', 'bitpub'),
			'subtitle' 		=> null,
			'style' 		=> 'ewf-iconbox-style-1',
			'type' 			=> 'icon',
			'link_style' 	=> 'button', 
			'link' 			=> null, 
			'align' 		=> 'center', 
			'icon' 			=> null,
			'number' 		=> 0,
			'image_id'      => 0,
			'image_url'     => '',
			'css' 			=> null 
		), $atts ) );
	
			
		ob_start();
		

		$link = vc_build_link($link); 
		$link_src = null;
		$link_title = '#';
		
		
		// echo '<pre>';
			// print_r($link);
		// echo '</pre>';
		

		
		$class_extra = ' '.$css;
	
		if ($link['title'] != null){
			$class_link = null;	
			
			if ($style == 'ewf-iconbox-style-1' || $style == 'ewf-iconbox-style-2' || $style == 'ewf-iconbox-style-3' || $style == 'ewf-iconbox-style-4' ){
				// $class_link .= 'class="btn btn-white alt" ';
				$link['title'] .= ' &gt;&gt;';
			}else{
				$class_link .= null;
			}
			
			if ($style == 'ewf-iconbox-style-5'){
				$class_link .= 'class="btn alt" ';
				$link['title'] .= ' &gt;&gt;';
				// $link['title'] = $link['title'].' <i class="ifc-right"></i>';
			}
			
			if ($link['target'] != null){
				$class_link .= 'target="_black" ';		
			}
			
			// $link_src .= '<p class="last text-'.$align.'"><a '.$class_link.'href="'.$link['url'].'">'.$link['title'].'</a></p>';
			$link_src .= '<p class="last text-'.$align.'"><a '.$class_link.'href="'.$link['url'].'">'.$link['title'].'</a></p>';
			$link_title = $link['url'];
		}
	
	
		switch($type){
			case 'icon':
				$icon = '<i class="'.$icon.'"></i>';
				break;
			
			case 'image':
				if ($image_id){
					$image_url = wp_get_attachment_image_src($image_id, 'full'); 
					$image_url = $image_url[0]; 
				}
				$icon = '<img src="'.$image_url.'" alt="">';
				break;
			
			case 'number':
				$icon = $number;
				break;
		}
	
	
		switch($style){
		
			case 'ewf-iconbox-style-1':
				echo '
				<div class="icon-box-1'.$class_extra.'">
					'.$icon.'
					<div class="icon-box-content">	
						<p>'.$content.'</p>
						'.$link_src.'
					</div><!-- end .icon-box-content -->
				</div><!-- end .icon-box-1 -->';
				break;
				
		
			case 'ewf-iconbox-style-2':
				echo '
				<div class="icon-box-2'.$class_extra.'">
					'.$icon.'

					<div class="icon-box-content">
						<h5>
							<a href="'.$link_title.'">'.$title.'</a>
							<small>'.$subtitle.'</small>
						</h5>
						
						<p>'.$content.'</p>
						'.$link_src.'
					</div><!-- end .icon-box-content -->
				</div><!-- end .icon-box-2 -->';
				break;
				
		
			case 'ewf-iconbox-style-3':
				echo '
				<div class="icon-box-3'.$class_extra.'">
					<h4>'.$icon.'</h4>
					<h3><a href="'.$link_title.'">'.$title.'</a></h3>
					<br class="clear">
					<p>'.$content.'</p>
					'.$link_src.'
				</div><!-- end .icon-box-3 -->';
				break;
			
			
			case 'ewf-iconbox-style-4':
				echo '
				<div class="icon-box-4'.$class_extra.'">
					'.$icon.'
					
					<div class="icon-box-content">
						<h4><a href="'.$link_title.'">'.$title.'</a></h4>
						<p>'.$content.'</p>
						'.$link_src.'
					</div><!-- end .icon-box-content -->
				</div><!-- end .icon-box-4 -->';
				break;
				
				
			case 'ewf-iconbox-style-5':
				echo '
				<div class="icon-box-5'.$class_extra.'">
					'.$icon.'
					
					<div class="icon-box-content">
						<h4><a href="'.$link_title.'">'.$title.'</a></h4>
						
						<p>'.$content.'</p>
						'.$link_src.'
					</div>
				</div><!-- end .icon-box-5 -->';
				break;
					
					
			case 'ewf-iconbox-style-6':
				echo '
				<div class="icon-box-6'.$class_extra.'">
					'.$icon.'
					
					<div class="icon-box-content">
						<h3><strong><a href="'.$link_title.'">'.$title.'</a></strong></h3>
					
						'.$content.'
						'.$link_src.'
					</div>
				</div><!-- end .icon-box-6 -->';
				break;
					
					
			case 'ewf-iconbox-style-7':
				echo '
				<div class="icon-box-7'.$class_extra.'">
					'.$icon.'
					
					<h3 class="text-uppercase"><a href="'.$link_title.'">'.$title.'</a></h3>
				
					<br class="clear">
				
					'.$content.'
					'.$link_src.'
				</div><!-- end .icon-box-7 -->';
				break;
			
			default:
			break;

		}
			
			
		return ob_get_clean();
	}

	
	vc_map( array(
	   "name" => __("Icon Box", 'bitpub'),
	   "base" => "ewf-iconbox",
	   "class" => "",
	   "category" => EWF_SETUP_VC_GROUP,
	   "icon" => "icon-wpb-ewf-iconbox",
	   "description" => __("Add an icon box, with icon and description", 'bitpub' ),  
	   "params" => array(
			array(
				"type" => "ewf-image-select",
				"holder" => "div",
				"class" => "",
				"heading" => __("Iconbox Style", 'bitpub'),
				"param_name" => "style",
				"options" => array(
					'Style 1' => 'ewf-iconbox-style-1',
					'Style 2' => 'ewf-iconbox-style-2',
					'Style 3' => 'ewf-iconbox-style-3',
					'Style 4' => 'ewf-iconbox-style-4',
					'Style 5' => 'ewf-iconbox-style-5',
				),
				"value" => 'ewf-iconbox-style-1',
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Title", 'bitpub'),
				"param_name" => "title",
				"value" => __("Title", 'bitpub')
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __("Subtitle", 'bitpub'),
				"param_name" => "subtitle",
				"value" => __("Subtitle", 'bitpub'),
				"description" => __("Only used by icon style 2", 'bitpub') 
			), 		
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Iconbox should display a number, an image or an icon", 'bitpub'),
				"param_name" => "type",
				"value" => array(
					__('Icon', 'bitpub') => 'icon', 
					__('Number', 'bitpub') => 'number',
					__('Image', 'bitpub') => 'image'
				),
				"description" => __("Style 1, 2 and 5 have been designed to use an icon, style 3 number, style 4 images however you can use each style you want.", 'bitpub')
			),
			array(
			"type" => "ewf-icon",
			"holder" => "div",
			"class" => "",
			"heading" => __("Select Icon", 'bitpub'),
			"param_name" => "icon",
			"value" => null,
			"dependency" => array( "element" => "type","value" => array("icon")),
			"description" => __("Select the icon", 'bitpub')
			),
			array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Number", 'bitpub'),
			"param_name" => "number",
			"value" => '1',
			"dependency" => array( "element" => "type","value" => array("number")),
			"description" => __("Add the number", 'bitpub')
			),
			array(
			"type" => "attach_image",
			"holder" => "div",
			"class" => "",
			"heading" => __("Image", 'bitpub'),
			"param_name" => "image_id",
			"description" => __("Add the image", 'bitpub'),
			"dependency" => array( "element" => "type","value" => array("image")),
			),
			array(
			"type" => "textarea_html",
			"holder" => "div",
			"class" => "",
			"heading" => __("Content", 'bitpub'),
			"param_name" => "content",
			"value" => __("I am test text block. Click edit button to change this text.", 'bitpub'),
			"description" => __("Add description text for the service", 'bitpub') 
			),
			array(
			"type" => "vc_link",
			"holder" => "div",
			"class" => "",
			"heading" => __("Link (Optional)", 'bitpub'),
			"param_name" => "link",
			"value" => '#',
			"description" => __("Specify an optional link to another page", 'bitpub') 
			),
			array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Link alignment", 'bitpub'),
			"param_name" => "align",
			"value" => array(__('Default', 'bitpub') => 'default', __('Left', 'bitpub') => 'left', __('Center', 'bitpub') => 'center', __('Right', 'bitpub') => 'right'),
			"description" => __("How the read more link should be aligned inside of iconbox", 'bitpub'),
			)
		  
		  ))
		);

		
		
		
	// add_shortcode( 'ewf_iconbox_slider', 'ewf_vc_iconbox_slider' );

	
	// function ewf_vc_iconbox_slider( $atts, $content ) {
		// extract( shortcode_atts( array(), $atts ) ); 
		
		// return '<div class="services-slider"><div class="slides">'.do_shortcode($content).'</div></div>';
	// }
	
	
	// vc_map( array(
		// "name" => __("Iconbox Slider", 'bitpub'),
		// "base" => "ewf_iconbox_slider",
		// "as_parent" => array('only' => 'ewf-iconbox'),
		// "content_element" => true,
		// "icon" => "icon-wpb-ewf-iconbox-slider",
		// "description" => __("Create a slider with iconbox components", 'bitpub'),  
		// "show_settings_on_create" => false,
		// "params" => array(
			
			// array(
				// "type" => "textfield",
				// "heading" => __("Extra class name", 'bitpub'),
				// "param_name" => "el_class",
				// "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'bitpub')
			// ),
		// ),
		// "js_view" => 'VcColumnView'
	// ) );
	
	
	
	// if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		// class WPBakeryShortCode_ewf_iconbox_slider extends WPBakeryShortCodesContainer {
		// }
	// }

?>