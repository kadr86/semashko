<?php

	add_shortcode( 'ewf-process-item', 'ewf_vc_process_item' );
	
	function ewf_vc_process_item( $atts, $content ) {
	   extract( shortcode_atts( array(
		  'title' => __('Sample title', 'bitpub'),
		  'number' => '1', 
		  'position' => __('Number first', 'bitpub')
	   ), $atts ) );
	 
		$src = null;
		
		if ($position == 'Number first'){
			$src .= '<div class="process alt fixed">';
				$src .= '<h1>'.$number.'</h1>';
				$src .= '<h5><span>'.$title.'</span></h5>';
				$src .= '<div class="services-description">'.$content.'</div>';
			$src .= '</div><!-- end .process -->';
		}elseif($position == 'Title first'){
			$src .= '<div class="process fixed">';
				$src .= '<h5><span>'.$title.'</span></h5>';
				$src .= '<h1>'.$number.'</h1>';
				$src .= '<div class="services-description">'.$content.'</div>';
			$src .= '</div><!-- end .process -->';
		}
		
		return $src;
	}
		

	vc_map( array(
	   "name" => __("Single Process", 'bitpub'),
	   "base" => "ewf-process-item",
	   "class" => "",
	   "icon" => "icon-wpb-ewf-process",
	   "description" => __("Add a process title with number and details", 'bitpub'),  
	   "category" => __('Typography', 'bitpub'),
	   "params" => array(
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Title", 'bitpub'),
			 "param_name" => "title",
			 "value" => __("Process title", 'bitpub'),
			 "description" => __("Specify the title that will fit in the hexagon.", 'bitpub')
		  ),
		  array(
			 "type" => "textfield",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Number", 'bitpub'),
			 "param_name" => "number",
			 "value" => 1,
			 "description" => __("Specify the number positioned in front or after the hexagon.", 'bitpub')
		  ),
		  array(
			 "type" => "dropdown",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Position", 'bitpub'),
			 "param_name" => "position",
			 "value" => array(__('Number first', 'bitpub'), __('Title first', 'bitpub')),
			 "description" => __("Specify the arrangement of number and hexagon", 'bitpub')
		  ),
		  array(
			 "type" => "textarea_html",
			 "holder" => "div",
			 "class" => "",
			 "heading" => __("Process description", 'bitpub'),
			 "param_name" => "content",
			 "value" => __("I am test text block. Click edit button to change this text.", 'bitpub'), 
		  )
	   )
	));

?>