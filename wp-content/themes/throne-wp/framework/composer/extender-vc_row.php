<?php


	$attr_fontcolor = array(
		"type" => "colorpicker",
		"holder" => "div",
		"class" => "vc_col-xs-5 vc_settings",
		"group" => __("Design Options", 'bitpub'),
		"heading" => __("Font color", 'bitpub'),
		"param_name" => "font_color",
		"value" => '',
		// "description" => __('Expand section full width (used for sliders in general)', 'bitpub')
	);

	// $attr_overlay = array(
			 // "type" => "ewf-image-select",
			 // "holder" => "div",
			 // "class" => "",
			 // "heading" => __("Overlay Style", 'bitpub'),
			 // "param_name" => "overlay",
			 // "options" => array(
				// 'Transparent' => 'ewf-row-overlay-transparent', 
				// 'Pattern' => 'ewf-row-overlay-pattern', 
			 // ),
			 // "value" => 'ewf-row-overlay-transparent',
		  // );
	
	// vc_add_param( "vc_row",  $attr_overlay);
	
	

	// Remove default composer full width option
	//
	vc_remove_param( "vc_row", "parallax" ); 
	vc_remove_param( "vc_row", "parallax_image" ); 

	vc_add_param( "vc_row",  $attr_fontcolor);

?>