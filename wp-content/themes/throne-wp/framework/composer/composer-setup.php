<?php


#	Visual Composer - Extenders
#
	include_once ('extender-vc_row.php');
	include_once ('composer-page-templates.php');
	include_once ('params/ewf-param-icon/ewf-param-icon.php');  
	include_once ('params/ewf-param-image-select/ewf-param-image-select.php');  
	

#	Visual Composer - EWF Components
#
	
	include_once ('components/vc_ewf_animation.php');
	include_once ('components/vc_ewf_button.php'); 
	// include_once ('components/vc_ewf_client.php');
	include_once ('components/vc_ewf_divider.php'); 
	include_once ('components/vc_ewf_headline.php');
	include_once ('components/vc_ewf_iconbox.php');
	include_once ('components/vc_ewf_message_box.php');
	include_once ('components/vc_ewf_milestone.php'); 
	include_once ('components/vc_ewf_map.php');
	include_once ('components/vc_ewf_process_horizontal.php');
	include_once ('components/vc_ewf_progress.php');
	include_once ('components/vc_ewf_piechart.php');
	include_once ('components/vc_ewf_pricing.php');
	include_once ('components/vc_ewf_social.php'); 
	include_once ('components/vc_ewf_team_member.php'); 
	include_once ('components/vc_ewf_testimonial.php');
	include_once ('components/vc_ewf_blog_posts.php');

	if (post_type_exists(EWF_PROJECTS_SLUG)){	
		include_once ('components/vc_ewf_portfolio_list.php');
		include_once ('components/vc_ewf_portfolio_grid.php');
		include_once ('components/vc_ewf_portfolio_strip.php');
		include_once ('components/vc_ewf_portfolio_filtrable/vc_ewf_portfolio_filtrable.php');
	}
	
	
	
	ewf_composer_load();
	
	
	
#	 Removing animations setting from default composer components
#
	$composer_version = explode( '.', WPB_VC_VERSION);							// 	4.3.2
	$composer_current = $composer_version[0] . '.' . $composer_version[1];		//  4.3
	$composer_shortcodes = array(
		'4.2' => array ( 'vc_column_text', 'vc_message' , 'vc_toggle' , 'vc_single_image' ),
		'4.3' => array ( 'vc_column_text', 'vc_message' , 'vc_toggle' , 'vc_single_image' , 'vc_cta_button' ),
		'4.4' => array ( 'vc_column_text', 'vc_message' , 'vc_toggle' , 'vc_single_image' , 'vc_icon' , 'vc_cta_button' ),
		'4.5' => array ( 'vc_column_text', 'vc_message' , 'vc_toggle' , 'vc_single_image' , 'vc_icon' , 'vc_cta_button' )
	);
	
	if (!empty($composer_shortcodes[$composer_current])){
		foreach($composer_shortcodes[$composer_current] as $shortcode_name){
			vc_remove_param($shortcode_name, 'css_animation');
		}
	}
	
	
	
#	
#		
	add_action( 'vc_before_init', 'ewf_composer_load' );
	
	function ewf_composer_load(){
		
		if(function_exists('vc_set_as_theme')) {
			vc_set_as_theme(true);
		}
		
		if (function_exists('vc_set_template_dir')){
			$dir = get_template_directory() . '/framework/composer/templates/';
			vc_set_template_dir($dir);
		}
		
		if (function_exists('vc_disable_frontend')){
			vc_disable_frontend();
		}
		
		// add_filter('vc_shortcodes_css_class', 'ewf_composer_custom_classes', 10, 2);
		
	}
		
	
#	Remove unsupported components
#

	vc_remove_element("vc_flickr");
	vc_remove_element("vc_message");
	vc_remove_element("vc_tweetmeme");
	vc_remove_element("vc_googleplus");
	vc_remove_element("vc_pinterest");
	vc_remove_element("vc_posts_slider");
	vc_remove_element("vc_button");
	vc_remove_element("vc_cta_button");
	vc_remove_element("vc_cta_button2");
	vc_remove_element("vc_gmaps");
	vc_remove_element("vc_progress_bar");
	vc_remove_element("vc_pie");
	vc_remove_element("vc_custom_heading"); 
	vc_remove_element("vc_toggle"); 
	vc_remove_element("vc_cta"); 
	vc_remove_element("vc_tour"); 
	vc_remove_element("vc_facebook");
	vc_remove_element("vc_text_separator");
	vc_remove_element("vc_separator");
	vc_remove_element("vc_carousel");
	vc_remove_element("vc_posts_grid");
	vc_remove_element("vc_button2");
	
	//	vc_remove_element("vc_btn"); 
	//	vc_gallery 	vc_images_carousel	vc_wp_search	vc_wp_meta	vc_wp_calendar	vc_wp_pages	 vc_wp_recentcomments  vc_wp_tagcloud  vc_wp_custommenu  vc_wp_text
	//	vc_wp_posts  vc_wp_links  vc_wp_categories  vc_wp_archives  vc_wp_rss
	
?>