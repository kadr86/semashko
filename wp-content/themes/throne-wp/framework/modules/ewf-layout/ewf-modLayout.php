<?php
/*	
 *	EWF - Mod Layout
 *	Package: Eazyee Wordpress Framework
 *	ver: 2.0
 *	upd: 24 Iun 2015
 *
 */
 
 
class EWF_ModLayout {
	
	public $settings = array();
	public $debug = array();
	
	
	function __construct(){
		global $ewf_theme_layout;
		
		$layout_default_settings = array(
			'support' 	=> array( 'page'),
			'layout' 	=> array(
				'default' => 'layout-full',		
				'types'  => array(
				
					'blog' => array(
						'layout' 	=> 'layout-sidebar-single-right',
						'sidebar' 	=> 'sidebar-page'
					),
					'page' => array(
						'layout' 	=> 'layout-full',
						'sidebar' 	=> 'sidebar-page'
					),		
					'shop' => array(
						'layout' 	=> 'layout-full',
						'sidebar' 	=> 'sidebar-shop'
					)
				)
			),
			'sidebars' 	=> array(

				#	Fallback sidebar - when you delete the current sidebar this is the default one
				'default' => 'sidebar-page',

				#	The list with uneditable sidebars
				'uneditable' => array(
					'sidebar-page' 		=> array( 'title' => __('Page Sidebar'			, 'bitpub'), 'description' => null ),
				), 

				#	The list with uneditable sidebars
				'install' => array(
					'sidebar-blog'		=> array( 'title' => __('Blog Sidebar'			, 'bitpub'), 'description' => null ),
				),
				
				'theme' => array(),
				
				'registred' => $this->sidebars_get_registred()
			),
			'widget' => array(
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>', 
				'before_widget' => '<div id="%1$s" class="widget %2$s">', 
				'after_widget'  => '</div>'
			)
		);
		
		
		//	Check if there are other custom settings for the layout
		//
		if (!empty($ewf_theme_layout)){
			$this->settings = array_replace_recursive ($layout_default_settings, $ewf_theme_layout);
		}else{
			$this->settings = $ewf_theme_layout;
		}

		
		
		
		add_action('admin_enqueue_scripts'						, array(&$this, 'load_backend'));
	
		add_action('wp_ajax_ewf_modlayout_createsidebar'		, array(&$this, 'ajax_create_sidebar'));
		add_action('wp_ajax_ewf_modlayout_updatesidebar'		, array(&$this, 'ajax_update_sidebar'));
		add_action('wp_ajax_ewf_modlayout_deletesidebar'		, array(&$this, 'ajax_delete_sidebar'));
		
		add_action('save_post'									, array(&$this, 'meta_update'));
		add_action('admin_menu'									, array(&$this, 'meta_register'));
		
		add_action('widgets_init'								, array(&$this, 'sidebars_register'));
		add_action('ewf_theme_install'							, array(&$this, 'sidebars_reset'));
		add_action('after_switch_theme'							, array(&$this, 'sidebars_theme_switch'));
		
	}
	
	
	function load_backend(){
		$includes = '/framework/modules/ewf-layout/includes';

		wp_enqueue_script ('wp-color-picker');
		wp_enqueue_style ('wp-color-picker');

		wp_enqueue_style ('thickbox');
		wp_enqueue_script ('thickbox');

		wp_enqueue_script ('media-upload');

		wp_enqueue_script ('ewf-modLayout-js'				, get_template_directory_uri().$includes.'/ewf-modLayout.js'		, array('jquery')); 		
		wp_enqueue_script ('ewf-modLayout-labels-js'		, get_template_directory_uri().$includes.'/jquery.label.min.js'		, array('jquery')); 		

		wp_register_style ('ewf-modLayout-css'				, get_template_directory_uri().$includes.'/ewf-modLayout.css',  array('dashicons') );
		wp_enqueue_style ('ewf-modLayout-css');

		wp_add_inline_style( 'ewf-modLayout-css'			, $this->load_backend_dynamic() );
	}

	function load_backend_dynamic() {
		global $_wp_admin_css_colors;
		
		$color_scheme = get_user_option( 'admin_color' ); 
		$color_scheme_highlight =  $this->helper_hex2rgb($_wp_admin_css_colors[$color_scheme]->colors[2]);
		
		$custom_css = '
			.ewf-ml-layoutUI.ewf-ml-layoutUI-active { border:2px solid rgba('.$color_scheme_highlight[0].', '.$color_scheme_highlight[1].', '.$color_scheme_highlight[2].', 0.7); }
			.ewf-ml-sidebarsList li.wp-ui-notification:hover  { background:'.$_wp_admin_css_colors[$color_scheme]->colors[2].';color:#fff; }
			
			.ewf-ml-HighlightBackground { background:rgba('.$color_scheme_highlight[0].', '.$color_scheme_highlight[1].', '.$color_scheme_highlight[2].', 0.09); }
			.ewf-ml-sidebarsList li.ewf-ml-HighlightBackground:hover { background:rgba('.$color_scheme_highlight[0].', '.$color_scheme_highlight[1].', '.$color_scheme_highlight[2].', 0.09); }
		'; 
		
		return $custom_css; 
	}


	
#	Remove the deleted sidebars from all the pages
#
	function sidebars_remove($sidebar_id, $default_id = null){		
		global $post;
		
		$data_return = array();
		
		$wp_query_pages = new wp_query(array('showposts' => -1, 'post_type' => 'page'));
		while ($wp_query_pages->have_posts()) : $wp_query_pages->the_post();		
		
			$page_custom_meta = get_post_custom($post->ID);
			if ( is_array($page_custom_meta) &&  array_key_exists('_ewf-page-sidebar',$page_custom_meta)){
				
				if (  $page_custom_meta["_ewf-page-sidebar"][0] == $sidebar_id){
					$data_return[$post->ID] = "true";
					update_post_meta($post->ID, "_ewf-page-sidebar", $default_id); 
				}else{
					$data_return[$post->ID] = "false";
				}
			}
			
		endwhile;
		wp_reset_postdata();
		
		return $data_return;
	}

	
	function sidebars_register(){
		
		// echo '<pre>';
			// print_r($this->debug);
			// print_r($this->settings);
		// echo '</pre>';
		
		$sidebars = array_merge($this->settings['sidebars']['uneditable'], $this->settings['sidebars']['registred'], $this->settings['sidebars']['theme']);
		
		if (is_array($sidebars)){
		
			foreach($sidebars as $sidebar_item_id => $sidebar_item){
				register_sidebar(array(
					'id' => $sidebar_item_id, 
					'name' => $sidebar_item['title'], 
					'description'   => $sidebar_item['description'],
					'before_title'  => $this->settings['widget']['before_title'],
					'after_title'   => $this->settings['widget']['after_title'], 
					'before_widget' => $this->settings['widget']['before_widget'], 
					'after_widget'  => $this->settings['widget']['after_widget']
				));
			}
		}
	}

	
#	Reset all the sidebars from pages to default
#
	function sidebars_reset(){
		update_option('ewf_modLayout_sidebars', serialize( $this->settings['sidebars']['install']) );
	}
	
	function sidebars_theme_switch(){
		if (unserialize(get_option('ewf_modLayout_sidebars', null)) == null){
			
			$this->sidebars_reset();
			
			$this->debug[] = 'sidebars_get_registred - Theme switch detected, restored default sidebars';
		}		
	}
	
	
#	Get all the sidebars registered by an user on EWF Layout
#
	function sidebars_get_registred(){
		$registred_sidebars = array();
		
		
		if (get_option('ewf_modLayout_sidebars', null) == null){		
			$this->sidebars_reset();
			
			$this->debug[] = 'sidebars_get_registred - Reset sidebars';
		}else{
			$registred_sidebars = unserialize(get_option('ewf_modLayout_sidebars', null));
						
			if (!is_array($registred_sidebars) || $registred_sidebars == null){
				$registred_sidebars = array();
				
				$this->debug[] = 'sidebars_get_registred - Sidebars cleaned!';
			}
		}
		
		return $registred_sidebars;
	}
	
	function sidebar($default = null, $post_id = 0){
		global $post;
		
		if ($post_id){
			$item_meta = get_post_custom($post_id);
		}else{
			if (is_object($post)){
				$item_meta = get_post_custom($post->ID);
			}else{
				$item_meta = null;
			}
		}
		
	
		if ( is_array($item_meta) &&  array_key_exists('_ewf-page-sidebar',$item_meta)){
			$sidebar_id = $item_meta["_ewf-page-sidebar"][0];
			
			if ($sidebar_id != null) { 
				$default = $sidebar_id; 
			}		
		}
			
	
		return $default;	
	}
	
	
	
	function meta_update(){
		global $post;
		
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post->ID;
		}
		
		
		if (!is_object($post)){
			 return false;
		}
			
		
		if (is_array($_POST) && array_key_exists('ewf-page-layout', $_POST) && array_key_exists('ewf-page-sidebar', $_POST) ){
			update_post_meta($post->ID, "_ewf-page-layout", $_POST["ewf-page-layout"]);
			update_post_meta($post->ID, "_ewf-page-sidebar", $_POST["ewf-page-sidebar"]);
		}
	}
	
	function meta_register() {
		if ( is_array($this->settings['support']) ){
			foreach($this->settings['support'] as $key => $post_type){
				add_meta_box( 'ewf-layout-sidebars-setup',__('Page layout & sidebars','bitpub'), array(&$this, 'meta_source') , $post_type, 'normal', 'high');
			}
		}
	}
	
	function meta_source() {
			global $post; 
			
			$layouts = array(
				array(
					'icon' => 'layout-sidebar-single-left.png',
					'name' => 'layout-sidebar-single-left',
					'title' => __('Sidebar Left', 'bitpub')
				),
				array(
					'icon' => 'layout-full.png',
					'name' => 'layout-full',
					'title' => __('No Sidebar', 'bitpub')
				),
				array(
					'icon' => 'layout-sidebar-single-right.png',
					'name' => 'layout-sidebar-single-right',
					'title' => __('Sidebar Right', 'bitpub')
				)
			);
			
			$layout_sidebar_id = $this->sidebar( $this->settings['sidebars']['default'] );
			
			$ewf_page_layout = null;
			$custom = get_post_custom($post->ID);
			
			# 	Check if there is a setup layout
			#
				if (array_key_exists('_ewf-page-layout',$custom)){
					$ewf_page_layout = $custom["_ewf-page-layout"][0];
				}else{
					$ewf_page_layout = $this->settings['layout']['default']; 
				}
			
			
			echo '<div class="ewf-modLayout-metaBox">';
				
				echo'<div class="ewf-ml-tabsBar clearfix">
						<ul>
							<li class="active"><a href="#" rel="ewf-ml-tab-layout" class="ewf-icon-layout">'.__('Layout', 'bitpub').'</a></li>
							<li><a href="#" rel="ewf-ml-tab-sidebars" class="ewf-icon-sidebars">'.__('Sidebars', 'bitpub').'</a></li> 
						</ul>
					</div>';
				
				
				
				echo '<div class="ewf-ml-tabscontent clearfix">';
					
					echo '<div class="ewf-ml-tab-layout ewf-ml-tab-content active">';

						echo '<label class="side-new">'.__('Choose the position the sidebar should have on the page', 'bitpub').'</label>'; 

						echo '<div class="ewf-ml-page-layout">';
								if ($ewf_page_layout == 'layout-sidebar-single-left'){ $class_active = ' ewf-ml-layoutUI-active'; }else{ $class_active = null; }
								echo '<div id="layout-sidebar-single-left" class="ewf-ml-layoutUI sidebar-left'.$class_active.'"><div></div><span class="wp-ui-highlight"></span></div>';
							
								if ($ewf_page_layout == 'layout-full'){ $class_active = ' ewf-ml-layoutUI-active'; }else{ $class_active = null; }
								echo '<div id="layout-full" class="ewf-ml-layoutUI no-sidebar'.$class_active.'"><div></div></div>';
								
								if ($ewf_page_layout == 'layout-sidebar-single-right'){ $class_active = ' ewf-ml-layoutUI-active'; }else{ $class_active = null; }
								echo '<div id="layout-sidebar-single-right" class="ewf-ml-layoutUI sidebar-right'.$class_active.'"><div></div><span class="wp-ui-highlight"></span></div>';
						echo '</div>';					
					
					
						$class_layoutBoxFooter = null;
						if ($ewf_page_layout == 'layout-sidebar-single-left' || $ewf_page_layout == 'layout-sidebar-single-right'){
							$class_layoutBoxFooter = 'expanded';
						}
						
						echo '<div class="ewf-ml-layout-boxFooter '.$class_layoutBoxFooter.'">';
							echo '<label>'.__('Choose from the existing sidebars, the one you want this page to use.', 'bitpub').'</label>';
							
							echo '<select id="ewf-page-sidebar" name="ewf-page-sidebar" data-default="'.$this->settings['sidebars']['default'].'">';
								
							
								if (is_array($this->settings['sidebars']['uneditable'])){
									foreach($this->settings['sidebars']['uneditable'] as $sidebar_item_id => $sidebar_item){
										if ($layout_sidebar_id == $sidebar_item_id){
											echo '<option value="'.$sidebar_item_id.'" selected="selected">'.$sidebar_item['title'].'</option>';
										}else{
											echo '<option value="'.$sidebar_item_id.'">'.$sidebar_item['title'].'</option>';
										}
									}									
								}
								
								if (is_array($this->settings['sidebars']['registred'])){
									foreach($this->settings['sidebars']['registred'] as $sidebar_item_id => $sidebar_item){
										if ($layout_sidebar_id == $sidebar_item_id){
											echo '<option value="'.$sidebar_item_id.'" selected="selected">'.$sidebar_item['title'].'</option>';
										}else{
											echo '<option value="'.$sidebar_item_id.'">'.$sidebar_item['title'].'</option>';
										}
									}
								}
								
							echo '</select>';
							
							echo '<a href="#" type="button" class="button button-primary button-large" ><span></span>'.__('Use sidebar', 'bitpub').'</a>';
						echo '</div>';
						
					echo '</div>';
					
					
					
					
					
					echo '<div class="ewf-ml-tab-sidebars ewf-ml-tab-content">';
						echo '<label class="side-new">'.__('This is where you manage the sidebars. Create new sidebars or modify/delete old ones.', 'bitpub').'</label>'; 
						
						echo '<ul class="ewf-ml-sidebarsList">';
								
								foreach($this->settings['sidebars']['registred'] as $sidebar_item_id => $sidebar_item){
									$sidebar_extra_class = null;
									
									if (trim($sidebar_item['description']) == null){ 
										$sidebar_extra_class = ' class="no-description" ';
									}
									
									  echo '<li '.$sidebar_extra_class.' id="'.$sidebar_item_id.'">
												<h4>'.$sidebar_item['title'].'</h4>
												<p>'.$sidebar_item['description'].'</p>
												<span class="ewf-button remove"></span>
												<span class="ewf-button edit"></span>
											</li>';
								}
								
						echo '</ul>
							<div class="ewf-ml-sidebarsListLoading"><span class="load"></span></div>';
							
							
						echo '<div class="ewf-ml-sidebars-boxFooter">';
							
							
							
							echo '<div class="ewf-ml-formCreateSidebar step-1 expanded">';
								
								echo '
								<div class="ewf-ml-create">
									<p><input type="text" value="" name="ewf-ml-formCreateNewTitle" id="ewf-ml-formCreateNewTitle" class="label" data-new-placeholder="'.__('Specify the sidebar title', 'bitpub').'" placeholder="'.__('Sidebar title', 'bitpub').'"  maxlength="30" /></p>
									<p><input type="text" value="" name="ewf-ml-formCreateNewDescription" id="ewf-ml-formCreateNewDescription" class="label" data-new-placeholder="'.__('Add a sidebar description', 'bitpub').'" placeholder="'.__('Sidebar description', 'bitpub').'" /></p>

									<input type="hidden" name="ewf-ml-formCreateOldTitle" id="ewf-ml-formCreateOldTitle" maxlength="30" />
									<input type="hidden" name="ewf-ml-formCreateOldDescription" id="ewf-ml-formCreateOldDescription" />
								</div>
								
								<div class="sidebars-action">
									
									<div class="action-step-1">
										<div class="action-apply">
											<a href="#" type="button" class="button button-primary button-large ewf-ml-btn-createSidebar">'.__('Create sidebar', 'bitpub').'</a>
										</div>										
									</div>
									 
									<div class="action-step-2">
										<div class="action-cancel"><a href="#" class="ewf-ml-btn-cancelSidebar">'.__('Cancel', 'bitpub').'</a></div>
										<div class="action-apply">
											<a href="#" type="button" class="button button-primary button-large ewf-ml-btn-createSidebar">'.__('Create sidebar', 'bitpub').'</a>
										</div>
									</div>
									
								</div>';
								
								
							echo '</div>';
							
							echo '<div class="ewf-ml-formDeleteSidebar">';
								echo '<label>'.__('Are you sure you want to delete sidebar: <strong>Blog Sidebar</strong>', 'bitpub').'</label>'; 
								
							echo '<div class="sidebars-action">
										<div class="action-cancel"><a href="#" class="ewf-ml-btn-cancelDeleteSidebar">'.__('Cancel', 'bitpub').'</a></div>
										<div class="action-apply"><a href="#" type="button" class="button button-primary button-large ewf-ml-btn-deleteSidebar">'.__('Delete sidebar', 'bitpub').'</a></div>
									</div>';
							echo '</div>'; 
							
							echo '<div class="ewf-ml-formEditSidebar">
								<p><input type="text" value="" name="ewf-ml-formEditTitle" id="ewf-ml-formEditTitle" class="label" data-new-placeholder="'.__('Specify the sidebar title', 'bitpub').'" placeholder="'.__('Sidebar title', 'bitpub').'"  maxlength="30"  /></p>
								<p><input type="text" value="" name="ewf-ml-formEditDescription" id="ewf-ml-formEditDescription" class="label" data-new-placeholder="'.__('Add a sidebar description', 'bitpub').'" placeholder="'.__('Sidebar description', 'bitpub').'" /></p>
								<p><input type="hidden" value="" name="" name="ewf-ml-formEditID" id="ewf-ml-formEditID" /></p>
								
								<input type="hidden" value="" id="ewf-ml-formEditOldTitle" />
								<input type="hidden" value="" id="ewf-ml-formEditOldDescription" />
								
								<div class="sidebars-action">
									<div class="action-cancel"><a href="#" class="ewf-ml-btn-cancelSidebar">'.__('Cancel', 'bitpub').'</a></div>
									<div class="action-apply">
										<a href="#" type="button" class="button button-primary button-large ewf-ml-btn-updateSidebar" >'.__('Save changes', 'bitpub').'</a>
									</div>
								</div>';
								
							echo '</div>';
							
							
							
						echo '</div>';
						
					echo '</div>';
					
					
	
					
				echo '</div>';
				
				
				echo '<input type="hidden" id="ewf-page-layout" name="ewf-page-layout" value="'.$ewf_page_layout.'" />';
			echo '</div>';
			
		
			// DEBUG
			//
			// global $_wp_admin_css_colors; 
			// $color_scheme = get_user_option( 'admin_color' ); 

			// echo '<pre>';
				// print_r($_wp_admin_css_colors[$color_scheme]->colors);
				
				// echo '<div class="wp-ui-primary">.wp-ui-primary</div>';
				// echo '<div class="wp-ui-text-primary">.wp-ui-text-primary</div>';
				// echo '<div class="wp-ui-highlight">.wp-ui-highlight</div>';
				// echo '<div class="wp-ui-notification">.wp-ui-notification</div>';
				// echo '<div class="wp-ui-text-notification">.wp-ui-text-notification</div>';
				// echo '<div class="wp-ui-text-highlight">.wp-ui-text-highlight</div>';
				// echo '<div class="wp-ui-text-icon">.wp-ui-text-icon</div>';
			// echo '</pre>';
			
			
	}


	
	function ajax_delete_sidebar(){	
		$result_ajax = array();
		$sidebar_id = 0;
		$sidebars_registred = unserialize(get_option('ewf_modLayout_sidebars', null));
		
		

		if (array_key_exists('id', $_POST)){
			$post_id = intval($_POST['post']);
			$post_sidebar = ewf_get_sidebar_id('*', $post_id);
			
			$sidebar_id = $_POST['id'];
			
			$result_ajax['sidebar_id'] = $sidebar_id;
			$result_ajax['post_id'] = $post_id;
			$result_ajax['sidebar_active'] = false;
			
			
			unset ($sidebars_registred[$sidebar_id]);
			$result_update = update_option('ewf_modLayout_sidebars', stripslashes(serialize($sidebars_registred)));
			
			$this->sidebars_remove($sidebar_id);
			
			if ($result_update && $post_id){
				$result_ajax['sidebar_current'] = $post_sidebar;
				
				if ($post_sidebar == $sidebar_id){
					$result_ajax['sidebar_active'] = true;
					$result_ajax['reset'] = update_post_meta($post_id, "_ewf-page-sidebar",  $this->settings['sidebars']['default']);
				}
			}
			
			if ($result_update){
				wp_send_json_success($result_ajax);
			}else{
				wp_send_json_error($result_ajax);
			}
			
		}else{
			wp_send_json_error($result_ajax);
		}
	}
	
	function ajax_update_sidebar(){
		$sidebars_registred = unserialize(get_option('ewf_modLayout_sidebars', null));
	
		if (array_key_exists('title', $_POST) && array_key_exists('id', $_POST) && array_key_exists('description', $_POST) ){
			$sidebar_id = sanitize_text_field($_POST['id']);
			
			if (array_key_exists($sidebar_id, $sidebars_registred)){
				$sidebars_registred[$sidebar_id]['title'] = sanitize_text_field($_POST['title']);
				$sidebars_registred[$sidebar_id]['description'] = sanitize_text_field($_POST['description']);
			
	
				$result = update_option('ewf_modLayout_sidebars', serialize($sidebars_registred));
				
				if ($result){
					wp_send_json_success(array('sidebar_id' => $sidebar_id));
				}else{
					wp_send_json_error(array('sidebar_id' => $sidebar_id));
				}
			}else{
					wp_send_json_error(array('sidebar_id' => $sidebar_id, 'error' => 'Sidebar not found!'));
			}
			
		}

	}
			
	function ajax_create_sidebar(){
		$sidebars_registred = unserialize(get_option('ewf_modLayout_sidebars', null));

		if (array_key_exists('title', $_POST) && array_key_exists('id', $_POST)){
			$sidebar_id = strtolower(str_replace(array('#', '@', '$', '-', '_', ' '), '_', $_POST['id'])); 
			
			if (array_key_exists($sidebar_id, $sidebars_registred)){
				$sidebar_id = $sidebar_id.rand(1000, 9999);
			}
			
			if (! array_key_exists($sidebar_id, $sidebars_registred)){
				$sidebars_registred[$sidebar_id] = array();
				$sidebars_registred[$sidebar_id]['title'] = sanitize_text_field($_POST['title']);
				$sidebars_registred[$sidebar_id]['description'] = sanitize_text_field($_POST['description']);
				
				$result = update_option('ewf_modLayout_sidebars', serialize($sidebars_registred));
				
				if ($result){
					wp_send_json_success(array('sidebar_id' => $sidebar_id));
				}else{
					wp_send_json_error(array('sidebar_id' => $sidebar_id));
				}
				
			}else{
				wp_send_json_error(array('sidebar_id' => $sidebar_id));
			} 
		}
		
		exit;
	}
	
	
	
	function helper_hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);

	   if(strlen($hex) == 3) {
		  $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		  $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		  $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
		  $r = hexdec(substr($hex,0,2));
		  $g = hexdec(substr($hex,2,2));
		  $b = hexdec(substr($hex,4,2));
	   }
	   
	   $rgb = array($r, $g, $b);

	   return $rgb;
	}
	
}
	
	$ewf_layout = new EWF_ModLayout;
	

	
	function ewf_get_sidebar_id($default = null, $post_id = 0, $return_debug = false){
		global $post;
		
		$debug = array();
		
		$debug['sidebar'] = 'Return page default sidebar: '.$default;
		
		if ($post_id){
			$item_meta = get_post_custom($post_id);
		}else{
			if (is_object($post)){
				$item_meta = get_post_custom($post->ID);
			}else{
				$item_meta = null;
			}
		}
		
	
		if ( is_array($item_meta) &&  array_key_exists('_ewf-page-sidebar',$item_meta)){
			$sidebar_id = $item_meta["_ewf-page-sidebar"][0];
			
			if ($sidebar_id != null) { 
				$default = $sidebar_id; 
				$debug['sidebar'] = 'Return page meta sidebar: '.$default;
			}		
		}
		
		$debug['post'] = $post_id;
		
		if ($return_debug){
			return array('sidebar' => $default, 'debug' => $debug);
		}else{
			return $default;
		}
	}

	function ewf_get_page_layout($default = "layout-full", $post_id = 0){
		global $post;
		$item_meta = array();
		$debug = array();
		
		
		if (!$post_id){
			if (is_object($post)){
				$post_id = $post->ID;
			}else{
				$post_id = 0;
			}
		}
		
		
		if ($post_id){
			$item_meta = get_post_custom($post_id);	// get the item custom variables
		}

		$debug['layout'] = 'Return page default layout: ' . $default . ' [ID: '.$post_id.']';
		
		if (!empty($item_meta['_ewf-page-layout'])){
			$layout = $item_meta["_ewf-page-layout"][0];
			
			$debug['layout'] .= '[not-empty][' . $layout . ']';
			
			if ($layout) { 
				$default = $layout;
				$debug['layout'] = 'Return page meta layout: '.$layout;
			}			
		}
		
		$debug['post'] = $post_id;
		
		return array('layout' => $default, 'debug' => $debug);
	} 
	
	
?>