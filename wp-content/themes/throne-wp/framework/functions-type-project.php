<?php
	
	#	Register shortcodes
	#

	
	function ewf_hlp_queryBuilder($options, $defaults = null){
		global $tax_query_projects, $post;
		
		if (!$defaults){
			$defaults = array(
				"items"					=> 0,
				"id" 					=> null,
				"exclude" 				=> null,
				"exclude" 				=> null,
				"order" 				=> "DESC",
				"unlimited" 			=> false,
				"service" 				=> null
			);
		} 
		
		$options = shortcode_atts($defaults, $options);
		extract($options);	
		
		if ($unlimited){
			$items = '-1'; 
		}
		

		$include_posts = array();
		$exclude_items = array();

		$order = strtoupper($order);
		$query = array( 'post_type' => EWF_PROJECTS_SLUG,
						'order'=> $order, 
						'orderby' => 'date',  
						'posts_per_page'=>$items, 
						// 'paged' => $paged
						); 
		
		
		if ($exclude != null){
			if (is_numeric($exclude)){
				$exclude_items[] = $exclude ;
			}else{
				
				$tmp_id = explode(',', trim($exclude));
				foreach($tmp_id as $key => $item_id){
					if (is_numeric($item_id)){
						$exclude_items[] = $item_id ;
					}
				}
			}
			
			$query['post__not_in'] = $exclude_items;
		}
		
	
		if ($id != null){
			if (is_numeric($id)){
				$include_posts[] = $id ;
			}else{
				$tmp_id = explode(',', trim($id));
				foreach($tmp_id as $key => $item_id){
					if (is_numeric($item_id)){
						$include_posts[] = $item_id ;
					} 
				}
			}
			
			unset($query['post__not_in']);
			unset($query['tax_query']);
			
			$query['post__in'] = $include_posts;
			$query['posts_per_page'] = count($include_posts);
		}
		
		if ($service != null){
			$query['tax_query'] = array(
				array(
					'taxonomy' => EWF_PROJECTS_TAX_SERVICES,
					'field' => 'slug',
					'terms' => $service
				));
		}
		
		
		
		return $query;
	
	}

	
	
	function ewf_helper_query_builder($options){
		
		$defaults = array(	"items" 	=> 3,
							"id" 		=> null,
							"exclude" 	=> null,
							"order" 	=> "DESC",
							"list" 		=> null,
							"service" 	=> null,
							"nav"		=> 1 );
						
		extract(shortcode_atts( $defaults, $options));
		
		
		if ($service == 'All Services'){
			$service = null;
		}
	 
		if ($list != 'service'){
			$service = null;
		}
		
		
		if ($nav){
			$ewf_paged 	= get_query_var('paged') ? get_query_var('paged') : 1;
		}else{
			$ewf_paged 	= 0;
		}
		
		$query = array( 'post_type' => EWF_PROJECTS_SLUG, 'order'=> $order, 'orderby' => 'date', 'paged' => $ewf_paged, 'posts_per_page'=>$items );
		
		if ($list == 'latest'){
			$query['orderby'] = 'date';
			$query['order'] = 'DESC';
		}						
	
		if ($list == 'random'){
			$query['orderby'] = 'rand';
		}
	
		if ($exclude != null){
			if (is_numeric($exclude)){
				$exclude_items[] = $exclude ;
			}else{
				$tmp_id = explode(',', trim($exclude));
				foreach($tmp_id as $key => $item_id){
					if (is_numeric($item_id)){
						$exclude_items[] = $item_id ;
					}
				}
			}
			
			$query['post__not_in'] = $exclude_items;
		}
		
		
		if ($id != null){
			if (is_numeric($id)){
				$include_posts[] = $id ;
			}else{
				$tmp_id = explode(',', trim($id));
				foreach($tmp_id as $key => $item_id){
					if (is_numeric($item_id)){
						$include_posts[] = $item_id ;
					} 
				}
			}
			
			unset($query['post__not_in']);
			unset($query['tax_query']);
			
			$query['post__in'] = $include_posts;
			$query['posts_per_page'] = count($include_posts);
		}
		
		if ($service != null && $list == 'service'){
			$query['tax_query'] = array(
				array(
					'taxonomy' => EWF_PROJECTS_TAX_SERVICES,
					'field' => 'slug',
					'terms' => array( $service )
				));
		}
		
		return $query;
			
	}
	
	function ewf_helper_get_post_extra($post, $thumb_size, $taxonomy , $arg = array( 'term_before' => null, 'term_after' => null )){
		$post_data = array(
			'image-thumb' 	=> null,
			'image-large' 	=> null,
			'image' 		=> 0,
			'terms' 		=> null,
			'terms-comma'	=> null,
			'terms-first'	=> null
		);
		
		
		#	Get image data
		#
		$post_data['image'] = get_post_thumbnail_id();  
		if ($post_data['image']){
			$post_data['image-thumb'] = wp_get_attachment_image_src( $post_data['image'], $thumb_size);
			$post_data['image-thumb'] = $post_data['image-thumb'][0];
			
			$post_data['image-large'] = wp_get_attachment_image_src( $post_data['image'], 'large');	
			$post_data['image-large'] = $post_data['image-large'][0];
		}
		
		
		#	Get terms data
		#
		$all_terms = wp_get_post_terms ($post->ID, $taxonomy);
		$terms_src = null;
		$project_services = null;
		
		
		foreach($all_terms as $key => $term){
			$post_data['terms'] .= $term->slug.' ';
				
			if ($post_data['terms-comma'] == null){
				$post_data['terms-comma'] .= $arg['term_before'] . $term->name . $arg['term_after'];
				$post_data['terms-first'] .= $term->name;
			}else{
				$post_data['terms-comma'] .= ', ' . $arg['term_before'] . $term->name . $arg['term_after'];
			}
		}
		
		
		return $post_data;
	}
	
?>