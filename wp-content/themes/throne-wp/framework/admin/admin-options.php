<?php
	global $ewf_theme_options_defaults;

	$ewf_theme_options = array();
	
	$ewf_theme_options['ewf-panel-general'] = array(
	
				array(    "type" => "ewf-ui-image",
					"image-size" => "full",
					"image-height" => "32",
							"id" => EWF_SETUP_THNAME."_favicon",
				 "section-title" => __("Favicon", 'bitpub'),
		   "section-description" => __("Upload your favicon", 'bitpub'),
						"import" => '/layout/images/default/favicon.png',
						   "std" => null),
						   
				array(    "type" => "ewf-ui-image", 
					"image-size" => "full",
					"image-height" => "64",
							"id" => EWF_SETUP_THNAME."_favicon_retina",
				 "section-title" => __("Favicon Retina", 'bitpub'),
		   "section-description" => __("Upload your favicon", 'bitpub'),
						"import" => '/layout/images/default/apple-touch-icon-144-precomposed.png',
						   "std" => null),

				array(    "type" => "ewf-ui-image", 
					"image-size" => "full",
					"image-height" => "64",
							"id" => EWF_SETUP_THNAME."_ms_tile_image",
				 "section-title" => __("MS Tile Image", 'bitpub'),
		   "section-description" => __("Support for internet explorer pinned tile image", 'bitpub'),
						"import" => '/layout/images/default/mstile.png',
						   "std" => null),
		
			
			   array(     "type" => "ewf-ui-columns-size", 
							"id" => EWF_SETUP_THNAME."_page_sidebar_size",
					   "min" => '2',
					   "max" => '5',
				 "section-title" => __("Page sidebar size", 'bitpub'),
		   "section-description" => __("Select the number of columns on sidebar", 'bitpub'),
						   "std" => $ewf_theme_options_defaults['page_sidebar_size'] ),
						   
						   
			   array(     "type" => "ewf-ui-columns-size", 
							"id" => EWF_SETUP_THNAME."_blog_sidebar_size",
					   "min" => '2',
					   "max" => '5',
				 "section-title" => __("Blog sidebar size", 'bitpub'),
		   "section-description" => __("Select the number of columns on sidebar", 'bitpub'),
						   "std" => $ewf_theme_options_defaults['blog_sidebar_size'] ),
						   
						   
				array(    "type" => "ewf-ui-options", 
							"id" => EWF_SETUP_THNAME."_page_layout",
					   "options" => array(
							'boxed-in'=>array(
									'label' => __('Boxed in', 'bitpub'),
									'value' => 'boxed-in',
									'class' => 'ewf-layout-boxedin'
								), 
							'full-width' => array(
									'label' => __('Full Width', 'bitpub'),
									'value' => 'full-width',
									'class' => 'ewf-layout-fullwidth'
								)
							),
				 "section-title" => __("Layout style", 'bitpub'),
		   "section-description" => __("Select the layout", 'bitpub'),
						   "std" => 'full-width'),	
						   
						   
				array(    "type" => "ewf-ui-select", 
							"id" => EWF_SETUP_THNAME."_page_404",
				 "section-title" => __("Page not found", 'bitpub'),
		   "section-description" => __("Select the 404 page from your existing pages", 'bitpub'),
					   "options" => ewf_load_site_pages(),		
						   "std" => 0),		
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_content_width",
						   "min" => '1300px',
						   "max" => '2000px',
						  "step" => '5',
				 "section-title" => __("Content width", 'bitpub'),
		   "section-description" => __("Select the content width", 'bitpub'),
						   "std" => $ewf_theme_options_defaults['content_width']),


				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_backtotop_button",
				 "section-title" => __("Show back to top button", 'bitpub'),
		   "section-description" => __("You can show or hide the button", 'bitpub'),
						   "std" => 'true'),
						   
					
				array(    "type" => "ewf-ui-background", 
					"image-size" => "thumbnail",
					"image-height" => "50",
							"id" => EWF_SETUP_THNAME."_background",
				 "section-title" => __("Background", 'bitpub'),
		   "section-description" => __("Adjust background settings", 'bitpub'),
						   "std" => json_encode(array(  
										array('name' => 'background-color'			, 'value' => '#ffffff'		),
										array('name' => 'background-pattern'		, 'value' => ''				),
										array('name' => 'background-repeat'			, 'value' => 'repeat-all'	),
										array('name' => 'background-position'		, 'value' => 'center center'),
										array('name' => 'background-image'			, 'value' => ''				),
										array('name' => 'background-image-preview'	, 'value' => ''				),
										array('name' => 'background-attachment'		, 'value' => 'scroll'		),
										array('name' => 'background-stretch'		, 'value' => 'false'		),
									))
								),

								
				array(    "type" => "ewf-ui-textarea",
							"id" => EWF_SETUP_THNAME."_include_css",
				 "section-title" => __('Extra CSS Code', 'bitpub'),
		   "section-description" => __('Paste extra css code here', 'bitpub'),
						   "std" => null ),
	);
	
	
	$ewf_theme_options['ewf-panel-typography'] = array(
	
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_fonts_custom",
				 "section-title" => __("Use custom typography", 'bitpub'),
		   "section-description" => __("You can overwrite custom fonts", 'bitpub'),
					"dependency" => '.group-fonts-custom',
						   "std" => 'false'),	

						   
				array(    "type" => "options-section", "name" => '<strong>'.__("Global Font", 'bitpub').'</strong>', "group" => 'fonts-custom') ,
			

				array(    "type" => "ewf-ui-font", 
							"id" => EWF_SETUP_THNAME."_body_font",
				 "section-title" => __("Font family", 'bitpub'),
		   "section-description" => __("Set the font of the body", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => json_encode(array(
										array('name' => 'font-family', 'value' => 'Raleway'),
										array('name' => 'font-weight', 'value' => '400'),
										array('name' => 'font-italic', 'value' => '')
									))
							),
		
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_body_font_size",
						   "max" => '60',
				 "section-title" => __("Font size", 'bitpub'),
		   "section-description" => __("Set fize of the font", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['body_font_size']),
						  
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_body_font_lineheight",
						   "max" => '60',
				 "section-title" => __("Line height", 'bitpub'),
		   "section-description" => __("Set the font line height", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['body_font_lineheight']),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_body_font_margin",
						   "max" => '60',
				 "section-title" => __("Margin bottom", 'bitpub'),
		   "section-description" => __("Set the bottom spacing", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['body_font_margin']),
													
				
				//
				//	H1
				//
				array(    "type" => "options-section", 
						  "name" => "<strong>".__("Font - Heading 1", 'bitpub')."</strong>",
						 "group" => 'fonts-custom'),
				
				array(    "type" => "ewf-ui-font", 
							"id" => EWF_SETUP_THNAME."_h1_font",
				 "section-title" => __("Font family", 'bitpub'),
		   "section-description" => __("Set the font of the body", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => json_encode(array(
										array('name' => 'font-family', 'value' => 'Roboto'),
										array('name' => 'font-weight', 'value' => '300'),
										array('name' => 'font-italic', 'value' => '')
									))
							),
		
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h1_font_size",
						   "max" => '60',
				 "section-title" => __("Font size", 'bitpub'),
		   "section-description" => __("Set fize of the font", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h1_font_size']),
						  
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h1_font_lineheight",
						   "max" => '60',
				 "section-title" => __("Line height", 'bitpub'),
		   "section-description" => __("Set the font line height", 'bitpub'),
						 "group" => 'fonts-custom',
						    "std" => $ewf_theme_options_defaults['h1_font_lineheight']),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h1_font_margin",
						   "max" => '60',
				 "section-title" => __("Margin bottom", 'bitpub'),
		   "section-description" => __("Set the bottom spacing", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h1_font_margin']),
				
				
				
				//
				//	H2
				//
				array(    "type" => "options-section", 
						  "name" => "<strong>".__("Font - Heading 2", 'bitpub')."</strong>",
						 "group" => 'fonts-custom'),
						 
						 
				array(    "type" => "ewf-ui-font", 
							"id" => EWF_SETUP_THNAME."_h2_font",
				 "section-title" => __("Font family", 'bitpub'),
		   "section-description" => __("Set the font of the body", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => json_encode(array(
										array('name' => 'font-family', 'value' => 'Roboto'),
										array('name' => 'font-weight', 'value' => '300'),
										array('name' => 'font-italic', 'value' => '')
									))
							),
		
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h2_font_size",
						   "max" => '60',
				 "section-title" => __("Font size", 'bitpub'),
		   "section-description" => __("Set fize of the font", 'bitpub'),
						 "group" => 'fonts-custom',
						    "std" => $ewf_theme_options_defaults['h2_font_size']),
						  
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h2_font_lineheight",
						   "max" => '60',
				 "section-title" => __("Line height", 'bitpub'),
		   "section-description" => __("Set the font line height", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h2_font_lineheight']),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h2_font_margin",
						   "max" => '60',
				 "section-title" => __("Margin bottom", 'bitpub'),
		   "section-description" => __("Set the bottom spacing", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h2_font_margin']),
						 
				//
				//	H3
				//
				array(    "type" => "options-section", 
						  "name" => "<strong>".__("Font - Heading 3", 'bitpub')."</strong>",
						 "group" => 'fonts-custom'),
						 
				array(    "type" => "ewf-ui-font", 
							"id" => EWF_SETUP_THNAME."_h3_font",
				 "section-title" => __("Font family", 'bitpub'),
		   "section-description" => __("Set the font of the body", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => json_encode(array(
										array('name' => 'font-family', 'value' => 'Roboto'),
										array('name' => 'font-weight', 'value' => '300'),
										array('name' => 'font-italic', 'value' => '')
									))
							),
		
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h3_font_size",
						   "max" => '60',
				 "section-title" => __("Font size", 'bitpub'),
		   "section-description" => __("Set fize of the font", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h3_font_size']),
						  
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h3_font_lineheight",
						   "max" => '60',
				 "section-title" => __("Line height", 'bitpub'),
		   "section-description" => __("Set the font line height", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h3_font_lineheight']),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h3_font_margin",
						   "max" => '60',
				 "section-title" => __("Margin bottom", 'bitpub'),
		   "section-description" => __("Set the bottom spacing", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h3_font_margin']),
						   
				//
				//	H4
				// 
				array(    "type" => "options-section", 
						  "name" => "<strong>".__("Font - Heading 4", 'bitpub')."</strong>",
						 "group" => 'fonts-custom'),
						 
				array(    "type" => "ewf-ui-font", 
							"id" => EWF_SETUP_THNAME."_h4_font",
				 "section-title" => __("Font family", 'bitpub'),
		   "section-description" => __("Set the font of the body", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => json_encode(array(
										array('name' => 'font-family', 'value' => 'Roboto'),
										array('name' => 'font-weight', 'value' => '300'),
										array('name' => 'font-italic', 'value' => '')
									))
							),
		
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h4_font_size",
						   "max" => '60',
				 "section-title" => __("Font size", 'bitpub'),
		   "section-description" => __("Set fize of the font", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h4_font_size']),
						  
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h4_font_lineheight",
						   "max" => '60',
				 "section-title" => __("Line height", 'bitpub'),
		   "section-description" => __("Set the font line height", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h4_font_lineheight']),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h4_font_margin",
						   "max" => '60',
				 "section-title" => __("Margin bottom", 'bitpub'),
		   "section-description" => __("Set the bottom spacing", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h4_font_margin']),
						   
				//
				//	H5
				// 
				array(    "type" => "options-section", 
						  "name" => "<strong>".__("Font - Heading 5", 'bitpub')."</strong>",
						 "group" => 'fonts-custom'),
						 
				array(    "type" => "ewf-ui-font", 
							"id" => EWF_SETUP_THNAME."_h5_font",
				 "section-title" => __("Font family", 'bitpub'),
		   "section-description" => __("Set the font of the body", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => json_encode(array(
										array('name' => 'font-family', 'value' => 'Roboto'),
										array('name' => 'font-weight', 'value' => '300'),
										array('name' => 'font-italic', 'value' => '')
									))
							),
		
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h5_font_size",
						   "max" => '60',
				 "section-title" => __("Font size", 'bitpub'),
		   "section-description" => __("Set fize of the font", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h5_font_size']),
						  
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h5_font_lineheight",
						   "max" => '60',
				 "section-title" => __("Line height", 'bitpub'),
		   "section-description" => __("Set the font line height", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h5_font_lineheight']),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h5_font_margin",
						   "max" => '60',
				 "section-title" => __("Margin bottom", 'bitpub'),
		   "section-description" => __("Set the bottom spacing", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h5_font_margin']),
						   
				//
				//	H6
				//
				array(    "type" => "options-section", 
						  "name" => "<strong>".__("Font - Heading 6", 'bitpub')."</strong>",
						 "group" => 'fonts-custom'),

				array(    "type" => "ewf-ui-font", 
							"id" => EWF_SETUP_THNAME."_h6_font",
				 "section-title" => __("Font family", 'bitpub'),
		   "section-description" => __("Set the font of the body", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => json_encode(array(
										array('name' => 'font-family', 'value' => 'Roboto'),
										array('name' => 'font-weight', 'value' => '300'),
										array('name' => 'font-italic', 'value' => '')
									))
							),
		
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h6_font_size",
						   "max" => '60',
				 "section-title" => __("Font size", 'bitpub'),
		   "section-description" => __("Set fize of the font", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h6_font_size']),
						  
		
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h6_font_lineheight",
						   "max" => '60',
				 "section-title" => __("Line height", 'bitpub'),
		   "section-description" => __("Set the font line height", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h6_font_lineheight']),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_h6_font_margin",
						   "max" => '60',
				 "section-title" => __("Margin bottom", 'bitpub'),
		   "section-description" => __("Set the bottom spacing", 'bitpub'),
						 "group" => 'fonts-custom',
						   "std" => $ewf_theme_options_defaults['h6_font_margin']),
	);
	
	
	$ewf_theme_options['ewf-panel-shop'] = array(
	
			   array(     "type" => "ewf-ui-columns-size", 
							"id" => EWF_SETUP_THNAME."_shop_sidebar_size",
					   "min" => '2',
					   "max" => '5',
				 "section-title" => __("Shop sidebar size", 'bitpub'),
		   "section-description" => __("Select the number of columns on sidebar", 'bitpub'),
						   "std" => $ewf_theme_options_defaults['shop_sidebar_size'] ),
				
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_shop_items",
						   "min" => '6',
						   "max" => '36',
						  "step" => '3',
						  "unit" => '',
				 "section-title" => __('Items per page', 'bitpub'),
		   "section-description" => __('Number of items to show', 'bitpub'),
						   "std" => $ewf_theme_options_defaults['shop_items']),
	
	);
	
	
	$ewf_theme_options['ewf-panel-header'] = array(
	
				array(    "type" => "ewf-ui-image",
					"image-size" => "ewf-header-logo",
							"id" => EWF_SETUP_THNAME."_logo_url",
				 "section-title" => __("Header logo", 'bitpub'),
		   "section-description" => __("Upload logo in the header", 'bitpub'),
						"import" => '/layout/images/default/logo.png',
						   "std" => null),
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_header_height",
						   "min" => '80px',
						   "max" => '500px',
						  "step" => '1',
				 "section-title" => __("Header height", 'bitpub'),
		   "section-description" => __("Allows you to adjust header height in case the logo doesn't fit properly.", 'bitpub'),
						   "std" => $ewf_theme_options_defaults['header_height']),
						   
								 /*						   
						   
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_header_contact",
				 "section-title" => __("Show contact info", 'bitpub'),
		   "section-description" => null,
 				    "dependency" => '.group-header-contact',
						   "std" => 'false'),	
						   
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_header_menuoffset",
						   "min" => '0px',
						   "max" => '250px',
						  "step" => '1',
				 "section-title" => __("Menu offset", 'bitpub'),
		   "section-description" => __("Allows you to center the menu vertically in relation with the logo. Usefull when you have a very large logo.", 'bitpub'),
						   "std" => '0px'),
						   
						   
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_header_contact_mail",
				 "section-title" => __("Show e-mail", 'bitpub'),
		   "section-description" => null,
					"dependency" => '.group-header-contact-mail',
						   "std" => 'off'),
						   
						   
				array(    "type" => "ewf-ui-text", 
							"id" => EWF_SETUP_THNAME."_header_mail",
				 "section-title" => __('Email from the header', 'bitpub'),
		   "section-description" => __('Specify your e-mail address', 'bitpub'),
						 "group" => 'header-contact-mail',
						   "std" => 'email@website.com' ),

						   
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_header_contact_phone",
				 "section-title" => __("Show phone number", 'bitpub'),
		   "section-description" => null,
					"dependency" => '.group-header-contact-phone',
						   "std" => 'off'),
						   
						   
				array(    "type" => "ewf-ui-text", 
							"id" => EWF_SETUP_THNAME."_header_phone",
				 "section-title" => __('Phone from the header', 'bitpub'),
		   "section-description" => __('Specify your bussines phone number', 'bitpub'),
						 "group" => 'header-contact-phone',
						   "std" => '(123) 456 7890' ),
								 
								 */
			
						   
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_header_sticky",
					"dependency" => '.group-header-sticky',
				 "section-title" => __("Sticky header", 'bitpub'),
		   "section-description" => __("Keep the header visible after scrolling", 'bitpub'),
						   "std" => 'true'),	
						   
				array(    "type" => "ewf-ui-slider", 
							"id" => EWF_SETUP_THNAME."_header_height_sticky",
						   "min" => '80px',
						   "max" => '500px',
						  "step" => '1',
						 "group" => 'header-sticky',
				 "section-title" => __("Sticky header height", 'bitpub'),
		   "section-description" => __("Allows you to specify the height of the sticky header.", 'bitpub'),
						   "std" => $ewf_theme_options_defaults['header_height_sticky']),
						   
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_header_action",
				 "section-title" => __("Use action in header", 'bitpub'),
					"dependency" => '.group-header-action',
						   "std" => $ewf_theme_options_defaults['header_action']),
						   
				array(    "type" => "ewf-ui-text", 
							"id" => EWF_SETUP_THNAME."_header_action_text",
				 "section-title" => __('Header action button text', 'bitpub'),
						 "group" => 'header-action',
						   "std" => null ),

				array(    "type" => "ewf-ui-text", 
							"id" => EWF_SETUP_THNAME."_header_action_link",
				 "section-title" => __('Header action button link', 'bitpub'),
						 "group" => 'header-action',
						   "std" => '#' ),
						   
				// array(    "type" => "ewf-ui-toggle", 
							// "id" => EWF_SETUP_THNAME."_header_search",
				 // "section-title" => __("Search", 'bitpub'),
		   // "section-description" => __("Adds a search box on the right side of the menu", 'bitpub'),
						   // "std" => 'false'),
	
	);
		
	
	$ewf_theme_options['ewf-panel-colors'] = array(
	
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_colors_custom",
				 "section-title" => __("Use custom colors", 'bitpub'),
		   "section-description" => __("You can overwrite the default color scheme", 'bitpub'),
					"dependency" => '.group-colors-custom',
						   "std" => 'false'),	
		
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_accent_01",
				 "section-title" => __('Accent color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_accent_01']),
		
				// array(    "type" => "ewf-ui-color", 
							// "id" => EWF_SETUP_THNAME."_color_accent_02",
				 // "section-title" => __('Accent color 2', 'bitpub'),
		   // "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 // "group" => 'colors-custom',
						   // "std" => $ewf_theme_options_defaults['color_accent_02']),

				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_header_font",
				 "section-title" => __('Header font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_header_font']),						   
						   
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_content_font",
				 "section-title" => __('Content font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_content_font']),	

								
			//	Menu

				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_menu_font",
				 "section-title" => __('Menu font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_menu_font']),
				/*
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_menu_font_hover",
				 "section-title" => __('Menu font hover color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_menu_font_hover']),
						   
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_menu_font_selected",
				 "section-title" => __('Menu font selected color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_menu_font_selected']),
				*/
						   
						   
				#	Dropdown
				#
				
				/*
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_menu_drop_background",
				 "section-title" => __('Dropdown menu font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_menu_drop_background']),
						   
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_menu_drop_divider",
				 "section-title" => __('Dropdown menu divider color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_menu_drop_divider']),

				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_menu_drop_font",
				 "section-title" => __('Dropdown menu font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_menu_drop_font']),

				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_menu_drop_font_hover",
				 "section-title" => __('Dropdown menu font hover color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_menu_drop_font_hover']),
				*/
				
				
				#	Grey
				#
				
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_darkgrey",
				 "section-title" => __('Dark grey color in the theme', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_darkgrey']),
				
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_lightgrey",
				 "section-title" => __('Light grey color in the theme', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_lightgrey']),
				
				#	Footer
				#
				/*
						   
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_footer_top",
				 "section-title" => __('Footer top color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_footer_top']),
						  

				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_footer_top_font",
				 "section-title" => __('Footer top font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_footer_top_font']),
				
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_footer_backgroundr",
				 "section-title" => __('Footer background color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_footer_background']),
						  
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_footer_font",
				 "section-title" => __('Footer font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_footer_font']),
						   
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_footer_divider",
				 "section-title" => __('Footer divider color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_footer_divider']),

						   
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_footer_bottom",
				 "section-title" => __('Footer bottom background color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_footer_bottom']),
						  
						  
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_footer_bottom_font",
				 "section-title" => __('Footer bottom font color', 'bitpub'),
		   "section-description" => __('Specify the color you want to use.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_footer_bottom_font']),
			*/
			
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_ms_tile",
				 "section-title" => __('MS Tile Color', 'bitpub'),
		   "section-description" => __('Support for internet explorer pinned tile color.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_ms_tile']),
						   
				array(    "type" => "ewf-ui-color", 
							"id" => EWF_SETUP_THNAME."_color_android_theme",
				 "section-title" => __('Android theme Color', 'bitpub'),
		   "section-description" => __('Support for Google Chrome tab colors on mobile.', 'bitpub'),
						 "group" => 'colors-custom',
						   "std" => $ewf_theme_options_defaults['color_android_theme']),
	
	);
	
	
	$ewf_theme_options['ewf-panel-footer'] = array(
	
				// array(    "type" => "ewf-ui-toggle", 
							// "id" => EWF_SETUP_THNAME."_footer_top",
				 // "section-title" => __("Footer top", 'bitpub'),
		   // "section-description" => __("Show top footer section ", 'bitpub'),
					// "dependency" => '.group-footer-top',
						   // "std" => 'false'),
						   
						   
			   // array(     "type" => "ewf-ui-columns", 
							// "id" => EWF_SETUP_THNAME."_footer_top_columns",
				 // "section-title" => __("Footer top columns", 'bitpub'),
		   // "section-description" => __("Select the number of columns", 'bitpub'),
						 // "group" => 'footer-top',
						   // "std" => $ewf_theme_options_defaults['footer_top_columns']),
						   
		
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_footer_section",
				 "section-title" => __("Footer section", 'bitpub'),
		   "section-description" => __("Show footer section at the bottom", 'bitpub'),
				    "dependency" => '.group-footer-custom',
						   "std" => 'true'),
						   
						   
			   array(     "type" => "ewf-ui-columns", 
							"id" => EWF_SETUP_THNAME."_footer_columns",
				 "section-title" => __("Footer columns", 'bitpub'),
		   "section-description" => __("Select the number of columns", 'bitpub'),
						 "group" => 'footer-custom',
						   "std" => $ewf_theme_options_defaults['footer_columns'] ),
						   
						   
				array(    "type" => "ewf-ui-toggle", 
							"id" => EWF_SETUP_THNAME."_footer_bottom",
				 "section-title" => __("Footer sub", 'bitpub'),
		   "section-description" => __("Show sub footer section at the bottom", 'bitpub'),
					"dependency" => '.group-footer-sub',
						   "std" => 'true'),	
						   
						   
			   array(     "type" => "ewf-ui-columns", 
							"id" => EWF_SETUP_THNAME."_footer_bottom_columns",
				 "section-title" => __("Footer sub columns", 'bitpub'),
		   "section-description" => __("Select the number of columns", 'bitpub'),
						 "group" => 'footer-sub',
						   "std" => $ewf_theme_options_defaults['footer_bottom_columns']),
						   
	);
	
	
	do_action('ewf_theme_options_append');
	
	
#	echo '<pre style="padding-left:200px;">';
#		print_r($ewf_theme_options);
#	echo '</pre>';
		

	function ewf_admin_load_dynamicStyles(){
		global $ewf_theme_options_defaults;
		
		ob_start();
		
			
			#	Theme Options - Content Width
			#	
			$_eto_content_width = get_option(EWF_SETUP_THNAME."_content_width", $ewf_theme_options_defaults['content_width']);
			echo ".ewf-boxed-layout #wrap { max-width:".$_eto_content_width.";} \n";
			echo ".ewf-boxed-layout #header { max-width:".$_eto_content_width.";} \n";
						
			// echo '.ewf-debug-message  { background-color: #FCFCFC;border-left: 4px solid #7ad03a;box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);padding:12px;margin:20px;color:#555; }';

			
			
			# 	Theme Options - Header Height
			# 	/* */
			
			$header_height = get_option(EWF_SETUP_THNAME."_header_height", $ewf_theme_options_defaults['header_height']);
			if ($header_height != $ewf_theme_options_defaults['header_height']){
				echo '@media (min-width: 1025px) {'."\n";
					echo '#header-wrap { height:'.$header_height.'; }'."\n";
				echo "} \n";
				
				$header_height_height_val = round(($header_height - 24) / 2, 1);
				
				echo '.sf-menu > li > a,'."\n";
				echo '.sf-menu > li.dropdown > a { padding-top: '.$header_height_height_val.'px;padding-bottom: '.$header_height_height_val.'px; }'."\n";
								
				
				$header_height_button = round(($header_height / 2) - 18, 1);
				echo '@media (min-width: 992px) {';
					echo '#header-button { margin-top:'.$header_height_button.'px; }';
				echo '}';
				
				// $header_height_form = round( $header_height_button + 54, 1);
					// echo '#custom-search-form { top:'.$header_height_form.'px; }';

			}
			
			
			$header_sticky = get_option(EWF_SETUP_THNAME."_header_sticky", "true");
			$header_height_sticky = get_option(EWF_SETUP_THNAME."_header_height_sticky", $ewf_theme_options_defaults['header_height_sticky']);
			
			if ($header_sticky === 'true' && $header_height_sticky != $ewf_theme_options_defaults['header_height_sticky']){
				$header_height_sticky_val = round(($header_height_sticky - 24) / 2, 1);
				
				$header_height_button = round(($header_height_sticky /2) - 18, 1);
				
				echo '@media (min-width: 1025px) {';
					echo '#header.stuck  .sf-menu > li > a,';
					echo '#header.stuck  .sf-menu > li.dropdown > a {padding-top:'.$header_height_sticky_val.'px;padding-bottom:'.$header_height_sticky_val.'px; }';
					
					echo '#header.stuck #header-button { margin-top:'.$header_height_button.'px; }';
				echo '}';
			}
				


			# 	Theme Options - Header Menu Offset
			#
			$header_menuoffset = get_option(EWF_SETUP_THNAME."_header_menuoffset", '0px');
			if ($header_menuoffset != '0px'){
				echo '.sf-menu { margin-top:'.$header_menuoffset.'; }';
			}
			
		
		?>
		
		<?php	if (get_option(EWF_SETUP_THNAME."_colors_custom", "false") == 'true'){  ?>	
				
				<?php
				
				

				$color_accent_01 = get_option(EWF_SETUP_THNAME."_color_accent_01"							, $ewf_theme_options_defaults['color_accent_01']);
				// $color_accent_02 = get_option(EWF_SETUP_THNAME."_color_accent_02"							, $ewf_theme_options_defaults['color_accent_02']);
				
				$color_content_font = get_option(EWF_SETUP_THNAME."_color_content_font"						, $ewf_theme_options_defaults['color_content_font']);
				$color_header_font = get_option(EWF_SETUP_THNAME."_color_header_font"						, $ewf_theme_options_defaults['color_header_font']);
												
				$color_darkgrey = get_option(EWF_SETUP_THNAME."_color_darkgrey"								, $ewf_theme_options_defaults['color_darkgrey']);
				$color_lightgrey = get_option(EWF_SETUP_THNAME."_color_lightgrey"							, $ewf_theme_options_defaults['color_lightgrey']);
				
				$color_menu_font = get_option(EWF_SETUP_THNAME."_color_menu_font"							, $ewf_theme_options_defaults['color_menu_font']);
				
				$color_ms_tile = get_option(EWF_SETUP_THNAME."_color_ms_tile"								, $ewf_theme_options_defaults['color_ms_tile']);
				$color_android_theme = get_option(EWF_SETUP_THNAME."_color_android_theme"					, $ewf_theme_options_defaults['color_android_theme']);
				
				
				// $color_menu_font_hover = get_option(EWF_SETUP_THNAME."_color_menu_font_hover"				, $ewf_theme_options_defaults['color_menu_font_hover']);
				// $color_menu_font_selected = get_option(EWF_SETUP_THNAME."_color_menu_font_selected"			, $ewf_theme_options_defaults['color_menu_font_selected']);
				
				// $color_menu_drop_background = get_option(EWF_SETUP_THNAME."_color_menu_drop_background"		, $ewf_theme_options_defaults['color_menu_drop_background']);
				// $color_menu_drop_divider = get_option(EWF_SETUP_THNAME."_color_menu_drop_divider"			, $ewf_theme_options_defaults['color_menu_drop_divider']);
				// $color_menu_drop_font = get_option(EWF_SETUP_THNAME."color_menu_drop_font"					, $ewf_theme_options_defaults['color_menu_drop_font']);
				// $color_menu_drop_font_hover = get_option(EWF_SETUP_THNAME."color_menu_drop_font_hover"		, $ewf_theme_options_defaults['color_menu_drop_font_hover']);

				// $color_footer_background = get_option(EWF_SETUP_THNAME."_color_footer_background"			, $ewf_theme_options_defaults['color_footer_background']);
				// $color_footer_font = get_option(EWF_SETUP_THNAME."_color_footer_font"						, $ewf_theme_options_defaults['color_footer_font']);
				// $color_footer_divider = get_option(EWF_SETUP_THNAME."_color_footer_divider"					, $ewf_theme_options_defaults['color_footer_divider']);
				
				
				?>
		
				/*	###	EWF Custom Colors 	*/
				
				 
body {
	background-color: #ffffff;
	color: <?php echo $color_content_font; ?>;
}

abbr[title] {  
	border-bottom: 1px dotted <?php echo $color_darkgrey; ?>; 
}

blockquote span { 
	color: <?php echo $color_darkgrey; ?>;  
}

hr { 
	border: solid <?php echo $color_darkgrey; ?>; 
}

code, 
pre { 
	color: <?php echo $color_content_font; ?>;	
}

code { 
	border: 1px solid <?php echo $color_darkgrey; ?>;
	background-color: <?php echo $color_lightgrey; ?>;  
	color: #d50f25;  
}

pre { 
	border: 1px solid <?php echo $color_darkgrey; ?>;   
	background-color: <?php echo $color_lightgrey; ?>; 
}

.hr { 
	border-top: 1px solid <?php echo $color_darkgrey; ?>;  
}

.text-highlight { color: <?php echo $color_accent_01; ?>; background:yellow; }

.mute{ 
	color: <?php echo $color_darkgrey; ?>;
}

@media (max-width: 767px) {

	body { 
		background-color: #fff; 
	}
	
}

a, 
a:visited,
a:hover, 
a:focus { 
	color: <?php echo $color_accent_01; ?>; 
}	

h1, 
h2, 
h3, 
h4, 
h5, 
h6,
h1 a, 
h2 a, 
h3 a, 
h4 a, 
h5 a, 
h6 a,
h1 a:visited, 
h2 a:visited, 
h3 a:visited, 
h4 a:visited, 
h5 a:visited, 
h6 a:visited,
h1 a:hover, 
h2 a:hover, 
h3 a:hover, 
h4 a:hover, 
h5 a:hover, 
h6 a:hover,
h1 a:focus, 
h2 a:focus, 
h3 a:focus, 
h4 a:focus, 
h5 a:focus, 
h6 a:focus {
	color: <?php echo $color_header_font; ?>;
}

table th, 
table td {
	border-top: 1px solid <?php echo $color_darkgrey; ?>;
}

::-moz-placeholder,
::-webkit-input-placeholder,
:-ms-input-placeholder {
	color: <?php echo $color_darkgrey; ?>;
}

input[type="text"],
input[type="password"],
input[type="date"],
input[type="datetime"],
input[type="datetime-local"],
input[type="month"],
input[type="week"],
input[type="email"],
input[type="number"],
input[type="search"],
input[type="tel"],
input[type="time"],
input[type="url"],
input[type="color"],
textarea {
	color: <?php echo $color_content_font; ?>;
	border: 1px solid <?php echo $color_darkgrey; ?>;
	background-color: #fff;
}

input[type="text"]:focus,
input[type="password"]:focus,
input[type="date"]:focus,
input[type="datetime"]:focus,
input[type="datetime-local"]:focus,
input[type="month"]:focus,
input[type="week"]:focus,
input[type="email"]:focus,
input[type="number"]:focus,
input[type="search"]:focus,
input[type="tel"]:focus,
input[type="time"]:focus,
input[type="url"]:focus,
input[type="color"]:focus,
textarea:focus {
	border-color: <?php echo $color_darkgrey; ?>;
}

input[type="text"]:disabled,
input[type="password"]:disabled,
input[type="date"]:disabled,
input[type="datetime"]:disabled,
input[type="datetime-local"]:disabled,
input[type="month"]:disabled,
input[type="week"]:disabled,
input[type="email"]:disabled,
input[type="number"]:disabled,
input[type="search"]:disabled,
input[type="tel"]:disabled,
input[type="time"]:disabled,
input[type="url"]:disabled,
input[type="color"]:disabled,
textarea:disabled {
	background-color: <?php echo $color_lightgrey; ?>;
}

input[type="text"][disabled],
input[type="text"][readonly],
fieldset[disabled] input[type="text"],
input[type="password"][disabled],
input[type="password"][readonly],
fieldset[disabled] input[type="password"],
input[type="date"][disabled],
input[type="date"][readonly],
fieldset[disabled] input[type="date"],
input[type="datetime"][disabled],
input[type="datetime"][readonly],
fieldset[disabled] input[type="datetime"],
input[type="datetime-local"][disabled],
input[type="datetime-local"][readonly],
fieldset[disabled] input[type="datetime-local"],
input[type="month"][disabled],
input[type="month"][readonly],
fieldset[disabled] input[type="month"],
input[type="week"][disabled],
input[type="week"][readonly],
fieldset[disabled] input[type="week"],
input[type="email"][disabled],
input[type="email"][readonly],
fieldset[disabled] input[type="email"],
input[type="number"][disabled],
input[type="number"][readonly],
fieldset[disabled] input[type="number"],
input[type="search"][disabled],
input[type="search"][readonly],
fieldset[disabled] input[type="search"],
input[type="tel"][disabled],
input[type="tel"][readonly],
fieldset[disabled] input[type="tel"],
input[type="time"][disabled],
input[type="time"][readonly],
fieldset[disabled] input[type="time"],
input[type="url"][disabled],
input[type="url"][readonly],
fieldset[disabled] input[type="url"],
input[type="color"][disabled],
input[type="color"][readonly],
fieldset[disabled] input[type="color"],
textarea[disabled],
textarea[readonly],
fieldset[disabled] textarea {
	background-color: <?php echo $color_lightgrey; ?>;
}

select {
	color: <?php echo $color_content_font; ?>;
	border: 1px solid <?php echo $color_darkgrey; ?>;
}

select:disabled {
	background-color: <?php echo $color_lightgrey; ?>;
}

select:focus { border-color: <?php echo $color_darkgrey; ?>; }

button,
input[type="reset"],
input[type="submit"],
input[type="button"]{
	border: 2px solid <?php echo $color_accent_01; ?>;
	background-color: <?php echo $color_accent_01; ?>;
}

.javascript-required,
.modern-browser-required{
	background-color: #d50f25;
	color: #fff;
}

.wpb_accordion_header {
	border: 1px solid <?php echo $color_darkgrey; ?> !important;
	background-color: #fff !important;
	color: <?php echo $color_content_font; ?> !important;
}

.wpb_accordion_header a { color: <?php echo $color_content_font; ?> !important; }

.wpb_accordion_header.ui-state-hover,
.wpb_accordion_header.ui-state-active {
	background-color: <?php echo $color_accent_01; ?> !important;
	border-color: #fff !important;
}

.wpb_accordion_header.ui-state-hover a,
.wpb_accordion_header.ui-state-active a { color: #fff !important;}

.wpb_accordion_header:after, .wpb_accordion_header:after {
	color: <?php echo $color_content_font; ?>;
}
.wpb_accordion_header.ui-state-active:after {
	color: #fff;
}

.wpb_accordion_header.ui-state-hover:after { color: #fff; }

.alert {
	background-color: <?php echo $color_accent_01; ?>;
	color: #fff;
}

.alert.info {
	background-color: #f1f3f4;
	color: <?php echo $color_content_font; ?>;
}

.alert.success {
	background-color: #d0dadf;
	color: <?php echo $color_content_font; ?>;
}

.alert.error {
	background-color: #474d50;
	color: #fff;
}

.alert.warning {
	background-color: #7b868c;
	color: #fff;
}

.btn { 
	border: 2px solid <?php echo $color_accent_01; ?>;
	background-color: <?php echo $color_accent_01; ?>;
	color: #fff;
}

a.btn { color: #fff; }

.btn.alt {
	color: <?php echo $color_accent_01; ?>;
}

a.btn.alt { color: <?php echo $color_accent_01; ?>; }

ul.fill-circle li:before {
	background-color: <?php echo $color_content_font; ?>;
}

ul.fill-circle.border li {
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}	
	
.divider.single-line { border-top: 1px solid <?php echo $color_darkgrey; ?>; }

.divider.double-line { border-top: 4px double <?php echo $color_darkgrey; ?>; }

.headline h4 { 
	color: <?php echo $color_content_font; ?>;
}

.icon-box-1 > i { 
	color: <?php echo $color_accent_01; ?>;
} 
	
.icon-box-2 > i {
	border: 2px solid <?php echo $color_accent_01; ?>;
	color: <?php echo $color_accent_01; ?>;
}

.icon-box-2:hover > i {
	background-color: <?php echo $color_accent_01; ?>;
	color: #fff;	
}

.icon-box-2 h5 small {
	color: <?php echo $color_content_font; ?>;
}

.icon-box-3 > h4 { 
	border: 2px solid <?php echo $color_header_font; ?>;
} 

.icon-box-3 h3 a {
	color: <?php echo $color_header_font; ?>;
}

.icon-box-3:hover h3 a {
	color: <?php echo $color_accent_01; ?>;
}

.icon-box-3:hover > h4 {
	border-color: <?php echo $color_accent_01; ?>;
	color: <?php echo $color_accent_01; ?>;
}

.icon-box-4 > i {
	color: <?php echo $color_accent_01; ?>;
}

.icon-box-5 > i {
	color: <?php echo $color_accent_01; ?>;
}

.icon-box-5 h4 a { color: <?php echo $color_accent_01; ?>; }

.info-box {
	background-color: <?php echo $color_lightgrey; ?>;
}

.info-box-2 {
	background-color: <?php echo $color_accent_01; ?>;
	color: #fff;
}

.info-box-2 h1,
.info-box-2 h2,
.info-box-2 h3,
.info-box-2 h4,
.info-box-2 h5,
.info-box-2 h6 { color: #fff; }

.info-box-2 .btn { 
	background-color: #fff; 
	color: <?php echo $color_accent_01; ?>;
}

.info-box-2 .btn.alt {
	border-color: #fff;
	background-color: transparent;
	color: #fff;
}

#box-1 {
	background-color: <?php echo $color_header_font; ?>;
}

.milestone .milestone-content {
	color: <?php echo $color_accent_01; ?>;
}

.milestone .milestone-description {
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
	color: <?php echo $color_header_font; ?>;
}

.horizontal-process-builder:before {
	border-top: 1px solid <?php echo $color_darkgrey; ?>;
}

.horizontal-process-builder li:before {
	border: 1px solid <?php echo $color_darkgrey; ?>;
	background-color: #fff;
}

.horizontal-process-builder li:hover:before {
	border-color: <?php echo $color_darkgrey; ?>;
}

.vertical-process-builder:before {
	border-left: 1px solid <?php echo $color_darkgrey; ?>;
}

.vertical-process-builder li i,
.vertical-process-builder li h1 {
	border: 1px solid <?php echo $color_darkgrey; ?>;
	outline: 10px solid #fff;
	background-color: #fff;
}

.vertical-process-builder li:hover i,
.vertical-process-builder li:hover h1 {
	background-color: <?php echo $color_lightgrey; ?>;
}

.pricing-table {
	border: 2px solid <?php echo $color_darkgrey; ?>;
}

.pricing-table.alt {
	border-color: <?php echo $color_darkgrey; ?>;
	background-color: <?php echo $color_darkgrey; ?>;
}

.pricing-table-header h1 sup {
	color: <?php echo $color_content_font; ?>;
}

.pricing-table-header h1 sub {
	color: <?php echo $color_content_font; ?>;
}

.pricing-table:hover { border-color: <?php echo $color_accent_01; ?>; }
.pricing-table:hover .pricing-table-header h1 { color: <?php echo $color_accent_01; ?>; }

.pricing-table.alt:hover {
	border-color: <?php echo $color_accent_01; ?>;
	background-color: <?php echo $color_accent_01; ?>;
	color: #fff;
}

.pricing-table.alt:hover .pricing-table-header h1 sup,
.pricing-table.alt:hover .pricing-table-header h1 sub,
.pricing-table.alt:hover .pricing-table-header h1,
.pricing-table.alt:hover .pricing-table-header h3 { color: #fff; }

.pricing-table.alt:hover .btn { border-color: #fff; }

.progress-bar-description {
	color: <?php echo $color_header_font; ?>;
}

.progress-bar {
	background-color: <?php echo $color_lightgrey; ?>;
	box-shadow: inset 0 1px 5px 0 <?php echo $color_darkgrey; ?>;
}

.progress-bar .progress-bar-outer {
	background-color: <?php echo $color_accent_01; ?>;
}

a.social-icon {
	color: <?php echo $color_content_font; ?>;
}


.table-bordered { 
	border: 1px solid <?php echo $color_darkgrey; ?>; 		
}

.table-bordered th, 
.table-bordered td { border-left: 1px solid <?php echo $color_darkgrey; ?>; }

.table-striped tbody tr:nth-child(odd) td,
.table-striped tbody tr:nth-child(odd) th { background-color: <?php echo $color_lightgrey; ?>; }	

@media (max-width: 480px) {

	tr { border-top: 1px solid <?php echo $color_darkgrey; ?>; }

  
}

.wpb_tabs_nav  {
	border-bottom: none !important;
}

.wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav a {
	border: 1px solid <?php echo $color_darkgrey; ?>;
	color: <?php echo $color_content_font; ?>;
}

.wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav li:last-child a { border-right: 1px solid <?php echo $color_darkgrey; ?> !important; }

.wpb_content_element .wpb_tabs_nav li.ui-tabs-active a {
	border-bottom-color: #fff;
	background-color: #fff !important;
	color: <?php echo $color_accent_01; ?> !important;
}

.wpb_content_element.wpb_tabs .wpb_tour_tabs_wrapper .wpb_tab {
	border: 1px solid <?php echo $color_darkgrey; ?> !important; 
	background-color: transparent !important;
}

.wpb_content_element .wpb_tabs_nav li.ui-tabs-active, 
.wpb_content_element .wpb_tabs_nav li:hover { background-color: transparent !important; }

.wpb_content_element .wpb_tabs_nav li:hover a { color: <?php echo $color_accent_01; ?>; }

.testimonial > i {
	color: <?php echo $color_accent_01; ?>;
}

.testimonial > h5 {
	color: <?php echo $color_accent_01; ?>;
}

.testimonial blockquote p span { color: <?php echo $color_accent_01; ?>; }

.testimonial h5 span { color: <?php echo $color_darkgrey; ?>; }

.team-member h5 small {
	color: <?php echo $color_accent_01; ?>;
}

.portfolio-item-overlay {
	background-color: #fff;
}

.portfolio-item-overlay .portfolio-item-description {
	color: <?php echo $color_content_font; ?>;
}

.portfolio-item-description h5 { color: <?php echo $color_header_font; ?>; }

.portfolio-item-description h5 a {
	color: <?php echo $color_header_font; ?>;
}	

.portfolio-item-description h5 a:hover { 
	color: <?php echo $color_header_font; ?>;
}

.portfolio-item-overlay-actions .portfolio-item-zoom i,
.portfolio-item-overlay-actions .portfolio-item-link i {
	color: #fff;  
}

.portfolio-filter ul li a {
	color: <?php echo $color_content_font; ?>;
}

.portfolio-filter ul li a:hover,
.portfolio-filter ul li a.active { color: <?php echo $color_accent_01; ?>; }	

.pagination a { 
	border: 2px solid <?php echo $color_accent_01; ?>;
}

.pagination li.current a,
.pagination li a:hover { 
	background-color: <?php echo $color_accent_01; ?>; 
	color: #fff; 
}	

.portfolio-switcher ul li.active a,
.portfolio-switcher ul li a:hover { border-color: <?php echo $color_accent_01; ?>; }

.project-navigation a {
	color: <?php echo $color_header_font; ?>;
}

.project-navigation a:hover { color: <?php echo $color_accent_01; ?>; }

.blog-post {
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}

.sticky-post {
	background-color: <?php echo $color_accent_01; ?>;
	border: 2px solid <?php echo $color_accent_01; ?>;
	color: #fff;	
}

.blog-post-title p a {
	color: <?php echo $color_header_font; ?>;
}

.blog-post-title p a small {
	color: <?php echo $color_content_font; ?>;
}

.widget {
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}

.textwidget blockquote:before {
	color: <?php echo $color_accent_01; ?>;
}

#s { 
	border: 2px solid <?php echo $color_accent_01; ?>; 
}

#s:focus { border-color: #bbb; }

.widget_pages a {
	color: <?php echo $color_content_font; ?>;
}

.widget_pages a:hover { 
	color: <?php echo $color_accent_01; ?>;
}

#footer-bottom .widget_pages ul li {
	border-right: 2px solid <?php echo $color_content_font; ?>;
}

.widget_archive a {
	color: <?php echo $color_content_font; ?>;
}

.widget_archive a:hover { 
	color: <?php echo $color_accent_01; ?>;
}

.widget_categories a {
	color: <?php echo $color_content_font; ?>;
}

.widget_categories a:hover { 
	color: <?php echo $color_accent_01; ?>;
}

.widget_meta a {
	color: <?php echo $color_content_font; ?>;
}

.widget_meta a:hover { 
	color: <?php echo $color_accent_01; ?>;
}

.widget_tag_cloud a { 
	border: 2px solid <?php echo $color_accent_01; ?>;
	background-color: <?php echo $color_accent_01; ?>;
	color: #fff;
}

.widget_tag_cloud a:hover { 
	background-color: #fff;
	color: <?php echo $color_accent_01; ?>; 
}

.widget_nav_menu a {
	color: <?php echo $color_content_font; ?>;
}

.widget_nav_menu a:hover { 
	color: <?php echo $color_accent_01; ?>;
}

.ewf_widget_navigation a {
	color: <?php echo $color_content_font; ?>;
}

.ewf_widget_navigation a:hover { 
	color: <?php echo $color_accent_01; ?>;
}

.commentlist .vcard cite.fn a.url {
	color: <?php echo $color_content_font; ?>;
}

.commentlist .comment-meta a { color: <?php echo $color_content_font; ?>; }

.commentlist > li,
.commentlist > li ul.children li {
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}

.commentlist > li.pingback {
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}

#comment-form { 
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}

.tp-bullets.simplebullets.round .bullet {
	background: <?php echo $color_content_font; ?>;
}

.tp-bullets.simplebullets.round .bullet.selected { background: <?php echo $color_accent_01; ?>;  }

.tp-leftarrow.default { background-color: <?php echo $color_header_font; ?>; }
.tp-rightarrow.default { background-color: <?php echo $color_header_font; ?>; }

.caption.title {
	color: <?php echo $color_header_font; ?>;
}

.caption.text {
	color: #fff;
}

.caption.text-2 {
	color: <?php echo $color_content_font; ?>;
}

.caption.text-3 {
	color: <?php echo $color_content_font; ?>;
}

.caption .btn {
	color: #fff;
}

.caption .btn:hover { color: #fff; }
.caption .btn.alt:hover { color: <?php echo $color_accent_01; ?>; }


@media (min-width: 768px) and (max-width: 991px) {
	
	.caption .btn {
		color: #fff;
	}
	
}

@media (max-width: 767px) {

	.caption .btn {
		color: #fff;
	}
	
}	

.woocommerce table th {
	color: #fff;
}	

.address {
	border: 2px solid <?php echo $color_darkgrey; ?>;		
}

.woocommerce a.added_to_cart {
	background-color: <?php echo $color_accent_01; ?>;
	border: 2px solid <?php echo $color_accent_01; ?>;
	color: #fff;
}

#wrap {
	background-color: #fff;
}

.sf-menu a {
	color: <?php echo $color_menu_font; ?>; 
}

.sf-menu > li > a,
.sf-menu > li.dropdown > a {
	color: <?php echo $color_menu_font; ?>;
}

.sf-menu > li > a:hover:before,
.sf-menu > li.current > a:before,
.sf-menu li.sfHover > a:before { border-top-color: <?php echo $color_accent_01; ?>; }


.sf-menu li.dropdown ul {	
	background-color: #fff;			
}

.sf-menu li.dropdown ul:before {
	border-top: 2px solid <?php echo $color_accent_01; ?>;
}

.sf-menu li.dropdown ul a:hover,
.sf-menu li.dropdown ul li.dropdown.sfHover > a { color: <?php echo $color_accent_01; ?>; }

.sf-mega {
	background-color: #fff;
}

.sf-menu .sf-mega:before {
	border-top: 2px solid <?php echo $color_accent_01; ?>;
}

.sf-mega ul li a:hover { color: <?php echo $color_accent_01; ?>; }

.sf-arrows .sf-with-ul:after {
	border-top-color: <?php echo $color_darkgrey; ?>;
}
	
.sf-arrows > li > .sf-with-ul:focus:after,
.sf-arrows > li:hover > .sf-with-ul:after,
.sf-arrows > .sfHover > .sf-with-ul:after { border-top-color: rgba(0, 0, 0, 0.7); }
	
.sf-arrows ul .sf-with-ul:after {
	border-left-color: <?php echo $color_darkgrey; ?>;
}

.sf-arrows ul li > .sf-with-ul:focus:after,
.sf-arrows ul li:hover > .sf-with-ul:after,
.sf-arrows ul .sfHover > .sf-with-ul:after { border-left-color: rgba(0, 0, 0, 0.7); }

#mobile-menu {
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}

#mobile-menu .sf-mega {
	background: #fff;
}

#mobile-menu li a {
	border-top: 1px solid <?php echo $color_darkgrey; ?>;
	color: <?php echo $color_menu_font; ?>;
}

#mobile-menu .mobile-menu-submenu-arrow {
	border-left: 1px solid <?php echo $color_darkgrey; ?>;
	color: <?php echo $color_menu_font; ?>;
}

#mobile-menu .mobile-menu-submenu-arrow:hover { background-color: <?php echo $color_lightgrey; ?>; }

@media (min-width: 1025px) {
	
	#header.stuck {
		box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.95);
	}

}

#page-header {
	border-top: 1px solid <?php echo $color_darkgrey; ?>;
	border-bottom: 1px solid <?php echo $color_darkgrey; ?>;
}

#back-to-top {
	background-color: <?php echo $color_header_font; ?>;
	color: #fafafa;
}

#back-to-top:hover { background-color: rgba(0, 0, 0, 0.7); }
#back-to-top:hover i { color: #fff; }

.bx-wrapper .bx-pager {
	color: <?php echo $color_header_font; ?>;
}

.bx-wrapper .bx-pager.bx-default-pager a {
	border: 1px solid <?php echo $color_accent_01; ?>;		
}

.bx-wrapper .bx-pager.bx-default-pager a:hover,
.bx-wrapper .bx-pager.bx-default-pager a.active { background: <?php echo $color_accent_01; ?>; }


			
		<?php	}	?>		
		
		
		
		
		<?php	
			
			

			#	Theme Options - Background
			#	
			$_body_background = ewf_admin_ui_font_decode(EWF_SETUP_THNAME."_background");
			echo "body { ".$_body_background."}\n" ;
						

			#	Theme Options - Typography
			#				
			if (get_option(EWF_SETUP_THNAME."_fonts_custom", 'false') == 'true'){
				echo "\n/*	###	EWF Custom Typography  */ \n";
				 

				#	Global Font
				#
				$_body_font = ewf_admin_ui_font_decode( EWF_SETUP_THNAME."_body_font", true);
				$_body_font_size = 'font-size:'.get_option(EWF_SETUP_THNAME."_body_font_size", $ewf_theme_options_defaults['body_font_size'])."; \n";
				$_body_font_lineheight = 'line-height:'.get_option(EWF_SETUP_THNAME."_body_font_lineheight", $ewf_theme_options_defaults['body_font_lineheight'])."; \n";
				echo "body { ".$_body_font['css'].$_body_font_size.$_body_font_lineheight."\n }" ;
				
				
				$_h1_font = ewf_admin_ui_font_decode( EWF_SETUP_THNAME."_h1_font", true);
				$_h1_font_size = 'font-size:'.get_option(EWF_SETUP_THNAME."_h1_font_size", $ewf_theme_options_defaults['h1_font_size'])."; \n";
				$_h1_font_lineheight = 'line-height:'.get_option(EWF_SETUP_THNAME."_h1_font_lineheight", $ewf_theme_options_defaults['h1_font_lineheight'])."; \n";
				$_h1_font_margin = 'margin-bottom:'.get_option(EWF_SETUP_THNAME."_h1_font_margin", $ewf_theme_options_defaults['h1_font_margin'])."; \n";
				echo "h1 { ".$_h1_font['css'].$_h1_font_size.$_h1_font_lineheight.$_h1_font_margin."}\n\n" ;
				
				
				$_h2_font = ewf_admin_ui_font_decode( EWF_SETUP_THNAME."_h2_font", true);
				$_h2_font_size = 'font-size:'.get_option(EWF_SETUP_THNAME."_h2_font_size", $ewf_theme_options_defaults['h2_font_size'])."; \n";
				$_h2_font_lineheight = 'line-height:'.get_option(EWF_SETUP_THNAME."_h2_font_lineheight", $ewf_theme_options_defaults['h2_font_lineheight'])."; \n";
				$_h2_font_margin = 'margin-bottom:'.get_option(EWF_SETUP_THNAME."_h2_font_margin", $ewf_theme_options_defaults['h2_font_margin'])."; \n";
				echo "h2 { ".$_h2_font['css'].$_h2_font_size.$_h2_font_lineheight.$_h2_font_margin."}\n\n" ;
				
				
				$_h3_font = ewf_admin_ui_font_decode( EWF_SETUP_THNAME."_h3_font", true);
				$_h3_font_size = 'font-size:'.get_option(EWF_SETUP_THNAME."_h3_font_size", $ewf_theme_options_defaults['h3_font_size'])."; \n";
				$_h3_font_lineheight = 'line-height:'.get_option(EWF_SETUP_THNAME."_h3_font_lineheight", $ewf_theme_options_defaults['h3_font_lineheight'])."; \n";
				$_h3_font_margin = 'margin-bottom:'.get_option(EWF_SETUP_THNAME."_h3_font_margin", $ewf_theme_options_defaults['h3_font_margin'])."; \n";
				echo "h3 { ".$_h3_font['css'].$_h3_font_size.$_h3_font_lineheight.$_h3_font_margin."}\n\n" ;
				
				
				$_h4_font = ewf_admin_ui_font_decode( EWF_SETUP_THNAME."_h4_font", true);
				$_h4_font_size = 'font-size:'.get_option(EWF_SETUP_THNAME."_h4_font_size", $ewf_theme_options_defaults['h4_font_size'])."; \n";
				$_h4_font_lineheight = 'line-height:'.get_option(EWF_SETUP_THNAME."_h4_font_lineheight", $ewf_theme_options_defaults['h4_font_lineheight'])."; \n";
				$_h4_font_margin = 'margin-bottom:'.get_option(EWF_SETUP_THNAME."_h4_font_margin", $ewf_theme_options_defaults['h4_font_margin'])."; \n";
				echo "h4 { ".$_h4_font['css'].$_h4_font_size.$_h4_font_lineheight.$_h4_font_margin."}\n\n" ;
				

				$_h5_font = ewf_admin_ui_font_decode( EWF_SETUP_THNAME."_h5_font", true);
				$_h5_font_size = 'font-size:'.get_option(EWF_SETUP_THNAME."_h5_font_size", $ewf_theme_options_defaults['h5_font_size'])."; \n";
				$_h5_font_lineheight = 'line-height:'.get_option(EWF_SETUP_THNAME."_h5_font_lineheight",$ewf_theme_options_defaults['h5_font_lineheight'])."; \n";
				$_h5_font_margin = 'margin-bottom:'.get_option(EWF_SETUP_THNAME."_h5_font_margin", $ewf_theme_options_defaults['h5_font_margin'])."; \n";
				echo "h5 { ".$_h5_font['css'].$_h5_font_size.$_h5_font_lineheight.$_h5_font_margin."}\n\n" ;
				

				$_h6_font = ewf_admin_ui_font_decode( EWF_SETUP_THNAME."_h6_font", true);
				$_h6_font_size = 'font-size:'.get_option(EWF_SETUP_THNAME."_h6_font_size", $ewf_theme_options_defaults['h6_font_size'])."; \n";
				$_h6_font_lineheight = 'line-height:'.get_option(EWF_SETUP_THNAME."_h6_font_lineheight", $ewf_theme_options_defaults['h6_font_lineheight'])."; \n";
				$_h6_font_margin = 'margin-bottom:'.get_option(EWF_SETUP_THNAME."_h6_font_margin", $ewf_theme_options_defaults['h6_font_margin'])."; \n";
				echo "h6 { ".$_h6_font['css'].$_h6_font_size.$_h6_font_lineheight.$_h6_font_margin."}\n\n" ;


			}	
		
		
		return ob_get_clean();
	
	}
	
?>