<?php


	add_action( 'tgmpa_register', 'ewf_register_required_plugins' );

	function ewf_register_required_plugins() {
		
		$plugins = array(
			
			array(
				'name' 					=> 'Contact Form 7',
				'slug' 					=> 'contact-form-7', 
				'required' 				=> false,
			),
			array(
				'name'					=> 'Custom Visual Composer for Throne Theme',
				'slug'					=> 'js_composer',
				'source'				=>  get_template_directory().'/framework/plugins/js_composer.zip',
				'required'				=> false,
				'version'				=> '1.5.3',
				'force_activation'		=> false,
				'force_deactivation'	=> false,
				'external_url'			=> '',
			),
			array(
				'name'					=> 'Parallax Backgrounds for VC',
				'slug'					=> 'parallax-backgrounds-for-vc',
				'source'				=>  get_template_directory().'/framework/plugins/parallax-backgrounds-for-vc.zip',
				'required'				=> false,
				'version'				=> '3.6.1',
				'force_activation'		=> false,
				'force_deactivation'	=> false,
				'external_url'			=> '',
			),
			array(
				'name'					=> 'Revolution Slider',
				'slug'					=> 'revslider',
				'source'				=> get_template_directory().'/framework/plugins/revslider.zip',
				'required'				=> false,
				'version'				=> '4.6.93',
				'force_activation'		=> false,
				'force_deactivation'	=> false,
				'external_url'			=> '',
			),
			array(
				'name'					=> 'EWF Portfolio',
				'slug'					=> 'ewf-portfolio',
				'source'				=> get_template_directory().'/framework/plugins/ewf-portfolio.zip',
				'required'				=> false,
				'version'				=> '1.0.0',
				'force_activation'		=> false,
				'force_deactivation'	=> false,
				'external_url'			=> '',
			)
			// array(
				// 'name' 					=> 'WooCommerce',
				// 'slug' 					=> 'woocommerce', 
				// 'required' 				=> false,
				// 'force_activation'		=> false,
				// 'force_deactivation'	=> false,
			// ),
		);

		$config = array(
			'domain'       		=> 'bitpub',         	// Text domain - likely want to be the same as your theme.
			'default_path' 		=> '',                       	  	// Default absolute path to pre-packaged plugins
			'menu'         		=> 'install-required-plugins', 		// Menu slug
			'parent_slug' 		=> 'themes.php', 					// Default parent URL slug
			'has_notices'      	=> true,                       		// Show admin notices or not
			'is_automatic'    	=> true,					   		// Automatically activate plugins after installation or not
			'message' 			=> '',								// Message to output right before the plugins table
			'strings'      		=> array(
				'page_title'                       			=> __( 'Install Required Plugins', 'bitpub' ),
				'menu_title'                       			=> __( 'Install Plugins', 'bitpub' ),
				'installing'                       			=> __( 'Installing Plugin: %s', 'bitpub' ), // %1$s = plugin name
				'oops'                             			=> __( 'Something went wrong with the plugin API.', 'bitpub' ),
				'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'bitpub' ), // %1$s = plugin name(s)
				'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'bitpub' ), // %1$s = plugin name(s)
				'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'bitpub' ), // %1$s = plugin name(s)
				'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'bitpub' ), // %1$s = plugin name(s)
				'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'bitpub' ), // %1$s = plugin name(s)
				'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'bitpub' ), // %1$s = plugin name(s)
				'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'bitpub' ), // %1$s = plugin name(s)
				'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'bitpub' ), // %1$s = plugin name(s)
				'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'bitpub' ),
				'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'bitpub' ),
				'return'                           			=> __( 'Return to Required Plugins Installer', 'bitpub' ),
				'plugin_activated'                 			=> __( 'Plugin activated successfully.', 'bitpub' ),
				'complete' 									=> __( 'All plugins installed and activated successfully. %s', 'bitpub' ),
				'nag_type'									=> 'updated' 
			)
		);

		tgmpa( $plugins, $config );		
	
	}
 
?>