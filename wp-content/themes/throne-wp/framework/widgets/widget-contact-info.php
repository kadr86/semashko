<?php


	class ewf_widget_contact_info extends WP_Widget {

		function ewf_widget_contact_info() {
			$widget_ops = array( 'classname' => 'ewf_widget_contact_info', 'description' => __('A widget that displays brochure item', 'bitpub') );
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ewf_widget_contact_info' );
			parent::__construct('ewf_widget_contact_info', __('EWF - Contact Info', 'bitpub'), $widget_ops, $control_ops );
		}
		


		function widget( $args, $instance ) {
			extract( $args );
			global $post;

			echo wp_kses_post($before_widget);

			$title = esc_html( apply_filters('widget_title', $instance['title']) );
			if ( $title ){
				echo wp_kses_post($before_title) . esc_html($title) . wp_kses_post($after_title);
			}
			
			if (empty($instance['icons'])){
				$instance['icons'] = 'off';
			}
		
			
			if ($instance['icons'] == 'on'){
				$link_detail = array(
					'address' => '<i class="ifc-home"></i>',
					  'phone' => '<i class="ifc-phone2"></i>',
						'fax' => '<i class="ifc-print"></i>',
					  'email' => '<i class="ifc-message"></i>'
				);
			}else{
				$link_detail = array(
					'address' => __('Address:' , 'bitpub'),
					  'phone' => __('Tel: ' , 'bitpub'),
						'fax' => __('Fax: ' , 'bitpub'),
				      'email' => __('Email: ' , 'bitpub')
				);				
			}
			
			
			echo '<ul>';
				
				$address = esc_html($instance['address']);
				if ($address){
					echo '<li>'.$address.'</li>';
				}	
				
				$phone = esc_html( $instance['phone'] );
				if ($phone){
					echo '<li>'.$link_detail['phone'].$phone.'</li>';
				}
				
				$fax = esc_html( $instance['fax'] );
				if ($fax){
					echo '<li>'.$link_detail['fax'].$fax.'</li>';
				}
				
				$email = esc_html( $instance['email'] );
				if ($email){
					echo '<li><span>'.$link_detail['email'].'</span><a href="mailto:'.$email.'">'.$email.'</a></li>';
				}
				
			echo '</ul>';
			
			echo wp_kses_post($after_widget);
		}
	 
		
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;


		#	Data Validation
		#
			$instance['title'] 		= sanitize_text_field($new_instance['title']);
			$instance['address'] 	= sanitize_text_field($new_instance['address']);
			$instance['email'] 		= sanitize_email($new_instance['email']);
			$instance['phone'] 		= sanitize_text_field($new_instance['phone']);
			$instance['fax'] 		= sanitize_text_field($new_instance['fax']);
			// $instance['icons'] 		= sanitize_text_field($new_instance['icons']);

			return $instance;
		}
		 

		function form( $instance ) {
			$defaults = array( 'title' => null, 'address' => null, 'email' => null, 'phone' => null, 'icons' => null, 'fax' => null);
			$instance = wp_parse_args( (array) $instance, $defaults ); 

			?>
			
				<div class="ewf-meta">
					<p>
						<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'bitpub'); ?></label>
						<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_html($instance['title']); ?>" style="width:100%;" />
					</p>

					<p>
						<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e('Address:', 'bitpub'); ?></label>
						<input id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" value="<?php echo esc_html($instance['address']); ?>" style="width:100%;" />
					</p>
					
					<p>
						<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone:', 'bitpub'); ?></label>
						<input id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo esc_html($instance['phone']); ?>" style="width:100%;" />
					</p>
					
					<p>
						<label for="<?php echo $this->get_field_id( 'fax' ); ?>"><?php _e('Fax:', 'bitpub'); ?></label>
						<input id="<?php echo $this->get_field_id( 'fax' ); ?>" name="<?php echo $this->get_field_name( 'fax' ); ?>" value="<?php echo esc_html($instance['fax']); ?>" style="width:100%;" />
					</p>
					
					<p>
						<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e('E-mail:', 'bitpub'); ?></label>
						<input id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo esc_html($instance['email']); ?>" style="width:100%;" />
					</p>
					
				</div>
			
			<?php
			
		}
	}


	function widget_contact_checkbox($value){
		if ($value == "on"){
			echo " checked='yes' ";
		}
	}
	
	register_widget( 'ewf_widget_contact_info' 	);


?>