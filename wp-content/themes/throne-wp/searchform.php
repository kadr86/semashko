<form method="get" id="searchform" name="searchform" class="searchform" action="<?php echo esc_url( home_url() ); ?>/">
	<div>
		<label class="screen-reader-text" for="s"><?php _e('Search for:', 'bitpub'); ?></label> 
		<input type="text" value="<?php the_search_query(); ?>" placeholder="<?php //_e('Search', 'bitpub'); ?>" name="s" id="s" />
		<input id="type" name="post_type" value="post" type="hidden">
		<input id="searchsubmit" type="submit" value="<?php _e('Search', 'bitpub'); ?>">
	</div>
</form>