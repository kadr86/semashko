<?php  get_header(); ?>

<?php

#	 Get page settings
#
	$page_data = ewf_get_page_settings( basename(__FILE__) );
	
	
	if ($page_data['page']['id']){
		
		$page_content = get_post($page_data['page']['id']);
		$page_data['content'] = $page_content->post_content;
		
		switch ($page_data['layout']){
		
			case "layout-sidebar-single-left": 
				echo '<div class="container">';
				echo '<div class="row">';
					echo '<div class="span'.$page_data['spans']['sidebar'].'">';
						dynamic_sidebar($page_data['sidebar']);
					echo '</div>';
					echo '<div class="span'.$page_data['spans']['content'].'">';
						
						echo do_shortcode($page_data['content']);
						
					echo '</div>';
				echo '</div>';
				echo '</div>';
				break;
		
			case "layout-sidebar-single-right": 
				echo '<div class="container">';
				echo '<div class="row">';
					echo '<div class="span'.$page_data['spans']['content'].'">';

						echo do_shortcode($page_data['content']);

					echo '</div>';
					echo '<div class="span'.$page_data['spans']['sidebar'].'">';
						dynamic_sidebar($page_data['sidebar']);
					echo '</div>';
				echo '</div>';
				echo '</div>';
				break; 
		
			case "layout-full": 
					
				if ($page_data['page']['composer']){
					echo do_shortcode($page_data['content']);
				}else{
				
					echo '<div class="container">';
					echo '<div class="row ewf-no-composer">';
						echo '<div class="span12">';
						
							echo do_shortcode($page_data['content']);
							
						echo '</div>';
					echo '</div>';
					echo '</div>';
				
				}
			}
		
	}else{
	
		echo '<div id="content" style=" padding-top:160px; min-height: 300px;">';
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span12">';
					echo '<div class="alert alert-info"><p class="archive-title">'.__('Error 404 - Page not found!', 'bitpub').'</p></div>';
				echo '</div>'; 
			echo '</div>'; 	
		echo '</div>'; 
		echo '</div>'; 
		
	}

?>
<?php	get_footer();  ?>