		
		<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
		
		</div><!-- end #content -->
		
		<?php 
		#	Check if is a blank page template
		
			global $ewf_theme_footer;
			
			$page_blank = ewf_is_page_blank();
			
			if (!$page_blank){ 
		?>
		
		<?php 

			
			if (!empty($ewf_theme_footer['footer'])) {
				echo '<div id="footer">';
				echo '<!-- /// FOOTER     ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->';


					echo '<div class="container">';
						echo '<div class="row">';
						foreach($ewf_theme_footer['footer'] as $col_index => $col_span){
							echo '<div class="span'.$col_span.'" id="footer-widget-area-'.($col_index+1).'">';
							
								if (is_active_sidebar('footer-widgets-mid-'.($col_index+1))){
									dynamic_sidebar('footer-widgets-mid-'.($col_index+1));
								}else{
									echo __("Footer widget area", 'bitpub').' '.($col_index+1);
								}
								
							echo '</div>';
						}
						echo '</div>';
					echo '</div>';
            
			
				echo '<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->';
				echo '</div><!-- end #footer -->';
			}
		
		?>
		
		<?php 
		
			
			if (!empty($ewf_theme_footer['footer-bottom'])) {
				
				echo '<div id="footer-bottom">';
				echo '<!-- /// FOOTER BOTTOM  ///////////////////////////////////////////////////////////////////////////////////////////////////// -->';
				
					echo '<div class="container">';
						echo '<div class="row">';
						foreach($ewf_theme_footer['footer-bottom'] as $col_index => $col_span){
							echo '<div class="span'.$col_span.'" id="footer-bottom-widget-area-'.($col_index+1).'">';
							
								if (is_active_sidebar('footer-widgets-bot-'.($col_index+1))){
									dynamic_sidebar('footer-widgets-bot-'.($col_index+1)); 
								}else{
									echo __("Footer bottom widget area", 'bitpub').' '.($col_index+1);
								}
								
							echo '</div>';
						}
						echo '</div>';
					echo '</div>';
					
					
				echo '<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->';
				echo '</div>';
				
			}
		
		?>

		
		<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

		<?php 
			
		#	Check if is a blank page template
		
			} 
		?>		
	
		
	</div><!-- end #wrap -->

	<?php
		
		if (get_option(EWF_SETUP_THNAME."_backtotop_button", 'true') == 'true' && !$page_blank){
			echo '<a id="back-to-top" href="#">
				<i class="ifc-up4"></i>
			</a>';
		}
		
	?>
	
	<?php wp_footer(); ?>

</body>
</html>