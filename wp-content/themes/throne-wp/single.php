<?php get_header(); ?>

<?php 
	
#	 Get page settings
#
	$page_data = ewf_get_page_settings( basename(__FILE__) );
	
	
	switch ($page_data['layout']) {
	
		case "layout-sidebar-single-left": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
					
					dynamic_sidebar($page_data['sidebar']);
					
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['content'].'">';
				
					if ( have_posts() ) while ( have_posts() ) : the_post(); 
						get_template_part('templates/blog-item-default');
					endwhile; 
					
				echo '</div>';
			echo '</div>';		
			echo '</div>';		
			break;
			
	
		case "layout-sidebar-single-right":
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span'.$page_data['spans']['content'].'">';
					
					if ( have_posts() ) while ( have_posts() ) : the_post(); 
						get_template_part('templates/blog-item-default');
					endwhile; 
						
				echo '</div>';
				echo '<div class="span'.$page_data['spans']['sidebar'].'">';
				
					dynamic_sidebar($page_data['sidebar']);
					
				echo '</div>';
			echo '</div>';	
			echo '</div>';	
			break;
	
		case "layout-full": 
			echo '<div class="container">';
			echo '<div class="row">';
				echo '<div class="span12">';
				
					if ( have_posts() ) while ( have_posts() ) : the_post(); 
						get_template_part('templates/blog-item-default');
					endwhile; 
					
				echo '</div>';
			echo '</div>';	
			echo '</div>';	
			break;
			
	}
	
?>
<?php get_footer(); ?>