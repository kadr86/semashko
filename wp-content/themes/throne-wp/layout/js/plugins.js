(function($){

	"use strict";

/* ==========================================================================
   When document is ready, do
   ========================================================================== */
   
	$(document).ready(function(){

	
		// twitterFetcher
		// http://jasonmayes.com/projects/twitterApi/
		
		if(typeof twitterFetcher !== 'undefined' && ($('.ewf_widget_latest_tweets').length > 0)) {
			
			$('.ewf_widget_latest_tweets').each(function(index){
			
				var account_id = $('.ewf-tweet-list', this).attr('data-account-id'),
					items = $('.ewf-tweet-list', this).attr('data-items'),
					newID = 'ewf-tweet-list-' + index;
				
				$('.ewf-tweet-list', this).attr('id', newID);
				
				var config = {
				  "id": account_id,
				  "domId": newID,
				  "maxTweets": items,
				  "showImages": false,
				  "showInteraction": false,
				  "showUser": false
				};
				
				twitterFetcher.fetch(config);
			});
			
		}
		
		
	
		// simplePlaceholder - polyfill for mimicking the HTML5 placeholder attribute using jQuery
		// https://github.com/marcgg/Simple-Placeholder/blob/master/README.md
		
		if(typeof $.fn.simplePlaceholder !== 'undefined'){
			
			$('input[placeholder], textarea[placeholder]').simplePlaceholder();
		
		}
		
		// Fitvids - fluid width video embeds
		// https://github.com/davatron5000/FitVids.js/blob/master/README.md
		
		if(typeof $.fn.fitVids !== 'undefined'){
			
			$('.fitvids').fitVids();
		
		}
		
		// Superfish - enhance pure CSS drop-down menus
		// http://users.tpg.com.au/j_birch/plugins/superfish/options/
		
		if(typeof $.fn.superfish !== 'undefined'){
			
			$('#menu').superfish({
				delay: 500,
				animation: {opacity:'show',height:'show'},
				speed: 100,
				cssArrows: false
			});
			
		}
		
		// bxSlider - responsive slider
		// http://bxslider.com/options
		
		if(typeof $.fn.bxSlider !== 'undefined'){

			// Latest posts widget
			
			$('.latest-posts-slider .slides').bxSlider({
				 mode: 'fade',					// Type of transition between slides: 'horizontal', 'vertical', 'fade'		
				 speed: 800,							// Slide transition duration (in ms)
				 infiniteLoop: true,					// If true, clicking "Next" while on the last slide will transition to the first slide and vice-versa.
				 hideControlOnEnd: false,				// If true, "Next" control will be hidden on last slide and vice-versa. Only used when infiniteLoop: false
				 pager: false,							// If true, a pager will be added
				 pagerType: 'full',						// If 'full', a pager link will be generated for each slide. If 'short', a x / y pager will be used (ex. 1/5)
				 controls: true,						// If true, "Next" / "Prev" controls will be added
				 auto: false,							// If true, slides will automatically transition
				 pause: 4000,							// The amount of time (in ms) between each auto transition
				 autoHover: true,						// Auto show will pause when mouse hovers over slider
				 useCSS: false,							// If true, CSS transitions will be used for animations. False, jQuery animations. Setting to false fixes problem with jQuery 2.1.0 and mode:horizontal
				 nextSelector: '#next-post',
				 prevSelector: '#prev-post',
				 nextText: '<i class="fa fa-angle-right"></i>',
				 prevText: '<i class="fa fa-angle-left"></i>'
			});

			// Testimonials Slider
			
			$('.testimonial-slider .slides').bxSlider({
				 mode: 'horizontal',					// Type of transition between slides: 'horizontal', 'vertical', 'fade'		
				 speed: 800,							// Slide transition duration (in ms)
				 infiniteLoop: true,					// If true, clicking "Next" while on the last slide will transition to the first slide and vice-versa.
				 hideControlOnEnd: false,				// If true, "Next" control will be hidden on last slide and vice-versa. Only used when infiniteLoop: false
				 pager: false,							// If true, a pager will be added
				 pagerType: 'full',						// If 'full', a pager link will be generated for each slide. If 'short', a x / y pager will be used (ex. 1/5)
				 controls: true,						// If true, "Next" / "Prev" controls will be added
				 auto: false,							// If true, slides will automatically transition
				 pause: 4000,							// The amount of time (in ms) between each auto transition
				 autoHover: true,						// Auto show will pause when mouse hovers over slider
				 useCSS: false,							// If true, CSS transitions will be used for animations. False, jQuery animations. Setting to false fixes problem with jQuery 2.1.0 and mode:horizontal
				 nextSelector: '#next-testimonial',
				 prevSelector: '#prev-testimonial',
				 nextText: '<i class="fa fa-angle-right"></i>',
				 prevText: '<i class="fa fa-angle-left"></i>'
			});
			
		}
		
		// OwlCarousel 
		if(typeof $.fn.owlCarousel !== 'undefined'){
		
			$(".client-logos-slider").each(function(){
				var items_number = $('.client-item', this).length;
				
				$(this).owlCarousel({
					// Most important owl features
					items : items_number,
					itemsCustom : false,
					itemsDesktop : [1399,4],
					itemsDesktopSmall : [979,3],
					itemsTablet: [767,2],
					itemsTabletSmall: false,
					itemsMobile : [480,1],
					 
					//Basic Speeds
					slideSpeed : 200,
					 
					//Autoplay
					autoPlay : false,
					stopOnHover : true,
					 
					// Navigation
					navigation : false,
					 
					//Pagination
					pagination : true,
					 
					// Responsive
					responsive: true,
				});
			});
			
		}
				
		// Magnific PopUp - responsive lightbox
		// http://dimsemenov.com/plugins/magnific-popup/documentation.html
		
		if(typeof $.fn.magnificPopup !== 'undefined'){
		
			$('.magnificPopup').magnificPopup({
				disableOn: 400,
				closeOnContentClick: true,
				type: 'image'
			});
			
			$('.magnificPopup-gallery').magnificPopup({
				disableOn: 400,
				type: 'image',
				gallery: {
					enabled: true
				}
			});
		
		}
		
		
		
		
		
		// gMap -  embed Google Maps into your website; uses Google Maps v3
		// http://labs.mario.ec/jquery-gmap/
		
		if(typeof $.fn.gMap !== 'undefined'){
			
			$('.google-map').each(function() {
				
				var $t = $(this),
					mapZoom = 15,
					mapAddress = $t.attr("data-address"),
					mapCaption = $t.attr("data-caption"),
					mapType = "ROADMAP",
					mapHeight = $t.attr("data-height"),
					popUp = false;
				
				if ($t.attr("data-zoom") !== undefined) {
					mapZoom = parseInt($t.attr("data-zoom"),10);
				}	
				
				if ($t.attr("data-height") !== undefined) {
					$t.css( "height", mapHeight+'px');
				}
				
				if ($t.attr("data-maptype") !== undefined) {
					mapType = $t.attr("data-maptype");
				} 
				
				if ($t.attr("data-popup") !== undefined) {
					popUp = $t.data("data-popup");
				} 
				
				$t.gMap({
					maptype: mapType,
					scrollwheel: false,
					zoom: mapZoom,
					markers: [{
						address: mapAddress,
						html: mapCaption,
						popup: popUp
					}],
					controls: {
						panControl: true,
						zoomControl: true,
						mapTypeControl: true,
						scaleControl: false,
						streetViewControl: false,
						overviewMapControl: false
					}
				});
		
			});
			
		}
		
		
		
		
	});
		
	
/* ==========================================================================
   When the window is scrolled, do
   ========================================================================== */
   	
	$(window).scroll(function () {
	
		
		
	});

})(window.jQuery);

// non jQuery plugins below

