(function($) {
	
	"use strict";
	
/* ==========================================================================
   ewfRowBehaviour - improves visual composer row resize
   ========================================================================== */

if ( typeof window[ 'ewf_rowBehaviour' ] !== 'function' ) {
	
	// console.log('- INIT - Ewf Row Behaviour -');
	window.ewf_rowBehaviour = function () {
			 
		var $ = window.jQuery;
		var local_function = function () {
			
			var $elements = $( '[data-ewf-full-width="true"]' );
			var $el_wrap = $('#wrap');
			
			$.each( $elements, function ( key, item ) {
				var $el = $( this );
				var $el_full = $el.next( '.vc_row-full-width' );
				var el_margin_left = parseInt( $el.css( 'margin-left' ), 10 );
				var el_margin_right = parseInt( $el.css( 'margin-right' ), 10 );
				var offset = 0 - ($el_full.offset().left - $el_wrap.offset().left) - el_margin_left;
				var width = $el_wrap.width();			 
				
				if ( ! $el.data( 'ewfStretchContent' ) ) {
					var padding = (- 1 * offset);
					if ( padding < 0 ) {
						padding = 0;
					}
					
					var paddingRight = $el_wrap.width() - padding - $el_full.width() + el_margin_left + el_margin_right;
					if ( paddingRight < 0 ) {
						paddingRight = 0;
					}
					
					$el.css( { 'padding-left': padding + 'px', 'padding-right': paddingRight + 'px' } );
				}
				
				$el.css( {
					'position': 'relative',
					'left': offset,
					'box-sizing': 'border-box',
					'width': (width)
				} );
				
				$el.attr( "data-ewf-full-width-init", "true" );
			} );
		};
		
		
		$( window ).bind( 'resize.ewf_rowBehaviour', local_function );
		local_function();
	}

}
	
	
	
	
/* ==========================================================================
   ieViewportFix - fixes viewport problem in IE 10 SnapMode and IE Mobile 10
   ========================================================================== */
   
	function ieViewportFix() {
	
		var msViewportStyle = document.createElement("style");
		
		msViewportStyle.appendChild(
			document.createTextNode(
				"@-ms-viewport { width: device-width; }"
			)
		);

		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			
			msViewportStyle.appendChild(
				document.createTextNode(
					"@-ms-viewport { width: auto !important; }"
				)
			);
		}
		
		document.getElementsByTagName("head")[0].
				appendChild(msViewportStyle);

	}
	

/* ==========================================================================
   Update cart items
   ========================================================================== */		
	
	var ewf_wooShop_buttons = [
		".edd-add-to-cart",
		".wpsc_buy_button",
		".eshopbutton",
		"div.cartopt p label.update input#update",
		".add_to_cart_button",
		".woocommerce-cart input.minus",
		".cart_item a.remove",
		"#order_review .opc_cart_item a.remove",
		".woocommerce-cart input.plus"
	];

	jQuery(document.body).on('click', ewf_wooShop_buttons.join(','), function(){
		ewf_wooShop_timeout();
	});
    
	function ewf_wooShop_timeout() {
		setTimeout(function () { ewf_wooShop_update(); }, 1000);
	}
	
	function ewf_wooShop_update() {

		$.post( siteURL+'/wp-admin/admin-ajax.php', { action:"ewf_wooshop_update"}, function(received){
			if (received.success){
				$('.ewf-shop-cart span').html(received.data.quantity);
			}

		}, "json"); 
	
		return;
	}
	

/* ==========================================================================
   exists - Check if an element exists
   ========================================================================== */		
	
	function exists(e) {
		return $(e).length > 0;
	}

/* ==========================================================================
   isTouchDevice - return true if it is a touch device
   ========================================================================== */

	function isTouchDevice() {
		return !!('ontouchstart' in window) || ( !! ('onmsgesturechange' in window) && !! window.navigator.maxTouchPoints);
	}

/* ==========================================================================
   setDimensionsPieCharts
   ========================================================================== */
	
	function setDimensionsPieCharts() {

		$('.pie-chart').each(function() {

			var $t = $(this),
				n = $t.parent().width(),
				r = $t.attr("data-barSize");
			
			if (n < r) {
				r = n;
			}
			
			$t.css("height", r);
			$t.css("width", r);
			$t.css("line-height", r + "px");
			
			$t.find("i").css({
				"line-height": r + "px",
				"font-size": r / 3
			});
			
		});

	}

/* ==========================================================================
   animatePieCharts
   ========================================================================== */

	function animatePieCharts() {

		if(typeof $.fn.easyPieChart !== 'undefined'){

			$('.pie-chart:in-viewport').each(function() {
	
				var $t = $(this),
					n = $t.parent().width(),
					r = $t.attr("data-barSize"),
					l = "square";
				
				if ($t.attr("data-lineCap") !== undefined) {
					l = $t.attr("data-lineCap");
				} 
				
				if (n < r) {
					r = n;
				}
				
				$t.easyPieChart({
					animate: 1300,
					lineCap: l,
					lineWidth: $t.attr("data-lineWidth"),
					size: r,
					barColor: $t.attr("data-barColor"),
					trackColor: $t.attr("data-trackColor"),
					scaleColor: "transparent",
					onStep: function(from, to, percent) {
						$(this.el).find('.pie-chart-percent span').text(Math.round(percent));
					}
	
				});
				
			});
			
		}

	}

/* ==========================================================================
   animateMilestones
   ========================================================================== */

	function animateMilestones() {

		$('.milestone:in-viewport').each(function() {
			
			var $t = $(this),
				n = $t.find(".milestone-value").attr("data-stop"),
				r = parseInt($t.find(".milestone-value").attr("data-speed"), 10);
				
			if (!$t.hasClass("already-animated")) {
				$t.addClass("already-animated");
				$({
					countNum: $t.find(".milestone-value").text()
				}).animate({
					countNum: n
				}, {
					duration: r,
					easing: "linear",
					step: function() {
						$t.find(".milestone-value").text(Math.floor(this.countNum));
					},
					complete: function() {
						$t.find(".milestone-value").text(this.countNum);
					}
				});
			}
			
		});

	}

/* ==========================================================================
   animateProgressBars
   ========================================================================== */

	function animateProgressBars() {

		$('.progress-bar .progress-bar-outer:in-viewport').each(function() {
			
			var $t = $(this);
			
			if (!$t.hasClass("already-animated")) {
				$t.addClass("already-animated");
				$t.animate({
					width: $t.attr("data-width") + "%"
				}, 2000);
			}
			
		});

	}


/* ==========================================================================
   handleMobileMenu 
   ========================================================================== */		

	var MOBILEBREAKPOINT = 992;

	function handleMobileMenu() {

		if ($(window).width() > MOBILEBREAKPOINT) {
			
			$("#mobile-menu").hide();
			$("#mobile-menu-trigger").removeClass("mobile-menu-opened").addClass("mobile-menu-closed");
		
		} else {
			
			if (!exists("#mobile-menu")) {
				
				$("#menu").clone().attr({
					id: "mobile-menu",
					"class": "fixed"
				}).insertAfter("#header");
				
				$("#mobile-menu > li > a, #mobile-menu > li > ul > li > a").each(function() {
					var $t = $(this);
					if ($t.next().hasClass('sub-menu') || $t.next().is('ul') || $t.next().is('.sf-mega')) {
						$t.append('<span class="fa fa-angle-down mobile-menu-submenu-arrow mobile-menu-submenu-closed"></span>');
					}
				});
			
				$(".mobile-menu-submenu-arrow").click(function(event) {
					var $t = $(this);
					if ($t.hasClass("mobile-menu-submenu-closed")) {
						$t.parent().siblings("ul").slideDown(300);
						$t.parent().siblings(".sf-mega").slideDown(300);
						$t.removeClass("mobile-menu-submenu-closed fa-angle-down").addClass("mobile-menu-submenu-opened fa-angle-up");
					} else {
						$t.parent().siblings("ul").slideUp(300);
						$t.parent().siblings(".sf-mega").slideUp(300);
						$t.removeClass("mobile-menu-submenu-opened fa-angle-up").addClass("mobile-menu-submenu-closed fa-angle-down");
					}
					event.preventDefault();
				});
				
				$("#mobile-menu li, #mobile-menu li a, #mobile-menu ul").attr("style", "");
				
			}
			
		}

	}

/* ==========================================================================
   showHideMobileMenu
   ========================================================================== */

	function showHideMobileMenu() {
		
		$("#mobile-menu-trigger").click(function(event) {
			
			var $t = $(this),
				$n = $("#mobile-menu");
			
			if ($t.hasClass("mobile-menu-opened")) {
				$t.removeClass("mobile-menu-opened").addClass("mobile-menu-closed");
				$n.slideUp(300);
			} else {
				$t.removeClass("mobile-menu-closed").addClass("mobile-menu-opened");
				$n.slideDown(300);
			}
			event.preventDefault();
			
		});
		
	}

/* ==========================================================================
   handleBackToTop
   ========================================================================== */
   
   function handleBackToTop() {
	   
		$('#back-to-top').click(function(){
			$('html, body').animate({scrollTop:0}, 'slow');
			return false;
		});
   
   }
   	
/* ==========================================================================
   showHidebackToTop
   ========================================================================== */	
	
	function showHidebackToTop() {
	
		if ($(window).scrollTop() > $(window).height() / 2 ) {
			$("#back-to-top").removeClass('gone');
			$("#back-to-top").addClass('visible');
		} else {
			$("#back-to-top").removeClass('visible');
			$("#back-to-top").addClass('gone');
		}
	
	}

/* ==========================================================================
   handleSearch
   ========================================================================== */
   
	function handleSearch() {	
		
		$("#custom-search-button").click(function(e) { 
	
			e.preventDefault();
			
			if(!$("#custom-search-button").hasClass('open')) {
			
				$("#custom-search-form-container").fadeIn(400);
				
			} else {
				
				$("#custom-search-form-container").fadeOut(400);	
			
			}
			
		});
		
		$('#custom-search-form').append('<a class="custom-search-form-close" href="#" title="Close Search Box">x</a>');
		
		$('#custom-search-form-container a.custom-search-form-close').click(function(event){
			
			event.preventDefault();
			$('#custom-search-form-container').fadeOut(300);
			
		});
		
	 }

/* ==========================================================================
   handleFullScreen
   ========================================================================== */

	function handleFullScreen() {
		
		var x = $(window).height();
		
		$('.fullscreen').css("height", x + "px");
		
	}

/* ==========================================================================
   handleStickyHeader
   ========================================================================== */	
	
	var stickypoint = 400;
	if (exists("#header-wrap")) {

		stickypoint = $("#header-wrap").outerHeight() + 150;
			
	}		
	
	function handleStickyHeader() {
	
		var b = document.documentElement,
        	e = false;

		function f() {
			
			window.addEventListener("scroll", function (h) {
				
				if (!e) {
					e = true;
					setTimeout(d, 250);
				}
			}, false);
			
			window.addEventListener("load", function (h) {
				
				if (!e) {
					e = true;
					setTimeout(d, 250);
				}
			}, false);
		}
	
		function d() {
			
			var h = c();
			
			if (h >= stickypoint) {
				$('#header').addClass("stuck");
			} else {
				$('#header').removeClass("stuck");
			}
			
			e = false;
		}
	
		function c() {
			
			return window.pageYOffset || b.scrollTop;
			
		}
		
		f();
		
	}

/* ==========================================================================
   animateOdometer
   ========================================================================== */	
	
	function animateOdometer() {
		
		if(typeof window.Odometer !== 'undefined'){
		
			$(".odometer:in-viewport").each(function(index) {													
					
					var newID = 'odometer-' + index;
					
					this.id = newID;
					
					var value = $(this).attr("data-value");
					
					if (!$(this).hasClass("already-animated")) {
						
						$(this).addClass("already-animated");
									
						setTimeout(function(){
							document.getElementById(newID).innerHTML = value;
						 });
						 
					}
						 
					
			});
			
		}	
	}
	
/* ==========================================================================
   When document is ready, do
   ========================================================================== */
   
	var stickyHeader = false;
   
	$(document).ready(function() {			   
		
		ieViewportFix();
		ewf_rowBehaviour();
		setDimensionsPieCharts();
		
		animatePieCharts();
		animateMilestones();
		animateProgressBars();
		animateOdometer();
		
		handleMobileMenu();
		showHideMobileMenu();
		
		handleBackToTop();
		showHidebackToTop();

		handleSearch();
		
		if ($('body').hasClass('ewf-sticky-header')){
			stickyHeader = true;
		}
			
		if(stickyHeader && ($(window).width() > 1024)){ 
		
			handleStickyHeader();
		
		}
		
		handleFullScreen();
		
	});

/* ==========================================================================
   When the window is scrolled, do
   ========================================================================== */
   
	$(window).scroll(function() {				   
		
		animateMilestones();
		animatePieCharts();
		animateProgressBars();
		animateOdometer();
		
		showHidebackToTop();
		
		if(stickyHeader && ($(window).width() > 1024)){ 
		
			handleStickyHeader();
		
		}
	
	});

/* ==========================================================================
   When the window is resized, do
   ========================================================================== */
   
	$(window).resize(function() {
		
		handleMobileMenu();
		handleFullScreen();
		
		if(stickyHeader && ($(window).width() > 1024)){ 
		
			handleStickyHeader();
		
		}
		
	});
	

})(window.jQuery);

(function($) {

    var $allVideos = $(".blog-post iframe[src^='//player.vimeo.com'], .blog-post iframe[src^='//www.youtube.com'], .blog-post iframe, .blog-post object, .blog-post embed"),
    $fluidEl = $(".blog-post");

	$allVideos.each(function() {

	  $(this)
	    // jQuery .data does not work on object/embed elements
	    .attr('data-aspectRatio', this.height / this.width)
	    .removeAttr('height')
	    .removeAttr('width');

	});

	$(window).resize(function() {

	  var newWidth = $fluidEl.width();
	  $allVideos.each(function() {

	    var $el = $(this);
	    $el
	        .width(newWidth)
	        .height(newWidth * $el.attr('data-aspectRatio'));

	  });

	}).resize();

})(window.jQuery);

// non jQuery scripts below