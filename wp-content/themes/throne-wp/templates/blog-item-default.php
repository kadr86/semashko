<?php
	
	global $current_user, $post, $multipage;
	

	
	
	#	Get post classes
	#
	$post_class_array = get_post_class();
	$ewf_post_class = null;
	
	foreach($post_class_array as $key=> $class_item){
		$ewf_post_class.= ' '.$class_item;
	}
	
	
	
	# 	Get post categories
	#
	$ewf_post_tags = get_the_tags();
	
	
	
	# 	Get post categories
	#
	$ewf_post_categories = null;
	$ewf_post_categories_first = null;
	
	
	
	foreach((get_the_category( $post->ID )) as $category) { 
		if ($ewf_post_categories_first == null){
			$ewf_post_categories_first = '<a href="'.get_category_link( $category->term_id ).'" >'.$category->cat_name.'</a>';
		}
		
		if ($ewf_post_categories == null){
			$ewf_post_categories.= '<a href="'.get_category_link( $category->term_id ).'" >'.$category->cat_name.'</a>';
			// break;
		}else{
			$ewf_post_categories.= ', <a href="'.get_category_link( $category->term_id ).'" >'.$category->cat_name.'</a>';
		}
	}
	
	
	#	Get default time / date format
	#
	$ewf_post_date_format = get_the_date();
	$ewf_post_time_format = get_the_time();
		
	
	# Get post featured image
	#
	$ewf_image_id = get_post_thumbnail_id($post->ID);  

	
	# Conditional preloading
	#
	
	$single_post = is_single();
	
	
	$ewf_user_name = get_the_author_meta('display_name', $post->post_author);
	
	
	if (is_sticky()){ 	$ewf_post_class .= ' sticky'; }
	// if ($single_post){ 	$ewf_post_class .= ' alt'; }
	// if ($ewf_image_id){ $ewf_post_class .= ' has-post-thumbnail'; }
	
	
	echo  '<div class="blog-post '.$ewf_post_class.'">';
	
		# Post fetured image
		#
		if ($ewf_image_id && ($single_post === false) ){
			echo  '<div class="blog-post-thumb">';
				the_post_thumbnail($post->ID);
			echo  '</div><!-- end .blog-post-thumb -->';
				
		// }elseif(!$ewf_image_id && !$single_post && is_sticky()){
			// if (is_sticky()){
				// echo '<span class="sticky-post">'.__('Featured', 'bitpub').'</span>';
			// }
		}
		
		
		echo '<div class="blog-post-title">';
		
			echo '<h4>';
				if (is_sticky()){
					echo '<span class="sticky-post">'.__('Featured', 'bitpub').'</span>';
				}
				
				echo '<a href="' . get_permalink() . '">'.get_the_title($post->ID).'</a>';
			echo '</h4>' ;
				 
			
			echo '<p>';
				echo $ewf_post_categories . ' / ' . get_the_date('d.m.Y');
				
				if (comments_open() || get_comments_number()){
					echo '<i class="fa fa-comments"></i><a href="'.get_permalink().'#comments"><small>'.get_comments_number() . ' ' . __('comments', 'bitpub') . '</small></a>';
				}
				
			echo '</p>';
			
		echo '</div><!-- end .blog-post-title -->';		
		
		
		if ($single_post){

			the_content();
			
			if (!empty($multipage)){
				wp_link_pages(array(
					'before' => '<p><strong>' . __( 'Pages:', 'bitpub'),
					'after' => '</strong></p>'
				));
			}
						
			if ($ewf_post_tags){
				echo '<div class="hr"></div>';
				
				echo '<div class="tags">'.the_tags( '<strong>'.__('Tags:', 'bitpub').'</strong> ', ', ').'</div>';
			}
			
		}else{
			
			global $more;
			$more = false; 
				the_content('');
			$more = true;
			
			echo '<a class="btn alt" href="' . get_permalink() . '">'.__('Read More', 'bitpub').' &gt;&gt;</a>';
			
		}
		
	echo  '</div> <!-- .blog-post -->'; 
	
	
	if ($single_post){
		comments_template( '', true );
	}
	
?>