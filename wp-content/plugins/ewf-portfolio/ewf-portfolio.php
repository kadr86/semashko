<?php
/**
 * Eeazyee Wordpress Framework - Portfolio.
 *
 *
 * @package EWF Plugins
 * @author Bitpub
 * @author Nicolae Gabriel
 * @version 1.0.0
 *
 * @wordpress
 * Plugin Name: EWF Portfolio
 * Plugin URI: 
 * Description: Register portfolio post type
 * Author: Bitpub
 * Version: 1.0.0
 * Text Domain: ewf-plugins
 */

 
 
#	If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

 
/**
 * Plugin class for EWF Portfolio plugin
 *
 * @package EWF-Portfolio
 */
class EWFPortfolio {

	/**
	 * Loads plugin textdomain, and hooks in further actions.
	 */
	function __construct() {

		#	Change default slugs
		#
		if (!defined('EWF_PROJECTS_SLUG')){
			define ('EWF_PROJECTS_SLUG'					, 'project'	);
			define ('EWF_PROJECTS_TAX_SERVICES'			, 'service'	);
		}
		
		

		#	Register taxonomy and post type
		#
		if (post_type_exists(EWF_PROJECTS_SLUG) == false ){
		
			add_action('init'						, array($this, 'ewf_register_type_project')			,0);
			add_action('init'						, array($this, 'ewf_register_project_taxonomies')	,0); 
			
		}
			
		load_plugin_textdomain( 'ewf-portfolio', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		
	}

	 
	function ewf_register_type_project(){
		
		register_post_type( EWF_PROJECTS_SLUG , 
		
			array(
			'labels' => array(
				'name' 					=> __( 'Portfolio'					,"ewf-portfolio" ),
				'singular_name' 		=> __( 'Project'					,"ewf-portfolio" ),
				'add_new' 				=> __( 'Add New'					,"ewf-portfolio" ),
				'add_new_item' 			=> __( 'Add New Project'			,"ewf-portfolio" ),
				'edit' 					=> __( 'Edit'						,"ewf-portfolio" ),
				'edit_item' 			=> __( 'Edit Project'				,"ewf-portfolio" ),
				'new_item' 				=> __( 'New Project'				,"ewf-portfolio" ),
				'view' 					=> __( 'View Project'				,"ewf-portfolio" ),
				'view_item' 			=> __( 'View Project'				,"ewf-portfolio" ),
				'search_items' 			=> __( 'Search Projects'			,"ewf-portfolio" ),
				'not_found' 			=> __( 'No projects found'			,"ewf-portfolio" ),
				'not_found_in_trash' 	=> __( 'No projects found in Trash'	,"ewf-portfolio" ),
				'parent' 				=> __( 'Parent project'				,"ewf-portfolio" ),
				),
			'public' 		=> true,
			'rewrite' 		=> true, 
			'menu_position' => 91,
			'slug'			=> 'project',
			'show_ui' 		=> true,
			'show_in_menu'  => true,
			'supports' 		=> array('title', 'editor', 'thumbnail', 'excerpt')
			));
	} 


	function ewf_register_project_taxonomies(){
		register_taxonomy( EWF_PROJECTS_TAX_SERVICES, EWF_PROJECTS_SLUG, 
			array( 'hierarchical' => true, 
						   'slug' => 'service',
						  'label' => __('Services', "ewf-portfolio"), 
					  'query_var' => true,
						'rewrite' => true ));  
	}


}

new EWFPortfolio;
